@extends('layouts.admin.app')

@section( 'header' )
    <h1>
        Termini e Condizioni
        <small>Modifica</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route( 'admin' ) }}"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li><a href="{{ route( 'admin.policies.index' ) }}"><i class="fa fa-list"></i> Termini e Condizioni</a></li>
        <li class="active">Modifica</li>
      </ol>
@stop

@section( 'content' )
   @include( 'adminlte-templates::common.errors' )

   <div class="row">

       {!! Form::model( $policy, [ 'route' => [ 'admin.policies.update', $policy->id ], 'method' => 'patch' ] ) !!}

            @include( 'admin.policies.fields' )

       {!! Form::close() !!}
   </div>
@endsection