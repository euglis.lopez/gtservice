@extends( 'layouts.admin.app' )

@section( 'header' )
    <h1>
        Configurazioni generali
        <small>Listato</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route( 'admin' ) }}"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li class="active">Configurazioni generali</li>
    </ol>
@stop

@section( 'content' )
   @include( 'adminlte-templates::common.errors' )

    @include( 'flash::message' )

    <div class="clearfix"></div>
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">Listato</h3>
            {{-- <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#createOfferSliderItem">Crear oferta</button> --}}
        </div>

        <div class="box-body">
            @include( 'admin.business_values.table' )
        </div>

    </div>
@endsection

