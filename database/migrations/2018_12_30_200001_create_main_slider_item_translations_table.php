<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainSliderItemTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'main_slider_item_translations', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->unsignedInteger( 'main_slider_item_id' );
            $table->foreign( 'main_slider_item_id' )->references( 'id' )->on( 'main_slider_items' );

            $table->unsignedInteger( 'language_id' );
            $table->foreign( 'language_id' )->references( 'id' )->on( 'languages' );

            $table->text( 'title' );
            $table->text( 'description' )->nullable()->default( null );

            $table->text( 'text_button' )->nullable()->default( null );
            $table->text( 'link_button' )->nullable()->default( null );

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'main_slider_item_translations' );
    }
}
