<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class GallerySliderItemTranslation
 * @package App\Models\Admin
 * @version February 22, 2019, 3:41 pm UTC
 *
 * @property \App\Models\Admin\GallerySliderItem gallerySliderItem
 * @property \App\Models\Admin\Language language
 * @property \Illuminate\Database\Eloquent\Collection mainSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection offerSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property integer gallery_slider_item_id
 * @property integer language_id
 * @property string title
 * @property string description
 */
class GallerySliderItemTranslation extends Model
{
    use SoftDeletes;

    public $table = 'gallery_slider_item_translations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'gallery_slider_item_id',
        'language_id',
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'gallery_slider_item_id' => 'integer',
        'language_id' => 'integer',
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function gallerySliderItem()
    {
        return $this->belongsTo(\App\Models\Admin\GallerySliderItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function language()
    {
        return $this->belongsTo(\App\Models\Admin\Language::class);
    }
}
