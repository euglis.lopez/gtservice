<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call( [
            StatusesTableSeeder::class,
            LanguagesTableSeeder::class,
            StatusTranslationsTableSeeder::class,
            UsersTableSeeder::class,

            PostsTableSeeder::class,
            MainSliderItemsTableSeeder::class,
            BusinessValuesTableSeeder::class,
            PoliciesTableSeeder::class,

            GallerySliderItemsTableSeeder::class,

            OfferSliderItemsTableSeeder::class,
            PageSectionsTableSeeder::class,
        ] );
    }
}
