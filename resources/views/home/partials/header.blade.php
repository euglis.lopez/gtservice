<section class="orverflow-hidden p-0">
    <div class="swiper-header">
        <div class="swiper-wrapper">

            @foreach ( $mainSliderItems as $mainSliderItem )
                <div class="swiper-slide">
                    <img class="img-full" src="{{ $mainSliderItem->image }}" alt="">
                    <div class="centered">
                        <!-- MOBILE -->
                        {{-- <h4 class="d-lg-none text-white">
                            <span class="font-weight-bold">{{ $mainSliderItem->translation()->title }}</span><br>
                            <small>{!! $mainSliderItem->translation()->description !!}</small>
                        </h4> --}}
                        <!-- DESKTOP -->
                        <div>
                            <h2 class="title-slider text-white font-weight-bold">
                                {{ $mainSliderItem->translation()->title }}<br>
                            </h2>
                            <div class="text-white">
                                {!! $mainSliderItem->translation()->description !!}
                            </div>
                            
                            <a class="btn btn-primary mt-2" href="{{ !empty( $mainSliderItem->translation()->link_button ) ? $mainSliderItem->translation()->link_button : 'javascript:void(0)' }}">
                                    {{ $mainSliderItem->translation()->text_button }}
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <!-- Add Pagination -->
        <!-- <div class="swiper-pagination"></div> -->
    </div>

</section>

@push('styles')
    <style>
        h2.title-slider {
            font-size: 3rem;
        }
        .img-full{
            width: 100%;
            height: 100vh;
            object-fit: cover;
            object-position: center;
        }
        .border-button {
            border: 2px solid;
            border-radius: 8px;
        }

        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 100%;
            transform: translate(-50%, -50%);
        }

        .swiper-header{
          text-align: center;
          font-size: 18px;
          background: #fff;
          /* Center slide text vertically */
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
          width: 100%;
        }
        .swiper-header {
            position: relative;
        }
        .swiper-header .swiper-pagination{
            margin-bottom: -15px;
        }
        @media screen and (min-width: 992px){
            .swiper-header .swiper-pagination{
                margin-bottom: 10px;
            }
        }

        .swiper-slide{
            flex-direction: column;
        }
         .padding-movil-3{
             padding: 3rem 0rem;
         }
         h1{
            font-size: 2rem;
         }
        @media (min-width: 992px) {
            .padding-movil-3 {
                padding: 0rem 0rem;
            }
            h1 {
                font-size: 38px;
            }
            .swiper-slide {
                flex-direction: row;
            }
            .content-header {
                /* -webkit-clip-path: polygon(50% 100%, 0 0, 100% 0);
                clip-path: polygon(50% 100%, 0 0, 100% 0); */

                /* background-color: #000000ad; */

                /* background-image: url("/images/header/header1.jpg"); */
                position: absolute;
                height: 100%;
                width: 100%;
                left: 0;
                top:0;
            }
            .border-width{
                border-width: 2px !important;
            }
        }
    </style>
@endpush

@push('scripts')
    <script>
        var swiperHeader = new Swiper('.swiper-header', {
            slidesPerView: 1,
            spaceBetween: 30,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
        });
    </script>
@endpush