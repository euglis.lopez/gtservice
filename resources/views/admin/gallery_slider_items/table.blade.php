<table class="table table-bordered table-striped table-responsive tableGeneral" id="gallerySliderItems-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Titolo</th>
            <th width="20%">Immagine</th>
            <th class="text-center">Azione</th>
        </tr>
    </thead>
    <tbody>
    @foreach ( $gallerySliderItems as $gallerySliderItem )
        <tr>
            <td>{!! $gallerySliderItem->id !!}</td>
            <td>{!! $gallerySliderItem->translation()->title !!}</td>
            <td>
                <div class="" style="height: 100px; overflow: hidden;">
                    <img src="{{ !empty( $gallerySliderItem->image ) ? url( $gallerySliderItem->image ) : 'Sin foto' }}" alt="" class="img-responsive">
                </div>
            </td>
            <td class="text-center">
                {!! Form::open(['route' => ['admin.gallerySliderItems.destroy', $gallerySliderItem->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {{-- <a href="{!! route('admin.gallerySliderItems.show', [$gallerySliderItem->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                    <a href="{!! route('admin.gallerySliderItems.edit', [$gallerySliderItem->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@push( 'style' )
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset('/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push( 'script' )
    <!-- bootstrap datepicker -->
    <!-- DataTables -->
    <script src="{{asset('/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    {{-- Para inicializar el datatable --}}
    <script src="{{ asset( '/js/datatables.js' ) }}"></script>
    <script>
        $(function() {
            TablesDatatables.init();
        });
    </script>
@endpush