<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateOfferSliderItemRequest;
use App\Http\Requests\Admin\UpdateOfferSliderItemRequest;
use App\Repositories\Admin\OfferSliderItemRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class OfferSliderItemController extends AppBaseController
{
    /** @var  OfferSliderItemRepository */
    private $offerSliderItemRepository;

    public function __construct( OfferSliderItemRepository $offerSliderItemRepo )
    {
        $this->offerSliderItemRepository = $offerSliderItemRepo;
    }

    /**
     * Display a listing of the OfferSliderItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->offerSliderItemRepository->pushCriteria( new RequestCriteria( $request ) );
        $offerSliderItems = $this->offerSliderItemRepository->all();

        return view( 'admin.offer_slider_items.index' )
            ->with( 'offerSliderItems', $offerSliderItems );
    }

    /**
     * Show the form for creating a new OfferSliderItem.
     *
     * @return Response
     */
    public function create()
    {
        return view( 'admin.offer_slider_items.create' );
    }

    /**
     * Store a newly created OfferSliderItem in storage.
     *
     * @param CreateOfferSliderItemRequest $request
     *
     * @return Response
     */
    public function store( CreateOfferSliderItemRequest $request )
    {
        $request->validate( [
            'title'         => 'required|unique:offer_slider_item_translations,title',
        ] );

        // inputs
        $input              = $this->input( $request, false );
        $inputTranslation   = $this->inputTranslation( $request, false );

        // create model
        $model = $this->offerSliderItemRepository->create( $input );

        // create translation
        $model->translations()->create( $inputTranslation );

        Flash::success( 'Offer Slider Item saved successfully.' );

        return redirect()->route( 'admin.offerSliderItems.edit', $model );
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request, $validate = true )
    {
        //Translation Validation
        if ( $validate === true ) {
            // $request->validate( [
            //     'image'     => 'required',
            // ] );
        }

        $model =  $request->only( [
            // 'image'
        ] );
        $model[ 'slug' ]        = str_slug( $request->input( 'title' ), '-' );
        $model[ 'status_id' ]   = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request, $validate = true )
    {
        //Translation Validation
        if ( $validate === true ) {
            $request->validate( [
                'title'         => 'required',
                // 'description'   => 'required',
                'text_button1'  => 'required',
                'value_button1' => 'required',
                'text_button2'  => 'required',
                'value_button2' => 'required',
            ] );
        }

        $translation = $request->only( [
            'title',
            // 'description',
            'text_button1',
            'value_button1',
            'text_button2',
            'value_button2',
        ] );
        $translation[ 'language_id' ] = 1;

        return $translation;
    }

    /**
     * Show the form for editing the specified OfferSliderItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $id )
    {
        $offerSliderItem = $this->offerSliderItemRepository->findWithoutFail( $id );

        if ( empty( $offerSliderItem ) ) {
            Flash::error( 'Offer Slider Item not found' );

            return redirect( route( 'admin.offerSliderItems.index' ) );
        }

        return view( 'admin.offer_slider_items.edit' )
            ->with( 'offerSliderItem', $offerSliderItem )
            ->with( 'translation', $offerSliderItem->translations->first() );
    }

    /**
     * Update the specified OfferSliderItem in storage.
     *
     * @param  int              $id
     * @param UpdateOfferSliderItemRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateOfferSliderItemRequest $request )
    {
        $model = $this->offerSliderItemRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Offer Slider Item not found' );

            return redirect( route( 'admin.offerSliderItems.index' ) );
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->offerSliderItemRepository->update( $input, $id );

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        Flash::success( 'Offer Slider Item updated successfully.' );

        return redirect( route( 'admin.offerSliderItems.index' ) );
    }

    /**
     * Remove the specified OfferSliderItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $offerSliderItem = $this->offerSliderItemRepository->findWithoutFail( $id );

        if ( empty( $offerSliderItem ) ) {
            Flash::error( 'Offer Slider Item not found' );

            return redirect( route( 'admin.offerSliderItems.index' ) );
        }

        // delete translations
        $translations = $offerSliderItem->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        // delete model
        $this->offerSliderItemRepository->delete( $id );

        Flash::success( 'Offer Slider Item deleted successfully.' );

        return redirect( route( 'admin.offerSliderItems.index' ) );
    }

    /**
     * Store a image in $model at given $field.
     *
     * @param Request $request
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function storePhoto( $id, $field )
    {
        $offerSliderItem = $this->offerSliderItemRepository->findWithoutFail( $id );

        if ( empty( $offerSliderItem ) ) {
            Flash::error( 'Offer Slider Item not found' );

            return redirect( route( 'admin.offerSliderItems.index' ) );
        }

        return $this->storePhotoGeneral( request(), $offerSliderItem, $field );
    }

    /**
     * Delete a image in $model at given $field.
     *
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function destroyPhoto( $id, $field )
    {
        $offerSliderItem = $this->offerSliderItemRepository->findWithoutFail( $id );

        if ( empty( $offerSliderItem ) ) {
            Flash::error( 'Offer Slider Item not found' );

            return redirect( route( 'admin.offerSliderItems.index' ) );
        }

        return $this->destroyPhotoGeneral( $offerSliderItem, $field );
    }
}
