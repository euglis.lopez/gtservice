<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGallerySliderItemTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'gallery_slider_item_translations', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->unsignedInteger( 'gallery_slider_item_id' );
            $table->foreign( 'gallery_slider_item_id' )->references( 'id' )->on( 'gallery_slider_items' );

            $table->unsignedInteger( 'language_id' );
            $table->foreign( 'language_id' )->references( 'id' )->on( 'languages' );

            $table->text( 'title' );
            $table->text( 'description' )->nullable()->default( null );

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'gallery_slider_item_translations' );
    }
}
