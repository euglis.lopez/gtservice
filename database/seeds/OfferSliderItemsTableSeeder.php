<?php

use Illuminate\Database\Seeder;

class OfferSliderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Oferta 1
         */
        DB::table( 'offer_slider_items' )->insert( [
            [
                'slug'              => 'invernale',
                'image'             => 'images/gallery/promocion1.jpg',
                'image_responsive'  => 'images/gallery/promocion1.jpg',
                'status_id'         => 1,
            ]
        ] );
        DB::table( 'offer_slider_item_translations' )->insert( [
            [
                'offer_slider_item_id'  => 1,
                'language_id'           => 1,
                'title'                 => 'Invernale',
                'description'           => '<small>Olbia Idraulici ed Elettricisti</small>',
                'text_button1'          => '',
                'value_button1'         => '+393458377263',
                'text_button2'          => '',
                'value_button2'         => 'gtserviceolbia@gmail.com',
            ]
        ] );


        /*
         * Oferta 2
         */
        DB::table( 'offer_slider_items' )->insert( [
            [
                'slug'              => 'estate',
                'image'             => 'images/gallery/promocion2.jpg',
                'image_responsive'  => 'images/gallery/promocion2.jpg',
                'status_id'         => 1,
            ]
        ] );
        DB::table( 'offer_slider_item_translations' )->insert( [
            [
                'offer_slider_item_id'  => 2,
                'language_id'           => 1,
                'title'                 => 'Estate',
                'description'           => '<small>Olbia Idraulici ed Elettricisti</small>',
                'text_button1'          => '',
                'value_button1'         => '+393458377263',
                'text_button2'          => '',
                'value_button2'         => 'gtserviceolbia@gmail.com',
            ]
        ] );
    }
}
