<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    /**
     * Update or create the translation for specified model.
     *
     * @param $model
     * @param array $inputTranslation
     *
     * @return void
     */
    protected function updateOrCreateTranslation( $model, $inputTranslation )
    {
        // ACA SE VALIDA SI EL IDIOMA INDICADO YA ESTA GUARDADO
        $translation = $model->translations->filter( function ( $trans ) use ( $inputTranslation ) {
            return $trans->language_id == $inputTranslation[ 'language_id' ];
        } )->first();

        if ( $translation !== null ) {
            // SI ENTRA AQUI SIGNIFICA QUE EL IDIOMA YA EXISTE, ENTONCES SE ACTUALIZA
            $translation->update( $inputTranslation, [ 'id' => $translation->id ] );
        }
        else {
            // SI ENTRA AQUI SIGNIFICA QUE EL IDIOMA ES NUEVO, ENTONCES SE GUARDA POR PRIMERA VEZ
            $model->translations()->create( $inputTranslation );
        }
    }

    /**
     * Store a image in $model at given $field.
     *
     * @param Request $request
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function storePhotoGeneral( Request $request, $model, string $field )
    {
        // validate if there is image in $request
        $this->validate( $request, [
            'photo' => 'image|max:2048|required'
        ] );

        // if the $field is not empty, destroy the current image
        if ( !empty( $model->$field ) ) {
            $this->destroyPhoto( $model->id, $field );
        }

        $photo = $request->file( 'photo' )->store( 'public' );

        $photoUrl = Storage::url( $photo );

        $model->$field = $photoUrl;
        $model->save();
    }

    /**
     * Delete a image in $model at given $field.
     *
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function destroyPhotoGeneral( $model, string $field )
    {
        $photoUrl       = $model->$field;
        $model->$field  = null;
        $model->save();

        $photoPatch = str_replace( 'storage', 'public', $photoUrl );
        Storage::delete( $photoPatch );

        return back()->with( 'flash', 'foto eliminada' );
    }
}
