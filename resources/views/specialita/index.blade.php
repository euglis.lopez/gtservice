@extends( 'app' )

@section( 'content' )

    @include( 'global.navbar' )

    <div id="esp">
        @include( 'specialita.partials.title' )
        @include( 'global.botones' )


        @foreach ( $posts as $key => $post )
            <section id="{{ $post->url }}" class="orverflow-hidden pb-5 pt-3 {{ $key%2 !== 0 ? 'bg-gray' : '' }}">
                <div class="container-fluid mt-md-5">
                    <div class="row px-5">
                        <div class="col-xs-12 col-md-6 text-center {{ $key%2 !== 0 ? 'order-1 order-md-2' : 'order-1 order-md-1' }}">
                            <img class="img-fluid" width="400" height="400" src="{{ $post->photos->first()->url }}">
                        </div>
                        <div class="col-xs-12 col-md-6 pl-0 {{ $key%2 !== 0 ? 'order-2 order-md-1' : 'order-2 order-md-2' }}">
                            <!-- MOBILE -->
                            <h4 class="d-md-none text-uppercase underline-left font-weight-bold text-primary mt-4 mb-5">
                                {{ $post->title }}
                            </h4>

                            <!-- DESKTOP -->
                            <h2 class="d-none d-md-block underline-left font-weight-bold text-primary mb-5">
                                {{ $post->title }}
                            </h2>

                            <span style="font-size: 14pt">
                                {!! $post->body !!}
                            </span>
                        </div>
                    </div>
                </div>
            </section>
        @endforeach

        @include( 'home.partials.contacto' )
    </div>

    @include( 'global.contact' )
    @include( 'global.footer' )
    @include( 'global.cookie-law' )
    @include( 'global.navfixed' )

@endsection

@push( 'styles' )
    <style>
        #esp h1 {
            font-family: 'Montserrat', sans-serif;
            color: #8fc73e;
            font-size: 2.6rem
        }
        #esp h2 {
            font-family: 'Montserrat', sans-serif;
            color: #333;
            font-size: 2.4rem;
        }
        #esp p {
            font-family: 'Montserrat', sans-serif;
            letter-spacing: 1px;
        }
        #gallery {
            overflow: hidden;
        }
        .swiper-gallery-desktop {
            width: 100%;
            height: 100%;
        }
        .swiper-gallery-desktop .swiper-slide {
          text-align: center;
          font-size: 18px;
          /* Center slide text vertically */
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
        }
        .box-action {
            width: 200px;
            height: 210px;
            background-color: #f9cb4c;
            color: white;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
    </style>
@endpush

@push( 'scripts' )
    <script>
        var swiperGallery = new Swiper( '.swiper-gallery-desktop', {
            slidesPerView: 3,
            spaceBetween: 30,
            centeredSlides: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
          // Responsive breakpoints
          breakpoints: {
                768: {
                slidesPerView: 2,
                spaceBetween: 20
                },
                1080: {
                slidesPerView:2,
                spaceBetween: 30
                }
            }
        } );
    </script>
@endpush