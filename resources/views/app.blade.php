<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}}</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1,maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" href="{{asset('vendor/lightbox2/dist/css/lightbox.min.css')}}">
    <!--
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css">
    -->

    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/swiper-4.3.5/dist/css/swiper.min.css')}}">
    <link rel="stylesheet" href="/css/cookiealert.css">

    <link rel="icon" href="{{url('images/logo/logo.png')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    @stack('styles')
    <style>
        .nav-item.active, .nav-item:hover {
            border-bottom: none;
        }
    </style>
</head>
<body>
<div id="app">

    @yield('content')
    <!-- Modal galleria -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="d-flex flex-row justify-content-center align-items-center w-100 h-100">
                <div class="content-foto-modal d-inline position-relative">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal-body">
                        <img class="img-fluid img-full" :src="image" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="{{asset('vendor/swiper-4.3.5/dist/js/swiper.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/gh/Wruczek/Bootstrap-Cookie-Alert@gh-pages/cookiealert.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<script src="{{asset('vendor/lightbox2/dist/js/lightbox-plus-jquery.min.js')}}"></script>

<!-- development version, includes helpful console warnings -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vee-validate@2.1.6"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vee-validate/2.1.6/locale/it.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script>
    Vue.use(VeeValidate, {
        locale: 'it'
    });

    const COOKIE_LAW_KEY = 'acceptCookies';

    const app = new Vue({
        el: '#app',
        data: {
            image: null,
            textleer: 'Vedi tutto',
            text_condicion: false,
            formEmail: {
                name: '',
                email: '',
                phone: '',
                message: ''
            },
            loading: false,
            pathname: ''
        },
        created() {
            this.pathname = window.location.pathname;
        },
        computed: {
            isFormEmailValid() {
                return (this.formEmail.name.length > 0 && this.formEmail.email.length > 0 &&
                    this.formEmail.phone.length > 0 && this.formEmail.message.length > 0);
            },
            showCookieLaw() {
                return !Cookies.get(COOKIE_LAW_KEY);
            }
        },
        methods: {
            modalImage(url){
                this.image = url;
            },
            verMas(){
                if(this.text_condicion == false)
                {
                    this.text_condicion = true;
                    this.textleer = 'Vedi meno';
                }
                else{
                    this.text_condicion = false;
                    this.textleer = 'Vedi tutto'
                }
            },
            showSuccess() {
                Swal.fire({
                    title: 'Email inviata!',
                    text: 'Il tuo messaggio è stato inviato.',
                    type: 'success',
                    showCloseButton: true,
                    timer: 3000,
                    confirmButtonText: 'Accettare'
                });
            },
            showError() {
                Swal.fire({
                    title: "Email non inviata",
                    text: "L'email non può essere inviata.",
                    type: 'error',
                    showCloseButton: true,
                    timer: 3000,
                    confirmButtonText: 'Accettare'
                });
            },
            async sendMail() {
                if (await this.$validator.validate()) {
                    try {
                        this.loading = true;
                        const response = await axios.post('sendmail', this.formEmail);
                        this.$refs.form.reset();
                        this.showSuccess();
                        this.clearForm();
                        this.$validator.reset();
                    } catch (error) {
                        this.showError();
                    } finally {
                        this.loading = false;
                    }
                }
            },
            clearForm() {
                this.formEmail.name = '';
                this.formEmail.email = '';
                this.formEmail.phone = '';
                this.formEmail.message = '';
            },
            setCookieLaw() {
                Cookies.set(COOKIE_LAW_KEY, true, { expires: 360 });
                this.$refs.cookiealert.classList.remove("show");
            }
        }
    });
</script>

@stack('scripts')
</body>
</html>