@extends( 'app' )

@section( 'content' )

    @include( 'global.navbar' )

    @include( 'home.partials.header' )
    @include( 'home.partials.chisiamo' )
    @include( 'home.partials.especialidades' )
    @include( 'home.partials.gallery' )
    @include( 'home.partials.oferta' )
    @include( 'home.partials.contacto' )
    @include( 'home.partials.map' )

    @include( 'global.contact' )
    @include( 'global.footer' )
    @include( 'global.cookie-law' )
    @include( 'global.navfixed' )

@endsection

@push( 'scripts' )
    {{-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> --}}
    {{-- <script src="{{ asset( 'js/contacto.js' ) }}"></script> --}}
@endpush