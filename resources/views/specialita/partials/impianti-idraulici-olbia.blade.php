<section id="impianti-idraulici-olbia" class="orverflow-hidden pb-5 pt-3 bg-gray">
    <div class="container-fluid mt-md-5">
        <div class="row px-5">
            <div class="col-xs-12 col-md-6 pl-0 order-2 order-md-2 pl-md-4">
                <!-- MOBILE -->
                <h4 class="d-md-none text-uppercase underline-left font-weight-bold text-primary mt-4 mb-5">
                    IMPIANTI IDRAULICI OLBIA
                </h4>
                
                <!-- DESKTOP -->
                <h2 class="d-none d-md-block underline-left font-weight-bold text-primary mb-5">
                    IMPIANTI IDRAULICI OLBIA
                </h2>

                <p style="font-size: 14pt">
                    Siamo in grado di realizzare impianti idraulici a Olbia di qualunque dimensione, raggiungiamo facilmente clienti in tutta la Sardegna ma sopratutto nelle aree limitrofi.
                </p>
            </div>
            <div class="col-xs-12 col-md-6 text-center order-sm-1 order-md-2">
                <img class="img-fluid" width="400" height="400" src="images/especial/slider_3.jpg">
            </div>
        </div>
    </div>
</section>

