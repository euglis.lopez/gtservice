@extends('layouts.auth.app')

@section( 'content' )

    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url( '/' ) }}">{{ config( 'app.name' ) }}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ __('Resetta la password') }}</p>

            @if ( session( 'status' ) )
                <div class="alert alert-success" role="alert">
                    {{ session( 'status' ) }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Resetta la password') }}">
                @csrf

                <div class="form-group {{ $errors->has('email') ? ' is-invalid' : '' }} has-feedback">
                    <input type="email"
                    class="form-control"
                    placeholder="E-mail"
                    name="email"
                    value="{{ old('email') }}"
                    required autofocus>

                    @if ( $errors->has( 'email' ) )
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first( 'email' ) }}</strong>
                        </span>
                    @endif
                </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-offset-2 col-xs-10">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Invia il link per reimpostare la password') }}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <!-- /.social-auth-links -->
            <br>
            <a href="{{ route('login') }}">Torna al login</a><br>

        </div>
        <!-- /.login-box-body -->
    </div>
@endsection
