<table class="table table-bordered table-striped table-responsive tableGeneral" id="offerSliderItems-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Titolo</th>
            <th width="20%">Immagine del desktop</th>
            <th width="20%">Immagine del responsive</th>
            <th class="text-center">Azione</th>
        </tr>
    </thead>
    <tbody>
    @foreach ( $offerSliderItems as $offerSliderItem )
        <tr>
            <td>{!! $offerSliderItem->id !!}</td>
            <td>{!! $offerSliderItem->translation()->title !!}</td>
            <td>
                <div class="" style="height: 100px; overflow: hidden;">
                    <img src="{{ !empty( $offerSliderItem->image ) ? url( $offerSliderItem->image ) : 'Sin foto' }}" alt="" class="img-responsive">
                </div>
            </td>
            <td>
                <div class="" style="height: 100px; overflow: hidden;">
                    <img src="{{ !empty( $offerSliderItem->image_responsive ) ? url( $offerSliderItem->image_responsive ) : 'Sin foto' }}" alt="" class="img-responsive">
                </div>
            </td>
            <td class="text-center">
                {!! Form::open(['route' => ['admin.offerSliderItems.destroy', $offerSliderItem->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {{-- <a href="{!! route('admin.offerSliderItems.show', [$offerSliderItem->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a> --}}
                    <a href="{!! route('admin.offerSliderItems.edit', [$offerSliderItem->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@push( 'style' )
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset('/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push( 'script' )
    <!-- bootstrap datepicker -->
    <!-- DataTables -->
    <script src="{{asset('/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    {{-- Para inicializar el datatable --}}
    <script src="{{ asset( '/js/datatables.js' ) }}"></script>
    <script>
        $(function() {
            TablesDatatables.init();
        });
    </script>
@endpush