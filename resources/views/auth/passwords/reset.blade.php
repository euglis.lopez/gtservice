@extends('layouts.auth.app')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url( '/' ) }}">{{ config( 'app.name' ) }}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ __('Resetta la password') }}</p>

            @if ( session( 'status' ) )
                <div class="alert alert-success" role="alert">
                    {{ session( 'status' ) }}
                </div>
            @endif

            <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Resetta la password') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group {{ $errors->has('email') ? ' is-invalid' : '' }} has-feedback">
                    <input id="email" type="email"
                    class="form-control"
                    placeholder="{{ __('E-Mail') }}"
                    name="email"
                    value="{{ old('email') }}"
                    required autofocus>

                    @if ( $errors->has( 'email' ) )
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first( 'email' ) }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('password') ? ' is-invalid' : '' }} has-feedback">
                    <input id="password" type="password" class="form-control" placeholder="{{ __('Password') }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('password-confirm') ? ' is-invalid' : '' }} has-feedback">
                    <input type="password" class="form-control" placeholder="{{ __('Conferma Password') }}" name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password-confirm') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-offset-4 col-xs-8">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Resetta la password') }}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <!-- /.social-auth-links -->
            <br>
            <a href="{{ route('login') }}">Torna al login</a><br>

        </div>
        <!-- /.login-box-body -->
    </div>
@endsection
