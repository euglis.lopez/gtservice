// variables
const nombre = document.getElementById('nombre');
const apellido = document.getElementById('apellido');
const email = document.getElementById('email');
const telefono = document.getElementById('telefono');
const mensaje = document.getElementById('mensaje');
const btnEnviar = document.getElementById('enviar');
const formularioEnviar = document.getElementById('enviar-mail');
const resetBtn = document.getElementById('resetBtn');

// eventListener
eventListeners();

function eventListeners(){
    // Inicio de la aplicacion y deshabilitar el submit
    document.addEventListener('DOMContentLoaded', inicioApp);

    // Campos del formulario
    nombre.addEventListener('blur', validarCampo);
    apellido.addEventListener('blur', validarCampo);
    email.addEventListener('blur', validarCampo);
    telefono.addEventListener('blur', validarCampo);
    mensaje.addEventListener('blur', validarCampo);

    //boton de enviar en el submit
    formularioEnviar.addEventListener('submit', enviarEmail);

    // boton de reset
    resetBtn.addEventListener('click', resetFormulario);
}

// funciones
function inicioApp(){
    // Desahabilitar el envio de email (boton enviar desahabilitado)
    btnEnviar.disabled = true;
}

// validar que los campos no esten vacios
function validarCampo(){
    // Se valida la longitud del texto y que  no este vacio
    validarLongitud(this);

    // validar unicamente el email
    if (this.type === 'email') {
        validarEmail(this);
    }

    let errores = document.querySelectorAll('.error');

    if (email.value !== '' && nombre.value !== '' && apellido.value !== '' && email.value !== '' && telefono.value !== '') {

        if (errores.length === 0) {
            btnEnviar.disabled = false;
        }
    }
}

// Resetear el formulario
function resetFormulario(e){
    formularioEnviar.reset();
    e.preventDefault();
}

// cuando se envia el correo
function enviarEmail(e){
    e.preventDefault();
    // spinner al presionar enviar
    const spinnerGif = document.querySelector('#spinner');
    spinnerGif.style.display = 'block';

    // Gif que envia el email
    const enviado = document.createElement('img');
    enviado.classList.add('img-fluid');
    enviado.src = 'images/gifs/mail.gif';
    enviado.style.display = 'block';
    
    let data = Array.from(new FormData(this), e => e.map(encodeURIComponent).join('=')).join('&');

    let url = '/sendmail';
    // Conectar con ajax
    // Iniciar XMLHTTPRequest
    const xhr = new XMLHttpRequest();
    // Abrimos la conexion
    xhr.open('POST', url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // Datos e impresion del template
    xhr.onload = function() {
        if(this.status === 200) {
            // ocultar spinner y mostrar gif de enviado
            setTimeout(function(){
                spinnerGif.style.display = 'none';
                document.querySelector('#loaders').appendChild(enviado);
                setTimeout(function(){
                    enviado.remove();
                    formularioEnviar.reset();
                }, 5000);
            }, 3000);
            btnEnviar.disabled = true;
            e.preventDefault();
        }
    }
    // Enviar el Request
    xhr.send(data);
}

//  verificar la longitud del texto de los campos
function validarLongitud(campo) {
    if (campo.value.length > 0) {
        campo.style.borderBottomColor = 'green';
        campo.classList.remove('error');
    } else {
        campo.style.borderBottomColor = 'red';
        campo.classList.add('error');
    }
}

function validarEmail(campo){
    const mensaje = campo.value;
    if (mensaje.indexOf('@') !== -1) {
        campo.style.borderBottomColor = 'green';
        campo.classList.remove('error');
    }else{
        campo.style.borderBottomColor = 'red';
        campo.classList.add('error');
    }
}
