<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'policy_translations', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->unsignedInteger( 'policy_id' );
            $table->foreign( 'policy_id' )->references( 'id' )->on( 'policies' );

            $table->unsignedInteger( 'language_id' );
            $table->foreign( 'language_id' )->references( 'id' )->on( 'languages' );

            $table->text( 'title' );
            $table->text( 'description' )->nullable()->default( null );

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'policy_translations' );
    }
}
