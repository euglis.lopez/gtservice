<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageSectionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'page_section_translations', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->unsignedInteger( 'page_section_id' );
            $table->foreign( 'page_section_id' )->references( 'id' )->on( 'page_sections' );

            $table->unsignedInteger( 'language_id' );
            $table->foreign( 'language_id' )->references( 'id' )->on( 'languages' );

            $table->text( 'title' );
            $table->text( 'description' )->nullable()->default( null );

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'page_section_translations' );
    }
}
