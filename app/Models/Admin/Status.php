<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Status
 * @package App\Models\Admin
 * @version February 19, 2019, 6:02 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Language
 * @property \Illuminate\Database\Eloquent\Collection mainSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection StatusTranslation
 * @property string code
 */
class Status extends Model
{

    public $table = 'statuses';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function languages()
    {
        return $this->hasMany(\App\Models\Admin\Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function statusTranslations()
    {
        return $this->hasMany(\App\Models\Admin\StatusTranslation::class);
    }
}
