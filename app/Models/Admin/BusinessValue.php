<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BusinessValue
 * @package App\Models\Admin
 * @version February 20, 2019, 8:07 pm UTC
 *
 * @property \App\Models\Admin\Status status
 * @property \Illuminate\Database\Eloquent\Collection mainSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection offerSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property string field
 * @property string value
 * @property string icon
 * @property integer status_id
 */
class BusinessValue extends Model
{
    use SoftDeletes;

    public $table = 'business_values';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'field',
        'name',
        'value',
        'icon',
        'status_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'field' => 'string',
        'name' => 'string',
        'value' => 'string',
        'icon' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Admin\Status::class);
    }
}
