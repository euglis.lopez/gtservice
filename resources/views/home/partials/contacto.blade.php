<section id="contacto" class="bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 offset-lg-6 col-lg-6"> 
                <div class="row mt-5">
                    <div class="col-12 text-md-left">
                        <h2 class="text-uppercase font-weight-bold text-primary mt-4">
                            Preventivo
                        </h2>
                        <hr class="border-title" align="left">
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-12">
                        <form ref="form" id="enviar-mail">
                            <input type="hidden" name="_token" value="{{{ Session::token() }}}">
                            <v-text-field
                                v-model="formEmail.name"
                                label="Nome*"
                                name="name"
                                color="#2c6ac9"
                                data-vv-as="Nome"
                                v-validate="'required|alpha_spaces'"
                                :error-messages="errors.collect('name')"
                            ></v-text-field>
                            <v-text-field
                                v-model="formEmail.email"
                                label="E-mail*"
                                name="email"
                                color="#2c6ac9"
                                v-validate="'required|email'"
                                :error-messages="errors.collect('email')"
                            ></v-text-field>
                            <v-text-field
                                v-model="formEmail.phone"
                                label="Telefono*"
                                name="phone"
                                color="#2c6ac9"
                                data-vv-as="Telefono"
                                v-validate="'required'"
                                :error-messages="errors.collect('phone')"
                            ></v-text-field>
                            <v-text-field
                                v-model="formEmail.message"
                                label="Messaggio*"
                                name="message"
                                color="#2c6ac9"
                                data-vv-as="Messaggio"
                                v-validate="'required'"
                                :error-messages="errors.collect('message')"
                            ></v-text-field>
                            <v-btn
                                id="enviar"
                                color="info"
                                class="font-weight-bold border-button ml-0"
                                large
                                :disabled="errors.any() || !isFormEmailValid"
                                @click="sendMail"
                                :loading="loading"
                            >
                                Enviare
                            </v-btn>
                        </form>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</section>

@push('styles')
<style>
    #contacto {
        background-color: #e7e4e7;
        background-image: url(images/contacto/fondo.jpg);
        background-repeat: no-repeat,no-repeat;
        background-position-y: center;
    }
    @media only screen and (max-width: 999px) {
        #contacto {
            background-image: none;
        }
    }
</style>
@endpush