<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreatePolicyRequest;
use App\Http\Requests\Admin\UpdatePolicyRequest;
use App\Repositories\Admin\PolicyRepository;
use Flash;
use Illuminate\Http\Request;

use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PolicyController extends AppBaseController
{
    /** @var  PolicyRepository */
    private $policyRepository;

    public function __construct( PolicyRepository $policyRepo )
    {
        $this->policyRepository = $policyRepo;
    }

    /**
     * Display a listing of the Policy.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->policyRepository->pushCriteria( new RequestCriteria( $request ) );
        $policies = $this->policyRepository->all();

        return view( 'admin.policies.index' )
            ->with( 'policies', $policies );
    }

    /**
     * Show the form for creating a new Policy.
     *
     * @return Response
     */
    public function create()
    {
        return view( 'admin.policies.create' );
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request, $updateSlug = true )
    {
        //Translation Validation
        // if ( $validate === true ) {
            // $request->validate( [
            //     'image'     => 'required',
            // ] );
        // }

        $model =  $request->only( [
            // 'image'
        ] );

        if ( $updateSlug === true ) {
            $model[ 'slug' ]        = str_slug( $request->input( 'title' ), '-' );
        }

        $model[ 'status_id' ]   = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request, $validate = true )
    {
        //Translation Validation
        if ( $validate === true ) {
            $request->validate( [
                'title'         => 'required',
                'description'   => 'required',
            ] );
        }

        $translation = $request->only( [
            'title',
            'description',
        ] );
        $translation[ 'language_id' ] = 1;

        return $translation;
    }

    /**
     * Show the form for editing the specified Policy.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $id )
    {
        $policy = $this->policyRepository->findWithoutFail( $id );

        if ( empty( $policy ) ) {
            Flash::error( 'Policy not found' );

            return redirect( route( 'admin.policies.index' ) );
        }

        return view( 'admin.policies.edit' )
            ->with( 'policy', $policy )
            ->with( 'translation', $policy->translations->first() );
    }

    /**
     * Update the specified Policy in storage.
     *
     * @param  int              $id
     * @param UpdatePolicyRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdatePolicyRequest $request )
    {
        $model = $this->policyRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Policy not found' );

            return redirect( route( 'admin.policies.index' ) );
        }

        // inputs
        $input              = $this->input( $request, false );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->policyRepository->update( $request->all(), $id );

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        Flash::success( 'Policy updated successfully.' );

        return redirect( route( 'admin.policies.index' ) );
    }

    /**
     * Remove the specified Policy from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $policy = $this->policyRepository->findWithoutFail( $id );

        if ( empty( $policy ) ) {
            Flash::error( 'Policy not found' );

            return redirect( route( 'admin.policies.index' ) );
        }

        // delete translations
        $translations = $mainSliderItem->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        // delete model
        $this->policyRepository->delete( $id );

        Flash::success( 'Policy deleted successfully.' );

        return redirect( route( 'admin.policies.index' ) );
    }
}
