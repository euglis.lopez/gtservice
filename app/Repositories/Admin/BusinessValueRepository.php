<?php

namespace App\Repositories\Admin;

use App\Models\Admin\BusinessValue;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BusinessValueRepository
 * @package App\Repositories\Admin
 * @version February 20, 2019, 8:07 pm UTC
 *
 * @method BusinessValue findWithoutFail($id, $columns = ['*'])
 * @method BusinessValue find($id, $columns = ['*'])
 * @method BusinessValue first($columns = ['*'])
*/
class BusinessValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'field',
        'value',
        'icon',
        'status_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BusinessValue::class;
    }

    public function getCustom()
    {
        $return = [];

        foreach ( $this->all() as $item ) {
            $return[ $item->field ] = $item->value;
        }

        return $return;
    }
}
