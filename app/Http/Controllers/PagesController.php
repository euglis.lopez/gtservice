<?php

namespace App\Http\Controllers;

use App\Post;
use App\Repositories\Admin\BusinessValueRepository;
use App\Repositories\Admin\GallerySliderItemRepository;
use App\Repositories\Admin\MainSliderItemRepository;
use App\Repositories\Admin\OfferSliderItemRepository;
use App\Repositories\Admin\PageSectionRepository;
use App\Repositories\Admin\PolicyRepository;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /** @var  MainSliderItemRepository */
    private $mainSliderItemRepository;

    /** @var  OfferSliderItemRepository */
    private $offerSliderItemRepository;

    /** @var  GallerySliderItemRepository */
    private $gallerySliderItemRepository;

    /** @var  BusinessValueRepository */
    private $businessValueRepository;

    /** @var  PolicyRepository */
    private $policyRepository;

    /** @var  PageSectionRepository */
    private $pageSectionRepository;

    public function __construct( MainSliderItemRepository $mainSliderItemRepo,
        OfferSliderItemRepository $offerSliderItemRepo,
        GallerySliderItemRepository $gallerySliderItemRepo,
        BusinessValueRepository $businessvalueRepo,
        PolicyRepository $policyRepo,
        PageSectionRepository $pageSectionRepo )
    {
        $this->mainSliderItemRepository     = $mainSliderItemRepo;
        $this->offerSliderItemRepository    = $offerSliderItemRepo;
        $this->businessValueRepository      = $businessvalueRepo;
        $this->gallerySliderItemRepository  = $gallerySliderItemRepo;
        $this->policyRepository             = $policyRepo;
        $this->pageSectionRepository        = $pageSectionRepo;
    }

    /**
     * Display the landing page.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $posts              = Post::all();
        $mainSliderItems    = $this->mainSliderItemRepository->all();
        $offerSliderItems   = $this->offerSliderItemRepository->all();
        $gallerySliderItems = $this->gallerySliderItemRepository->all();
        $businessValues     = $this->businessValueRepository->getCustom();

        // chi siamo
        $chiSiamo = $this->pageSectionRepository->findByField( 'code', 'chi-siamo' )->first();

        return view( 'home.index', compact( 'posts', 'mainSliderItems', 'offerSliderItems', 'gallerySliderItems', 'businessValues', 'chiSiamo' ) );
    }

    /**
     * Display the services page.
     *
     * @return \Illuminate\Http\Response
     */
    public function services()
    {
        $posts = Post::all();
        $businessValues = $this->businessValueRepository->getCustom();

        return view( 'specialita.index', compact( 'posts', 'businessValues' ) );
    }

    /**
     * Display the terms page.
     *
     * @return \Illuminate\Http\Response
     */
    public function terms( Request $request )
    {
        $posts          = Post::all();
        $businessValues = $this->businessValueRepository->getCustom();
        $policies       = $this->policyRepository->all();

        $active = $request->get( 't' );

        if ( $active === null ) {
            $active = 'policy';
        }

        return view( 'term' , compact( 'posts', 'businessValues', 'policies', 'active' ) );
    }

    /**
     * Display the policy page.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function policy()
    {
        return view( 'privacy' );
    }*/

    /**
     * Display the cookies page.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function cookies()
    {
        return view( 'cookies' );
    }*/

}
