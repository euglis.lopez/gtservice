-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 16, 2019 at 05:36 PM
-- Server version: 5.6.43-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u438uqkk_gtservice`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_values`
--

CREATE TABLE `business_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `field` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_values`
--

INSERT INTO `business_values` (`id`, `field`, `name`, `value`, `icon`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'phone1', 'Telefono 1', '3458377263', NULL, 1, NULL, NULL, NULL),
(2, 'email', 'E-mail', 'gtserviceolbia@gmail.com', NULL, 1, NULL, NULL, NULL),
(3, 'address1', 'Indirizzo 1', 'VIA LONDRA, 14/B 07026 Olbia (ss)', NULL, 1, NULL, '2019-06-05 16:35:02', NULL),
(4, 'address2', 'Indirizzo 2', 'Olbia OT, Italia', NULL, 1, NULL, NULL, NULL),
(5, 'link_maps', 'Url Google Map', 'https://goo.gl/maps/YnVtnZ52X1G2', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'SERVIZI', '2019-03-22 17:13:09', '2019-03-22 17:13:09');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_slider_items`
--

CREATE TABLE `gallery_slider_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_slider_items`
--

INSERT INTO `gallery_slider_items` (`id`, `slug`, `image`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'imagen-1', 'images/gallery/imagen1.jpeg', 1, NULL, NULL, NULL),
(2, 'imagen-2', 'images/gallery/imagen2.jpeg', 1, NULL, NULL, NULL),
(3, 'imagen-3', 'images/gallery/imagen3.jpeg', 1, NULL, NULL, NULL),
(4, 'imagen-4', 'images/gallery/imagen4.jpeg', 1, NULL, NULL, NULL),
(5, 'nuevo-post', '/storage/IuFHSym5f40yAnRO4c64F4FgJD4We1gw8AT19nA3.png', 1, '2019-03-22 20:53:58', '2019-03-22 20:54:37', '2019-03-22 20:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_slider_item_translations`
--

CREATE TABLE `gallery_slider_item_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `gallery_slider_item_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_slider_item_translations`
--

INSERT INTO `gallery_slider_item_translations` (`id`, `gallery_slider_item_id`, `language_id`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Imagen 1', NULL, NULL, NULL, NULL),
(2, 2, 1, 'Imagen 2', NULL, NULL, NULL, NULL),
(3, 3, 1, 'Imagen 3', NULL, NULL, NULL, NULL),
(4, 4, 1, 'Imagen 4', NULL, NULL, NULL, NULL),
(5, 5, 1, 'nuevo post', NULL, '2019-03-22 20:53:58', '2019-03-22 20:54:37', '2019-03-22 20:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `code`, `name`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'it', 'Italiano', 1, NULL, NULL, NULL),
(2, 'en', 'English', 1, NULL, NULL, NULL),
(3, 'de', 'Tedesco', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `main_slider_items`
--

CREATE TABLE `main_slider_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_slider_items`
--

INSERT INTO `main_slider_items` (`id`, `slug`, `image`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'gt-service', 'images/header/header1.jpg', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `main_slider_item_translations`
--

CREATE TABLE `main_slider_item_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `main_slider_item_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `text_button` text COLLATE utf8mb4_unicode_ci,
  `link_button` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_slider_item_translations`
--

INSERT INTO `main_slider_item_translations` (`id`, `main_slider_item_id`, `language_id`, `title`, `description`, `text_button`, `link_button`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'GT SERVICE', '<p class=\"lead mb-0\">Olbia</p> <p class=\"lead mb-0\">Idraulici ed Elettricisti</p>', 'CONTATTACI', '#contacto', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_30_000001_create_statuses_table', 1),
(4, '2018_07_30_000003_create_languages_table', 1),
(5, '2018_08_02_151858_create_status_translations_table', 1),
(6, '2018_12_21_123848_create_posts_table', 1),
(7, '2018_12_21_131255_create_categories_table', 1),
(8, '2018_12_21_135636_create_tags_table', 1),
(9, '2018_12_21_135855_create_post_tag_table', 1),
(10, '2018_12_30_152750_create_photos_table', 1),
(11, '2018_12_30_200000_create_main_slider_items_table', 1),
(12, '2018_12_30_200001_create_main_slider_item_translations_table', 1),
(13, '2018_12_30_200010_create_offer_slider_items_table', 1),
(14, '2018_12_30_200011_create_offer_slider_item_translations_table', 1),
(15, '2018_12_30_200020_create_business_values_table', 1),
(16, '2018_12_30_200030_create_gallery_slider_items_table', 1),
(17, '2018_12_30_200031_create_gallery_slider_item_translations_table', 1),
(18, '2018_12_30_200040_create_policies_table', 1),
(19, '2018_12_30_200041_create_policy_translations_table', 1),
(20, '2018_12_30_200050_create_page_sections_table', 1),
(21, '2018_12_30_200051_create_page_section_translations_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `offer_slider_items`
--

CREATE TABLE `offer_slider_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_responsive` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_slider_items`
--

INSERT INTO `offer_slider_items` (`id`, `slug`, `image`, `image_responsive`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'invernale', '/storage/7vr2TQ3OoRjPZShpJGOBfnKgf9IjdmBOTWeVLgb3.png', '/storage/ocnqo19fY1V2jeDFYzDWcw91A7FDCFcrlcGoyNGM.png', 1, NULL, '2019-12-10 19:50:28', '2019-12-10 19:50:28'),
(2, 'estate', 'images/gallery/promocion2.jpg', 'images/gallery/promocion2.jpg', 1, NULL, '2019-12-10 20:18:00', '2019-12-10 20:18:00'),
(3, 'promo', '/storage/k3azwzbjeBQd8fiCmz20Q2paXGIXkEjKeEz4zg1d.png', '/storage/FxbkhJOlSVhLn9Cg0v0FdgUIscO4Sncvys6mPgrt.png', 1, '2019-12-10 19:49:29', '2019-12-10 19:50:32', '2019-12-10 19:50:32'),
(4, 'vb', '/storage/qYapA0nURgpKVc4VRcViaZCsoeuxIHWdSN8Uq3Mc.png', '/storage/NxzKWwJrW0pqitprYKCtGwPT5TuaqBoOC8iXHPVS.png', 1, '2019-12-10 19:59:27', '2019-12-10 20:17:18', '2019-12-10 20:17:18'),
(5, 'prueba44', '/storage/HgCbXla6fGt6oD81ENDLNcNybw5DDyh0zmwsLlCQ.jpeg', '/storage/Mh3bBIzlzI3i8tpyslyrWfPv0mO6PbUp8g5TN1vL.png', 1, '2019-12-12 21:10:33', '2019-12-12 21:13:38', '2019-12-12 21:13:38'),
(6, 'tokens', NULL, NULL, 1, '2019-12-13 18:19:36', '2019-12-14 22:10:48', '2019-12-14 22:10:48'),
(7, 'super-promo-paghi-a-rate-fino-a-12-mesi-senza-interessi-e-senza-busta-paga-esito-immediato-serve-solo-il-bancomat-super-offerta-a-installazione-inclusa-gtservice-olbia-co-artema-showroom-viale-aldo-moro-425a-b-07026-olbia-tel-0789-604824-telefono-39-3458377263-offerta-valida-sino-ad-esaurimento-scorte', '/storage/5SOE2kqjmj9qOchSiDmXsI01ml89qPh0Qd9rGyaW.png', '/storage/8UcB0GiwSKtj4tdq3w4EsisJ2INDYfIXBa4q6pBF.png', 1, '2019-12-15 19:41:47', '2019-12-15 19:50:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `offer_slider_item_translations`
--

CREATE TABLE `offer_slider_item_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `offer_slider_item_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `text_button1` text COLLATE utf8mb4_unicode_ci,
  `value_button1` text COLLATE utf8mb4_unicode_ci,
  `text_button2` text COLLATE utf8mb4_unicode_ci,
  `value_button2` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offer_slider_item_translations`
--

INSERT INTO `offer_slider_item_translations` (`id`, `offer_slider_item_id`, `language_id`, `title`, `description`, `text_button1`, `value_button1`, `text_button2`, `value_button2`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Invernale', '<small>Olbia Idraulici ed Elettricisti</small>', '+393458377263', '+393458377263', 'gtserviceolbia@gmail.com', 'gtserviceolbia@gmail.com', NULL, '2019-12-10 19:50:28', '2019-12-10 19:50:28'),
(2, 2, 1, 'Estate', '<small>Olbia Idraulici ed Elettricisti</small>', '', '+393458377263', '', 'gtserviceolbia@gmail.com', NULL, '2019-12-10 20:18:00', '2019-12-10 20:18:00'),
(3, 3, 1, 'promo', NULL, '323232', '32323', '32323', '32323', '2019-12-10 19:49:29', '2019-12-10 19:50:32', '2019-12-10 19:50:32'),
(4, 4, 1, 'vb', NULL, 'jhjh', 'jnjnjn', 'jnjnj', 'jhjhj', '2019-12-10 19:59:27', '2019-12-10 20:17:18', '2019-12-10 20:17:18'),
(5, 5, 1, 'prueba44', NULL, '0412568956', '0412568956', 'info@gtserviceolbia.com', 'info@gtserviceolbia.com', '2019-12-12 21:10:33', '2019-12-12 21:13:38', '2019-12-12 21:13:38'),
(6, 6, 1, 'Tokens', NULL, '2352523423523', '255235345352', 'info@gtserviceolbia.com', 'info@gtserviceolbia.com', '2019-12-13 18:19:36', '2019-12-14 22:10:48', '2019-12-14 22:10:48'),
(7, 7, 1, '⭕️SUPER PROMO PAGHI A RATE FINO A 12 MESI ⭕️Senza interessi e SENZA BUSTA PAGA ⭕️Esito immediato serve solo il bancomat .    SUPER OFFERTA A INSTALLAZIONE INCLUSA.  Gtservice Olbia c/o Artema Showroom Viale Aldo Moro 425/A-B 07026 Olbia tel: 0789 604824  TELEFONO +39 3458377263  Offerta valida sino ad ESAURIMENTO SCORTE', NULL, 'Telefona', '+393458377263', 'scrivi', 'gtserviceolbia@gmail.com', '2019-12-15 19:41:47', '2019-12-15 19:50:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_sections`
--

CREATE TABLE `page_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` text COLLATE utf8mb4_unicode_ci,
  `slug` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_sections`
--

INSERT INTO `page_sections` (`id`, `code`, `slug`, `image`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'chi-siamo', 'chi-siamo', 'images/chisiamo/chisiamo.png', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `page_section_translations`
--

CREATE TABLE `page_section_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_section_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_section_translations`
--

INSERT INTO `page_section_translations` (`id`, `page_section_id`, `language_id`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Chi Siamo', '<p>La nostra azienda ha a disposizione un team di tecnici specializzati, pronti ad intervenire a qualsiasi ora per fornire servizi idraulici di pronto intervento. I nostri operatori valuteranno la situazione in maniera rapida ed efficace in modo da proporre rapidamente la soluzione pi&ugrave; consona ad ovviare al problema.<br />\r\n<br />\r\nLa ditta Gt service di Olbia esegue piccole ristrutturazioni edili in ambito domestico, manutenzione e installazione impianti elettrici, manutenzione e installazione impianti idraulici, sostituzione scaldabagni, rubinetterie, sifoni e sanitari, fornitura e installazione di caldaie e condizionatori.</p>', NULL, '2019-12-14 20:55:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `post_id`, `url`, `created_at`, `updated_at`) VALUES
(1, 1, 'images/especial/slider_1.jpg', '2019-03-22 17:13:09', '2019-03-22 17:13:09'),
(2, 1, 'images/especial/stempi-servizi-digitali.jpg', '2019-03-22 17:13:09', '2019-03-22 17:13:09'),
(3, 2, 'images/especial/slider_3.jpg', '2019-03-22 17:13:09', '2019-03-22 17:13:09'),
(4, 2, 'images/especial/StampeCad-servizio.jpg', '2019-03-22 17:13:09', '2019-03-22 17:13:09'),
(5, 3, 'images/especial/slider_2.jpg', '2019-03-22 17:13:09', '2019-03-22 17:13:09'),
(6, 3, 'images/especial/grafica-e-siti-web-serviziol.jpg', '2019-03-22 17:13:09', '2019-03-22 17:13:09');

-- --------------------------------------------------------

--
-- Table structure for table `policies`
--

CREATE TABLE `policies` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `policies`
--

INSERT INTO `policies` (`id`, `slug`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'policy', 1, NULL, NULL, NULL),
(2, 'terms', 1, NULL, NULL, NULL),
(3, 'cookies', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `policy_translations`
--

CREATE TABLE `policy_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `policy_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `policy_translations`
--

INSERT INTO `policy_translations` (`id`, `policy_id`, `language_id`, `title`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Informativa sulla privacy', '<h4 class=\"sub-header\">GT SERVICE Informativa sulla privacy</h4>\n                    <p>\n                        Questa Informativa sulla privacy descrive come le tue informazioni personali vengono raccolte, utilizzate e condivise quando visiti o effettui un acquisto da http://gtservice.com/ (il \"Sito\").\n                    </p>\n                    <h4 class=\"sub-header\">Informazioni personali raccolte</h4>\n                    <p>\n                        Quando visiti il Sito, raccogliamo automaticamente alcune informazioni sul tuo dispositivo, incluse informazioni sul tuo browser web, indirizzo IP, fuso orario e alcuni dei cookie installati sul tuo dispositivo. Inoltre, mentre navighi sul Sito, raccogliamo informazioni sulle singole pagine web o sui prodotti che visualizzi, su quali siti web o termini di ricerca ti hanno indirizzato al Sito e su come interagisci con il Sito. Ci riferiamo a queste informazioni raccolte automaticamente come \"Informazioni sul dispositivo\".\n                    </p>\n                    <h4 class=\"sub-header\">Raccogliamo informazioni sui dispositivi utilizzando le seguenti tecnologie:</h4>\n                    <p>\n                        - I \"cookie\" sono file di dati che vengono inseriti sul dispositivo o sul computer e spesso includono un identificatore univoco anonimo. Per ulteriori informazioni sui cookie e su come disabilitare i cookie, visitare http://www.allaboutcookies.org. - \"File di registro\" tiene traccia delle azioni che si verificano sul sito e raccolgono dati tra cui indirizzo IP, tipo di browser, provider di servizi Internet, pagine di riferimento / uscita e timbri di data / ora. - \"Web beacon\", \"tag\" e \"pixel\" sono file elettronici utilizzati per registrare informazioni su come si naviga nel sito. Inoltre, quando effettui un acquisto o tenti di effettuare un acquisto tramite il Sito, raccogliamo alcune informazioni da te, tra cui il tuo nome, indirizzo di fatturazione, indirizzo di spedizione, informazioni di pagamento (inclusi numeri di carta di credito), indirizzo email e numero di telefono. Ci riferiamo a queste informazioni come \"Informazioni sull\'Ordine\". Quando parliamo di \"Informazioni personali\" in questa Informativa sulla privacy, stiamo parlando sia di Informazioni dispositivo che di informazioni sull\'ordine. Come usiamo le tue informazioni personali?\n                    </p>\n                    <h4 class=\"sub-header\">Come usiamo le tue informazioni personali?</h4>\n                    <p>\n                        Usiamo le informazioni sull\'ordine che raccogliamo in genere per soddisfare qualsiasi ordine effettuato attraverso il Sito (incluso l\'elaborazione delle informazioni di pagamento, la preparazione della spedizione e la fornitura di fatture e / o conferme d\'ordine). Inoltre, utilizziamo le informazioni dell\'ordine per: Comunicare con voi; Esamina i nostri ordini di potenziali rischi o frodi; e se in linea con le preferenze che hai condiviso con noi, fornisci informazioni o pubblicità relative ai nostri prodotti o servizi. Utilizziamo le Informazioni sul dispositivo che raccogliamo per aiutarci a individuare potenziali rischi e frodi (in particolare il vostro indirizzo IP) e, più in generale, a migliorare e ottimizzare il nostro Sito (ad esempio, generando analisi su come i nostri clienti navigano e interagiscono con il sito e per valutare il successo delle nostre campagne di marketing e pubblicitarie).\n                    </p>\n                    <h4 class=\"sub-header\">Pubblicità comportamentale</h4>\n                    <p>\n                        Come descritto sopra, utilizziamo le vostre informazioni personali per fornirvi pubblicità mirate o comunicazioni di marketing che riteniamo possano essere di vostro interesse. Per ulteriori informazioni su come funziona la pubblicità mirata, è possibile visitare la pagina educativa Network Advertising Initiative (\"NAI\") all\'indirizzo http://www.networkadvertising.org/understanding-online-advertising/how-does-it-work. Inoltre, puoi disattivare alcuni di questi servizi visitando il portale di opt-out di Digital Advertising Alliance all\'indirizzo: http://optout.aboutads.info/.</p>\n                    <h4 class=\"sub-header\">Non tracciare</h4>\n                    <p>\n                        Si prega di notare che non alteriamo la raccolta dei dati del nostro sito e le pratiche di utilizzo quando vediamo un segnale Do Not Track dal browser.\n                    </p>\n                    <h4 class=\"sub-header\">I tuoi diritti</h4>\n                    <p>\n                        Se sei residente in Europa, hai il diritto di accedere alle informazioni personali che deteniamo su di te e di chiedere che le tue informazioni personali siano corrette, aggiornate o cancellate. Se desideri esercitare questo diritto, ti preghiamo di contattarci attraverso le informazioni di contatto qui sotto. Inoltre, se sei residente in Europa, notiamo che stiamo elaborando le tue informazioni al fine di adempiere ai contratti che potremmo avere con te (ad esempio se fai un ordine attraverso il Sito), o comunque per perseguire i nostri legittimi interessi commerciali sopra elencati. Inoltre, ti preghiamo di notare che le tue informazioni saranno trasferite al di fuori dell\'Europa, incluso in Canada e negli Stati Uniti.\n                    </p>\n                    <h4 class=\"sub-header\">Conservazione dei dati</h4>\n                    <p>\n                        Quando effettui un ordine attraverso il Sito, manterremo le informazioni sull\'Ordine per i nostri archivi a meno che e fino a quando non ci chiedi di eliminare queste informazioni.\n                    </p>\n                    <h4 class=\"sub-header\">I minorenni</h4>\n                    <p>\n                        Il sito non è destinato a persone di età inferiore ai 15 anni.\n                    </p>\n                    <h4 class=\"sub-header\">I cambiamenti</h4>\n                    <p>\n                        Potremmo aggiornare la presente informativa sulla privacy di volta in volta al fine di riflettere, ad esempio, modifiche alle nostre pratiche o per altri motivi operativi, legali o normativi.\n                    </p>\n                    <h4 class=\"sub-header\">Contattaci</h4>\n                    <p>\n                        Per ulteriori informazioni sulle nostre pratiche sulla privacy, se avete domande, o se volete fare un reclamo, vi preghiamo di contattarci via e-mail a\n                        <a href=\"mailto:gtserviceolbia@gmail.com\" target=\"_blank\">\n                            gtserviceolbia@gmail.com\n                        </a>\n                </p>', NULL, NULL, NULL),
(2, 2, 1, 'Termini', '<h4 class=\"sub-header\">Panoramica</h4>\n                    <p>\n                        Questo sito è gestito da GT SERVICE. In tutto il sito, i termini \"noi\", \"noi\" e \"nostro\" si riferiscono a GT SERVICE. GT SERVICE offers this website, incluse tutte le informazioni, gli strumenti e i servizi disponibili da questo sito all\'utente, condizionati all\'accettazione da parte dell\'utente di tutti i termini, condizioni, politiche e comunicazioni qui indicati. Visitando il nostro sito e / o acquistando qualcosa da noi, ti impegni nel nostro \"Servizio\" e accetti di essere vincolato dai seguenti termini e condizioni (\"Termini di servizio\", \"Termini\"), compresi i termini e le condizioni supplementari e le politiche di cui al presente documento e / o disponibile tramite collegamento ipertestuale. I presenti Termini di servizio si applicano a tutti gli utenti del sito, compresi, a titolo esemplificativo, gli utenti che sono browser, fornitori, clienti, commercianti e / o contributori di contenuti. Leggere attentamente questi Termini di servizio prima di accedere o utilizzare il nostro sito Web. Accedendo o utilizzando qualsiasi parte del sito, l\'utente accetta di essere vincolato da questi Termini di servizio. Se non si accettano tutti i termini e le condizioni del presente accordo, non è possibile accedere al sito Web o utilizzare alcun servizio. Se questi Termini di servizio sono considerati un\'offerta, l\'accettazione è espressamente limitata ai presenti Termini di servizio. Eventuali nuove funzionalità o strumenti aggiunti al negozio corrente saranno anch\'essi soggetti ai Termini di servizio. Puoi rivedere la versione più aggiornata dei Termini di servizio in qualsiasi momento su questa pagina. Ci riserviamo il diritto di aggiornare, modificare o sostituire qualsiasi parte di questi Termini di servizio pubblicando aggiornamenti e / o modifiche al nostro sito web. È tua responsabilità controllare periodicamente questa pagina per le modifiche. L\'uso continuato o l\'accesso al sito Web dopo la pubblicazione di qualsiasi modifica costituisce accettazione di tali modifiche. Il nostro negozio è ospitato su Shopify Inc. Ci forniscono la piattaforma di e-commerce online che ci consente di vendere i nostri prodotti e servizi.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 1 - Termini del negozio online</h4>\n                    <p>\n                        Accettando questi Termini di servizio, dichiari di avere almeno la maggiore età nel tuo stato o provincia di residenza, o di avere la maggiore età nel tuo stato o provincia di residenza e ci hai dato il tuo consenso a consentire a tutti i minori dipendenti di utilizzare questo sito. L\'utente non può utilizzare i nostri prodotti per scopi illegali o non autorizzati né può, nell\'uso del Servizio, violare alcuna legge nella propria giurisdizione (incluse, a titolo esemplificativo ma non esaustivo, le leggi sul copyright). Non è necessario trasmettere worm o virus o codici di natura distruttiva. Una violazione o una violazione di uno dei Termini comporterà la cessazione immediata dei Servizi.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 2 - Condizioni generali</h4>\n                    <p>\n                        Ci riserviamo il diritto di rifiutare il servizio a chiunque, per qualsiasi motivo, in qualsiasi momento. Comprendi che i tuoi contenuti (non compresi i dati della carta di credito), possono essere trasferiti in chiaro e implicano (a) trasmissioni su varie reti; e (b) modifiche per conformarsi e adattarsi ai requisiti tecnici delle reti o dei dispositivi di collegamento. Le informazioni sulla carta di credito vengono sempre crittografate durante il trasferimento su reti. Accetti di non riprodurre, duplicare, copiare, vendere, rivendere o sfruttare alcuna parte del Servizio, l\'uso del Servizio, o l\'accesso al Servizio o qualsiasi contatto sul sito web attraverso il quale il servizio è fornito, senza espressa autorizzazione scritta da parte nostra . I titoli utilizzati nel presente accordo sono inclusi solo per comodità e non limiteranno o influenzeranno in alcun modo le presenti Condizioni.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 3 - Accuratezza, completezza e tempestività delle informazioni</h4>\n                    <p>\n                        Non siamo responsabili se le informazioni rese disponibili su questo sito non sono accurate, complete o attuali. Il materiale su questo sito è fornito solo per informazione generale e non dovrebbe essere invocato o utilizzato come unica base per prendere decisioni senza consultare fonti di informazione primarie, più accurate, più complete o più tempestive. Qualsiasi affidamento sul materiale in questo sito è a tuo rischio. Questo sito può contenere alcune informazioni storiche. Le informazioni storiche, necessariamente, non sono aggiornate e vengono fornite solo come riferimento. Ci riserviamo il diritto di modificare i contenuti di questo sito in qualsiasi momento, ma non abbiamo l\'obbligo di aggiornare alcuna informazione sul nostro sito. Accetti che è tua responsabilità monitorare le modifiche al nostro sito.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 4 - Modifiche al servizio e ai prezzi</h4>\n                    <p>\n                        I prezzi dei nostri prodotti sono soggetti a modifiche senza preavviso. Ci riserviamo il diritto in qualsiasi momento di modificare o interrompere il Servizio (o qualsiasi parte o contenuto dello stesso) senza preavviso in qualsiasi momento. Non saremo responsabili nei confronti dell\'utente o di terzi per eventuali modifiche, variazioni di prezzo, sospensione o interruzione del servizio.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 5 - Prodotti o servizi (se applicabile)</h4> Alcuni prodotti o servizi potrebbero essere disponibili esclusivamente online attraverso il sito web. Questi prodotti o servizi possono avere quantità limitate e sono soggetti a restituzione o sostituzione solo in base alla nostra Politica di restituzione. Abbiamo fatto ogni sforzo per visualizzare nel modo più accurato possibile i colori e le immagini dei nostri prodotti che appaiono nel negozio. Non possiamo garantire che il display del monitor del computer di qualsiasi colore sarà accurato. Ci riserviamo il diritto, ma non siamo obbligati, di limitare le vendite dei nostri prodotti o servizi a qualsiasi persona, area geografica o giurisdizione. Potremmo esercitare questo diritto caso per caso. Ci riserviamo il diritto di limitare le quantità di prodotti o servizi che offriamo. Tutte le descrizioni dei prodotti o dei prezzi dei prodotti sono soggette a modifiche in qualsiasi momento e senza preavviso, a nostra esclusiva discrezione. Ci riserviamo il diritto di interrompere qualsiasi prodotto in qualsiasi momento. Qualsiasi offerta per qualsiasi prodotto o servizio fatto su questo sito è nulla ove proibito. Non garantiamo che la qualità di qualsiasi prodotto, servizio, informazione o altro materiale acquistato o ottenuto da voi soddisferà le vostre aspettative o che eventuali errori nel Servizio verranno corretti.\n                    <p></p>\n                    <h4 class=\"sub-header\">Sezione 6 - Precisione della fatturazione e informazioni sull\'account</h4>\n                    <p>\n                        Ci riserviamo il diritto di rifiutare qualsiasi ordine effettuato da noi. Potremmo, a nostra esclusiva discrezione, limitare o annullare le quantità acquistate a persona, per nucleo familiare o per ordine. Queste restrizioni possono includere gli ordini effettuati da o sotto lo stesso account cliente, la stessa carta di credito e / o ordini che utilizzano lo stesso indirizzo di fatturazione e / o spedizione. Nel caso in cui apportiamo una modifica o annulliamo un ordine, possiamo tentare di notificarlo contattando l\'indirizzo e-mail e / o l\'indirizzo di fatturazione / numero di telefono forniti al momento dell\'ordine. Ci riserviamo il diritto di limitare o vietare ordini che, a nostro insindacabile giudizio, sembrano essere posti da rivenditori, rivenditori o distributori. Accetti di fornire informazioni di acquisto e di conto correnti, complete e accurate per tutti gli acquisti effettuati nel nostro negozio. Accetti di aggiornare tempestivamente il tuo account e altre informazioni, incluso il tuo indirizzo e-mail, i numeri di carta di credito e le date di scadenza, in modo che possiamo completare le tue transazioni e contattarti secondo necessità. Per maggiori dettagli, si prega di rivedere la nostra politica di restituzione. For more detail, please review our Returns Policy.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 7 - Strumenti opzionali</h4>\n                    <p>\n                        Potremmo fornirti l\'accesso a strumenti di terze parti sui quali non controlliamo né abbiamo alcun controllo o input. Riconosci e accetti che forniamo l\'accesso a tali strumenti \"così come sono\" e \"come disponibili\" senza garanzie, dichiarazioni o condizioni di alcun tipo e senza alcuna approvazione. Non avremo alcuna responsabilità derivante da o relativa al tuo utilizzo di strumenti di terze parti opzionali. Qualsiasi utilizzo da parte dell\'utente degli strumenti opzionali offerti attraverso il sito è interamente a proprio rischio e discrezione e si dovrebbe garantire di conoscere e approvare i termini su quali strumenti sono forniti dai relativi fornitori di terze parti. Potremmo anche, in futuro, offrire nuovi servizi e / o funzionalità attraverso il sito web (incluso il rilascio di nuovi strumenti e risorse). Tali nuove funzionalità e / o servizi saranno inoltre soggetti alle presenti Condizioni di servizio.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 8 - Link di terze parti</h4>\n                    <p>\n                        Alcuni contenuti, prodotti e servizi disponibili tramite il nostro Servizio possono includere materiali di terze parti. I link di terze parti su questo sito possono indirizzarti a siti Web di terze parti che non sono affiliati con noi. Non siamo responsabili per l\'esame o la valutazione del contenuto o dell\'accuratezza e non garantiamo e non avremo alcuna responsabilità o responsabilità per materiali o siti Web di terzi o per altri materiali, prodotti o servizi di terze parti. Non siamo responsabili di alcun danno o danno relativo all\'acquisto o all\'utilizzo di beni, servizi, risorse, contenuti o qualsiasi altra transazione effettuata in relazione a siti Web di terzi. Rivedi attentamente le politiche e le pratiche di terze parti e assicurati di averle comprese prima di intraprendere qualsiasi transazione. Reclami, reclami, dubbi o domande relative a prodotti di terzi devono essere indirizzati a terze parti.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 9 - Commenti degli utenti, feedback e altri contributi</h4>\n                    <p>\n                        Se, su nostra richiesta, invii determinati invii specifici (ad esempio voci di concorso) o senza una nostra richiesta, invii idee creative, suggerimenti, proposte, piani o altro materiale, sia online, per email, per posta o in altro modo (collettivamente, \"commenti\"), l\'utente accetta che possiamo, in qualsiasi momento, senza limitazioni, modificare, copiare, pubblicare, distribuire, tradurre e altrimenti utilizzare in qualsiasi supporto i commenti che ci inoltrate. Siamo e non abbiamo alcun obbligo (1) di mantenere alcun commento in confidenza; (2) pagare un risarcimento per eventuali commenti; o (3) per rispondere a qualsiasi commento. Potremmo, ma non abbiamo alcun obbligo, monitorare, modificare o rimuovere contenuti che determiniamo a nostra esclusiva discrezione illeciti, offensivi, minacciosi, calunniosi, diffamatori, pornografici, osceni o altrimenti discutibili o che violino la proprietà intellettuale di qualsiasi parte o questi Termini di servizio . Accetti che i tuoi commenti non violino alcun diritto di terze parti, inclusi copyright, marchio, privacy, personalità o altri diritti personali o proprietari. Accetti inoltre che i tuoi commenti non contengano materiale diffamatorio o altrimenti illegale, offensivo o osceno, o contengano virus informatici o altri malware che potrebbero in qualche modo compromettere il funzionamento del Servizio o di qualsiasi sito web correlato. Non è possibile utilizzare un falso indirizzo e-mail, fingere di essere qualcuno diverso da te o altrimenti indurre in errore noi o terze parti circa l\'origine di eventuali commenti. Sei l\'unico responsabile per i commenti che fai e la loro accuratezza. Non ci assumiamo alcuna responsabilità e non ci assumiamo alcuna responsabilità per eventuali commenti pubblicati dall\'utente o da terze parti.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 10 - Informazioni personali</h4>\n                    <p>\n                        L\'invio di informazioni personali tramite questo sito Web è regolato dal presente documento che rappresenta la nostra attuale Informativa sulla privacy.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 11 - Errori, inesattezze e omissioni</h4>\n                    <p>\n                        Occasionalmente possono esserci informazioni sul nostro sito o nel Servizio che contengono errori tipografici, inesattezze o omissioni che possono riguardare descrizioni di prodotti, prezzi, promozioni, offerte, spese di spedizione del prodotto, tempi di transito e disponibilità. Ci riserviamo il diritto di correggere eventuali errori, inesattezze o omissioni e di modificare o aggiornare le informazioni o annullare gli ordini se qualsiasi informazione nel Servizio o su qualsiasi sito Web correlato è inaccurata in qualsiasi momento senza preavviso (anche dopo aver inviato l\'ordine) . Non ci assumiamo alcun obbligo di aggiornare, modificare o chiarire le informazioni nel Servizio o su qualsiasi sito Web correlato, incluse, senza limitazioni, informazioni sui prezzi, ad eccezione di quanto richiesto dalla legge. Nessun aggiornamento specifico o data di aggiornamento applicata nel Servizio o su qualsiasi sito Web correlato deve essere presa per indicare che tutte le informazioni nel Servizio o su qualsiasi sito Web correlato sono state modificate o aggiornate.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 12 - Usi proibiti</h4>\n                    <p>\n                        Oltre agli altri divieti stabiliti nei Termini di servizio, è vietato l\'uso del sito o del suo contenuto: (a) per scopi illeciti; (b) sollecitare altri a compiere o partecipare a atti illeciti; (c) violare regolamenti, regole, leggi o ordinanze locali internazionali, federali, provinciali o statali; (d) violare o violare i nostri diritti di proprietà intellettuale o i diritti di proprietà intellettuale di terzi; (e) molestare, abusare, insultare, danneggiare, diffamare, diffamare, denigrare, intimidire o discriminare in base al genere, all\'orientamento sessuale, alla religione, all\'etnia, alla razza, all\'età, all\'origine nazionale o alla disabilità; (f) fornire informazioni false o fuorvianti; (g) caricare o trasmettere virus o qualsiasi altro tipo di codice dannoso che possa o possa essere utilizzato in qualsiasi modo che possa influenzare la funzionalità o il funzionamento del Servizio o di qualsiasi sito Web correlato, altri siti Web o Internet; (h) per raccogliere o tracciare le informazioni personali di terzi; (i) spam, phish, pharm, pretesto, spider, crawl o scrap; (j) per qualsiasi scopo osceno o immorale; o (k) per interferire con o eludere le funzionalità di sicurezza del Servizio o qualsiasi sito Web correlato, altri siti Web o Internet. Ci riserviamo il diritto di interrompere l\'utilizzo del Servizio o di qualsiasi sito Web correlato per violare uno degli usi vietati.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 13 - Limitazione di responsabilità</h4>\n                    <p>\n                        Non garantiamo, dichiariamo o garantiamo che l\'utilizzo del nostro servizio sarà ininterrotto, tempestivo, sicuro o privo di errori. Non garantiamo che i risultati che possono essere ottenuti dall\'uso del servizio saranno accurati o affidabili. Accetti che, di volta in volta, possiamo rimuovere il servizio per periodi di tempo indefiniti o annullare il servizio in qualsiasi momento, senza preavviso. L\'utente accetta espressamente che l\'utilizzo o l\'impossibilità di utilizzare il servizio è a proprio rischio. Il servizio e tutti i prodotti e servizi forniti all\'utente tramite il servizio sono (salvo quanto espressamente dichiarato da noi) forniti \"come sono\" e \"come disponibili\" per l\'uso dell\'utente, senza alcuna dichiarazione, garanzia o condizione di alcun tipo, espressa o implicita, comprese tutte le garanzie implicite o condizioni di commerciabilità, qualità commerciabile, idoneità per uno scopo particolare, durata, titolo e non violazione. In nessun caso GT SERVICE, i nostri amministratori, funzionari, dipendenti, affiliati, agenti, appaltatori, stagisti, fornitori, fornitori di servizi o licenziatari saranno ritenuti responsabili per eventuali lesioni, perdite, reclami o qualsiasi azione diretta, indiretta, incidentale, punitiva, speciale , o danni conseguenti di qualsiasi tipo, inclusi, a titolo esemplificativo, mancati profitti, mancati guadagni, risparmi persi, perdita di dati, costi di sostituzione o altri danni simili, siano essi derivanti da contratto, illecito civile (inclusa negligenza), responsabilità oggettiva o altro dall\'utilizzo di qualsiasi servizio o di qualsiasi prodotto acquistato utilizzando il servizio, o per qualsiasi altra rivendicazione relativa in qualsiasi modo all\'utilizzo del servizio o di qualsiasi prodotto, compresi, a titolo esemplificativo, errori o omissioni in qualsiasi contenuto, o qualsiasi perdita o danno di qualsiasi tipo derivante dall\'uso del servizio o di qualsiasi contenuto (o prodotto) pubblicato, trasmesso o reso altrimenti disponibile tramite il servizio, anche se consigliato. Poiché alcuni stati o giurisdizioni non consentono l\'esclusione o la limitazione di responsabilità per danni conseguenti o incidentali, in tali stati o giurisdizioni, la nostra responsabilità è limitata alla massima estensione consentita dalla legge\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 14 – Indennizzo</h4>\n                    <p>\n                        Accetti di indennizzare, difendere e tenere indenne GT SERVICE e i nostri genitori, sussidiarie, affiliate, partner, funzionari, direttori, agenti, appaltatori, licenzianti, fornitori di servizi, subappaltatori, fornitori, stagisti e dipendenti, inoffensivi da qualsiasi richiesta o richiesta, incluse le spese legali ragionevoli, fatte da terze parti a causa o derivanti dalla violazione delle presenti Condizioni di servizio o dei documenti che incorporano per riferimento, o la violazione di qualsiasi legge o dei diritti di terzi.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 15 – Separabilità</h4>\n                    <p>\n                        Nel caso in cui qualsiasi disposizione dei presenti Termini di servizio sia ritenuta illegale, nulla o inapplicabile, tale disposizione sarà comunque applicabile nella misura massima consentita dalla legge applicabile, e la parte inapplicabile sarà considerata come recisa dai presenti Termini di Servizio, tale determinazione non pregiudica la validità e l\'applicabilità di altre disposizioni rimanenti.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 16 – Risoluzione</h4>\n                    <p>\n                        Gli obblighi e le responsabilità delle parti sostenute prima della data di scadenza sopravvivono alla risoluzione del presente accordo a tutti gli effetti. I presenti Termini di servizio sono efficaci a meno che e fino alla risoluzione da parte nostra o di noi. È possibile risolvere i presenti Termini di servizio in qualsiasi momento informandoci che non si desidera più utilizzare i nostri Servizi o quando si interrompe l\'utilizzo del nostro sito. Se a nostro insindacabile giudizio fallisci, o sospettiamo che tu abbia fallito, di rispettare qualsiasi termine o disposizione dei presenti Termini di servizio, potremmo anche rescindere il presente accordo in qualsiasi momento senza preavviso e rimarrai responsabile per tutte le somme dovute fino alla data di cessazione e / o di conseguenza può negare l\'accesso ai nostri Servizi (o parte di essi).\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 17 - Intero accordo</h4>\n                    <p>\n                        L\'incapacità di esercitare o far valere qualsiasi diritto o disposizione di questi Termini di servizio non costituisce una rinuncia a tale diritto o disposizione. I presenti Termini di servizio e le eventuali politiche o regole operative pubblicate da noi su questo sito o in relazione al Servizio costituiscono l\'intero accordo e intesa tra l\'utente e noi e disciplinano l\'utilizzo del Servizio, sostituendo qualsiasi accordo, comunicazione e proposta precedente o contemporanea. , sia orale che scritto, tra te e noi (incluse, ma non limitate a, eventuali versioni precedenti dei Termini di servizio). Eventuali ambiguità nell\'interpretazione di questi Termini di servizio non devono essere interpretate contro la parte redazionale.\n                    </p>\n                    <h4 class=\"sub-header\">Section 18 - Legge applicabile</h4>\n                    <p>\n\n                        I presenti Termini di servizio e gli eventuali accordi separati in base ai quali vi forniamo i Servizi saranno disciplinati e interpretati in conformità con le leggi di Loc.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 19 - Modifiche ai Termini di servizio</h4>\n                    <p>\n                        È possibile rivedere la versione più recente dei Termini di servizio in qualsiasi momento su questa pagina. Ci riserviamo il diritto, a nostra esclusiva discrezione, di aggiornare, modificare o sostituire qualsiasi parte di questi Termini di servizio pubblicando aggiornamenti e modifiche al nostro sito web. È responsabilità dell\'utente controllare periodicamente il nostro sito Web per eventuali modifiche. L\'uso continuato o l\'accesso al nostro sito Web o al Servizio in seguito alla pubblicazione di eventuali modifiche ai presenti Termini di servizio costituisce accettazione di tali modifiche.\n                    </p>\n                    <h4 class=\"sub-header\">Sezione 20 - Informazioni di contatto</h4>\n                    <p>\n                        Le domande sui Termini di servizio devono essere inviate a\n                        <a href=\"mailto:gtserviceolbia@gmail.com\" target=\"_blank\">gtserviceolbia@gmail.com\n                        </a>\n                </p>', NULL, NULL, NULL),
(3, 3, 1, 'Politica sui cookie', '<h4 class=\"sub-header\">GT SERVICE Politica sui cookie</h4>\n                    <p>\n                        Questa Informativa sulla privacy descrive come le vostre informazioni personali vengono raccolte, utilizzate e condivise quando visitate o effettuate un acquisto da http://gtservice.com/ (il \"Sito\").\n                    </p>\n                    <h4 class=\"sub-header\">Informazioni personali We Collec</h4>\n                    <p>\n                        Quando visiti il Sito, raccogliamo automaticamente alcune informazioni sul tuo dispositivo, incluse informazioni sul tuo browser web, indirizzo IP, fuso orario e alcuni dei cookie installati sul tuo dispositivo. Inoltre, mentre navighi sul Sito, raccogliamo informazioni sulle singole pagine web o sui prodotti che visualizzi, su quali siti web o termini di ricerca ti hanno indirizzato al Sito e su come interagisci con il Sito. Ci riferiamo a queste informazioni raccolte automaticamente come \"Informazioni sul dispositivo\".\n                    </p>\n                    <h4 class=\"sub-header\">Raccogliamo informazioni sui dispositivi utilizzando le seguenti tecnologie:</h4>\n                    <p>\n                        - I \"cookie\" sono file di dati che vengono inseriti sul dispositivo o sul computer e spesso includono un identificatore univoco anonimo. Per ulteriori informazioni sui cookie e su come disabilitare i cookie, visitare http://www.allaboutcookies.org. - \"File di registro\" tiene traccia delle azioni che si verificano sul sito e raccolgono dati tra cui indirizzo IP, tipo di browser, provider di servizi Internet, pagine di riferimento / uscita e timbri di data / ora. - \"Web beacon\", \"tag\" e \"pixel\" sono file elettronici utilizzati per registrare informazioni su come si naviga nel sito. Inoltre, quando effettui un acquisto o tenti di effettuare un acquisto tramite il Sito, raccogliamo alcune informazioni da te, tra cui il tuo nome, indirizzo di fatturazione, indirizzo di spedizione, informazioni di pagamento (inclusi numeri di carta di credito), indirizzo email e numero di telefono. Ci riferiamo a queste informazioni come \"Informazioni sull\'Ordine\". Quando parliamo di \"Informazioni personali\" in questa Informativa sulla privacy, stiamo parlando sia di Informazioni dispositivo che di informazioni sull\'ordine.\n                    </p>\n                    <h4 class=\"sub-header\">Come usiamo le tue informazioni personali?</h4>\n                    <p>Usiamo le informazioni sull\'ordine che raccogliamo in genere per soddisfare qualsiasi ordine effettuato attraverso il Sito (incluso l\'elaborazione delle informazioni di pagamento, la preparazione della spedizione e la fornitura di fatture e / o conferme d\'ordine). Inoltre, utilizziamo le informazioni dell\'ordine per: Comunicare con voi; Esamina i nostri ordini di potenziali rischi o frodi; e se in linea con le preferenze che hai condiviso con noi, fornisci informazioni o pubblicità relative ai nostri prodotti o servizi. Utilizziamo le Informazioni sul dispositivo che raccogliamo per aiutarci a individuare potenziali rischi e frodi (in particolare il vostro indirizzo IP) e, più in generale, a migliorare e ottimizzare il nostro Sito (ad esempio, generando analisi su come i nostri clienti navigano e interagiscono con il sito e per valutare il successo delle nostre campagne di marketing e pubblicitarie).\n                    </p>\n                    <h4 class=\"sub-header\">Pubblicità comportamentale</h4>\n                    <p>\n                        Come descritto sopra, utilizziamo le vostre informazioni personali per fornirvi pubblicità mirate o comunicazioni di marketing che riteniamo possano essere di vostro interesse. Per ulteriori informazioni su come funziona la pubblicità mirata, è possibile visitare la pagina educativa Network Advertising Initiative (\"NAI\") all\'indirizzo http://www.networkadvertising.org/understanding-online-advertising/how-does-it-work. Inoltre, puoi disattivare alcuni di questi servizi visitando il portale di opt-out di Digital Advertising Alliance all\'indirizzo: http://optout.aboutads.info/.\n                    </p>\n                    <h4 class=\"sub-header\">Non tracciare</h4>\n                    <p>\n                        Si prega di notare che non alteriamo la raccolta dei dati del nostro sito e le pratiche di utilizzo quando vediamo un segnale Do Not Track dal browser.\n                    </p>\n                    <h4 class=\"sub-header\">I tuoi diritti</h4>\n                    <p>\n                        Se sei residente in Europa, hai il diritto di accedere alle informazioni personali che deteniamo su di te e di chiedere che le tue informazioni personali siano corrette, aggiornate o cancellate. Se desideri esercitare questo diritto, ti preghiamo di contattarci attraverso le informazioni di contatto qui sotto. Inoltre, se sei residente in Europa, notiamo che stiamo elaborando le tue informazioni al fine di adempiere ai contratti che potremmo avere con te (ad esempio se fai un ordine attraverso il Sito), o comunque per perseguire i nostri legittimi interessi commerciali sopra elencati. Inoltre, ti preghiamo di notare che le tue informazioni saranno trasferite al di fuori dell\'Europa, incluso in Canada e negli Stati Uniti.\n                    </p>\n                    <h4 class=\"sub-header\">Conservazione dei dati</h4>\n                    <p>\n                        Quando effettui un ordine attraverso il Sito, manterremo le informazioni sull\'Ordine per i nostri archivi a meno che e fino a quando non ci chiedi di eliminare queste informazioni.\n                    </p>\n                    <h4 class=\"sub-header\">I minorenni</h4>\n                    <p>\n                        Il sito non è destinato a persone di età inferiore ai 15 anni.\n                    </p>\n                    <h4 class=\"sub-header\">I cambiamenti</h4>\n                    <p>\n                        Potremmo aggiornare la presente informativa sulla privacy di volta in volta al fine di riflettere, ad esempio, modifiche alle nostre pratiche o per altri motivi operativi, legali o normativi.\n                    </p>\n                    <h4 class=\"sub-header\">Contattaci</h4>\n                    <p>\n                        Per ulteriori informazioni sulle nostre pratiche sulla privacy, se avete domande, o se volete fare un reclamo, vi preghiamo di contattarci via e-mail a gtservice.com oo via mail utilizzando i dettagli forniti di seguito: Loc. Spiaggia Bianca Golfo Aranci, Sardegna, Italy\n                </p>', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` mediumtext COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `published_at` timestamp NULL DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `url`, `excerpt`, `body`, `published_at`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'IMPIANTI ELETTRICI', 'impianti-elettrici', 'IMPIANTI ELETTRICI OLBIA', '<p>Realizziamo impianti elettrici civili (edilizia residenziale e commerciale) per nuove abitazioni o ristrutturando impianti fuori norma.</p>', '2019-03-18 23:00:00', 1, '2019-03-22 17:13:09', '2019-12-14 20:51:08'),
(2, 'IMPIANTI IDRAULICI', 'impianti-idraulici', 'IMPIANTI IDRAULICI OLBIA', '<p>Siamo in grado di realizzare impianti idraulici a Olbia di qualunque dimensione, raggiungiamo facilmente clienti in tutta la Sardegna ma sopratutto nelle aree limitrofi.</p>', '2019-03-19 23:00:00', 1, '2019-03-22 17:13:09', '2019-12-14 20:45:25'),
(3, 'MANUTENZIONI', 'manutenzioni', 'MANUTENZIONI OLBIA', '<p>Siamo specializzati e svolgiamo servizi di manutenzione ordinaria e straordinaria per privati, condomini e pubblica amministrazione.</p>', '2019-03-20 23:00:00', 1, '2019-03-22 17:13:09', '2019-12-14 20:45:50');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 2, NULL, NULL),
(3, 3, 3, NULL, NULL),
(4, 1, 1, NULL, NULL),
(5, 2, 2, NULL, NULL),
(6, 3, 3, NULL, NULL),
(7, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ACT', NULL, NULL, NULL),
(2, 'INA', NULL, NULL, NULL),
(3, 'PPP', NULL, NULL, NULL),
(4, 'FAC', NULL, NULL, NULL),
(5, 'REV', NULL, NULL, NULL),
(6, 'UNR', NULL, NULL, NULL),
(7, 'REA', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status_translations`
--

CREATE TABLE `status_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status_translations`
--

INSERT INTO `status_translations` (`id`, `status_id`, `language_id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Attivo', 'Elemento attivo', NULL, NULL, NULL),
(2, 2, 1, 'Inattivo', 'Elemento inattivo', NULL, NULL, NULL),
(3, 3, 1, 'In attesa di Pagamento', 'In attesa di Pagamento', NULL, NULL, NULL),
(4, 4, 1, 'Prenotato', 'Prenotato', NULL, NULL, NULL),
(5, 5, 1, 'Annullato', 'Annullato', NULL, NULL, NULL),
(6, 6, 1, 'Non letto', 'Contatto non letto', NULL, NULL, NULL),
(7, 7, 1, 'Letto', 'Contatto letto', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'etiqueta 1', '2019-03-22 17:13:09', '2019-03-22 17:13:09'),
(2, 'etiqueta 3', '2019-03-22 17:13:09', '2019-03-22 17:13:09'),
(3, 'etiqueta 4', '2019-03-22 17:13:09', '2019-03-22 17:13:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Giovanni Tronci', 'info@gtserviceolbia.com', NULL, '$2y$10$IpJSoloLQzkFmuwfdFH.9.Qlx1RP3AG2/hGJMQOjfRx1pd/H8ZkDq', 'EwKw4gLl5teTNehVHvH2VEw6Utwe9NJAfbgCqYhOkDajflfa99cc9HlsHDMD', '2019-03-22 17:13:09', '2019-12-13 18:18:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business_values`
--
ALTER TABLE `business_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `business_values_status_id_foreign` (`status_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_slider_items`
--
ALTER TABLE `gallery_slider_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallery_slider_items_status_id_foreign` (`status_id`);

--
-- Indexes for table `gallery_slider_item_translations`
--
ALTER TABLE `gallery_slider_item_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallery_slider_item_translations_gallery_slider_item_id_foreign` (`gallery_slider_item_id`),
  ADD KEY `gallery_slider_item_translations_language_id_foreign` (`language_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `languages_status_id_foreign` (`status_id`);

--
-- Indexes for table `main_slider_items`
--
ALTER TABLE `main_slider_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_slider_items_status_id_foreign` (`status_id`);

--
-- Indexes for table `main_slider_item_translations`
--
ALTER TABLE `main_slider_item_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_slider_item_translations_main_slider_item_id_foreign` (`main_slider_item_id`),
  ADD KEY `main_slider_item_translations_language_id_foreign` (`language_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_slider_items`
--
ALTER TABLE `offer_slider_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offer_slider_items_status_id_foreign` (`status_id`);

--
-- Indexes for table `offer_slider_item_translations`
--
ALTER TABLE `offer_slider_item_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offer_slider_item_translations_offer_slider_item_id_foreign` (`offer_slider_item_id`),
  ADD KEY `offer_slider_item_translations_language_id_foreign` (`language_id`);

--
-- Indexes for table `page_sections`
--
ALTER TABLE `page_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_sections_status_id_foreign` (`status_id`);

--
-- Indexes for table `page_section_translations`
--
ALTER TABLE `page_section_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_section_translations_page_section_id_foreign` (`page_section_id`),
  ADD KEY `page_section_translations_language_id_foreign` (`language_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policies`
--
ALTER TABLE `policies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `policies_status_id_foreign` (`status_id`);

--
-- Indexes for table `policy_translations`
--
ALTER TABLE `policy_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `policy_translations_policy_id_foreign` (`policy_id`),
  ADD KEY `policy_translations_language_id_foreign` (`language_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_translations`
--
ALTER TABLE `status_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_translations_status_id_foreign` (`status_id`),
  ADD KEY `status_translations_language_id_foreign` (`language_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business_values`
--
ALTER TABLE `business_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gallery_slider_items`
--
ALTER TABLE `gallery_slider_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gallery_slider_item_translations`
--
ALTER TABLE `gallery_slider_item_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `main_slider_items`
--
ALTER TABLE `main_slider_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `main_slider_item_translations`
--
ALTER TABLE `main_slider_item_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `offer_slider_items`
--
ALTER TABLE `offer_slider_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `offer_slider_item_translations`
--
ALTER TABLE `offer_slider_item_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `page_sections`
--
ALTER TABLE `page_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `page_section_translations`
--
ALTER TABLE `page_section_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `policies`
--
ALTER TABLE `policies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `policy_translations`
--
ALTER TABLE `policy_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `status_translations`
--
ALTER TABLE `status_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
