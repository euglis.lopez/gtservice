<div ref="cookiealert" class="alert text-center cookiealert" role="alert">
    Questo sito Web utilizza i cookie per offrirti la migliore esperienza utente
    <button @click="setCookieLaw" type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
        <b>Accetto</b> le politiche sui cookie
    </button>
</div>