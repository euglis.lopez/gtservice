<?php

namespace App\Models\Admin;

use App\Models\Admin\Language;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OfferSliderItem
 * @package App\Models\Admin
 * @version February 20, 2019, 3:29 pm UTC
 *
 * @property \App\Models\Admin\Status status
 * @property \Illuminate\Database\Eloquent\Collection mainSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection OfferSliderItemTranslation
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property string slug
 * @property string image
 * @property string image_responsive
 * @property integer status_id
 */
class OfferSliderItem extends Model
{
    use SoftDeletes;

    public $table = 'offer_slider_items';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'slug',
        'image',
        'image_responsive',
        'status_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'image' => 'string',
        'image_responsive' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Admin\Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function translations()
    {
        return $this->hasMany(\App\Models\Admin\OfferSliderItemTranslation::class);
    }

    /**
     * Return the Model in current languague.
     *
     * @return Translation
     */
    public function translation( $code = null )
    {
        if ( !empty( $code ) ) {
            $attribute = ( gettype( $code ) === 'string' ) ? 'code' : 'id';
        }
        else {
            $code = \App::getLocale();
            $attribute = 'code';
        }

        $language = Language::where( $attribute, $code )->first();
        if ( $language === null ) {
            return [];
        }

        $trans = $this->translations()->where( 'language_id', $language->id )->first();
        return $trans;
    }
}
