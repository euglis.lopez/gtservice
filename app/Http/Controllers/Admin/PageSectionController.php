<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreatePageSectionRequest;
use App\Http\Requests\Admin\UpdatePageSectionRequest;
use App\Repositories\Admin\PageSectionRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PageSectionController extends AppBaseController
{
    /** @var  PageSectionRepository */
    private $pageSectionRepository;

    public function __construct( PageSectionRepository $pageSectionRepo )
    {
        $this->pageSectionRepository = $pageSectionRepo;
    }

    /**
     * Display a listing of the PageSection.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->pageSectionRepository->pushCriteria( new RequestCriteria( $request) );
        $pageSections = $this->pageSectionRepository->all();

        return view( 'admin.page_sections.index' )
            ->with( 'pageSections', $pageSections );
    }

    /**
     * Show the form for creating a new PageSection.
     *
     * @return Response
     */
    public function create()
    {
        return view( 'admin.page_sections.create' );
    }

    /**
     * Store a newly created PageSection in storage.
     *
     * @param CreatePageSectionRequest $request
     *
     * @return Response
     */
    public function store(CreatePageSectionRequest $request )
    {
        $request->validate( [
            'title'         => 'required|unique:offer_slider_item_translations,title',
        ] );

        // inputs
        $input              = $this->input( $request, false );
        $inputTranslation   = $this->inputTranslation( $request, false );

        // create model
        $model = $this->pageSectionRepository->create( $input );

        // create translation
        $model->translations()->create( $inputTranslation );

        Flash::success( 'Page Section saved successfully.' );

        return redirect( route( 'admin.pageSections.index' ) );
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request, $validate = true )
    {
        //Translation Validation
        if ( $validate === true ) {
            // $request->validate( [
            //     'image'     => 'required',
            // ] );
        }

        $model =  $request->only( [
            // 'image'
        ] );
        $model[ 'slug' ]        = str_slug( $request->input( 'title' ), '-' );
        $model[ 'status_id' ]   = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request, $validate = true )
    {
        //Translation Validation
        if ( $validate === true ) {
            $request->validate( [
                'title'         => 'required',
                'description'   => 'required',
            ] );
        }

        $translation = $request->only( [
            'title',
            'description',
        ] );
        $translation[ 'language_id' ] = 1;

        return $translation;
    }

    /**
     * Show the form for editing the specified PageSection.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $code )
    {
        $pageSection = $this->pageSectionRepository->findByField( 'code', $code )->first();

        if ( empty( $pageSection ) ) {
            Flash::error( 'Page Section not found' );

            return redirect( route( 'admin.pageSections.index' ) );
        }

        return view( 'admin.page_sections.edit' )
            ->with( 'pageSection', $pageSection )
            ->with( 'translation', $pageSection->translations->first() );
    }

    /**
     * Update the specified PageSection in storage.
     *
     * @param  int              $id
     * @param UpdatePageSectionRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdatePageSectionRequest $request )
    {
        $model = $this->pageSectionRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Page Section not found' );

            return redirect( route( 'admin.pageSections.index' ) );
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->pageSectionRepository->update( $input, $id );

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        Flash::success( 'Page Section updated successfully.' );

        return redirect( route( 'admin.pageSections.index' ) );
    }

    /**
     * Remove the specified PageSection from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $pageSection = $this->pageSectionRepository->findWithoutFail( $id );

        if ( empty( $pageSection ) ) {
            Flash::error( 'Page Section not found' );

            return redirect( route( 'admin.pageSections.index' ) );
        }

        // delete translations
        $translations = $model->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        // delete model
        $this->pageSectionRepository->delete( $id );

        Flash::success( 'Page Section deleted successfully.' );

        return redirect( route( 'admin.pageSections.index' ) );
    }

    /**
     * Store a image in $model at given $field.
     *
     * @param Request $request
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function storePhoto( $id, $field )
    {
        $model = $this->pageSectionRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Page Section not found' );

            return redirect( route( 'admin.pageSections.index' ) );
        }

        return $this->storePhotoGeneral( request(), $model, $field );
    }

    /**
     * Delete a image in $model at given $field.
     *
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function destroyPhoto( $id, $field )
    {
        $model = $this->pageSectionRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Page Section not found' );

            return redirect( route( 'admin.pageSections.index' ) );
        }

        return $this->destroyPhotoGeneral( $model, $field );
    }
}
