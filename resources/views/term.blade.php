@extends( 'app' )

@section( 'content' )

    @include( 'global.navbar' )

    <section id="term">

        <div class="header-term">
            <h1 class="display-1 font-weight-bold text-white text-center">
                TERMINI E CONDIZIONI
            </h1>
        </div>

        <div class="container py-5">
            <div class="row">

                <div class="col-3" style="border-right: 1px solid #cccbcb;">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                        @foreach ( $policies as $policy )
                            <a class="nav-link {{ ( $active === $policy->slug ) ? 'active' : '' }}" id="v-pills-{{ $policy->slug }}-tab" data-toggle="pill" href="#v-pills-{{ $policy->slug }}" role="tab" aria-controls="v-pills-{{ $policy->slug }}" aria-selected="true">
                                {{ $policy->translation()->title }}
                            </a>
                        @endforeach

                    </div>
                </div>
                <div class="col-9">
                    <div class="tab-content" id="v-pills-tabContent">

                        @foreach ( $policies as $policy )
                            <div class="tab-pane fade {{ ( $active === $policy->slug ) ? 'show active' : '' }}" id="v-pills-{{ $policy->slug }}" role="tabpanel" aria-labelledby="v-pills-{{ $policy->slug }}-tab">
                                <div class="col-12">
                                    <h1>{{ $policy->translation()->title }}</h1>

                                    {!! $policy->translation()->description !!}
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>

            </div>
        </div>
    </section>

    @include( 'global.contact' )
    @include( 'global.footer' )
    @include( 'global.navfixed' )

@endsection

@push( 'styles' )
    <style>
        .header-term {
            widows: 100%;
            min-height: 300px;
            padding-top: 200px;
            padding-bottom: 100px;
            background-image: url(images/header/header1.jpg);
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }
        .header-term h1 {
            font-size: 4rem !important;
        }
        @media screen and (max-width: 728px) {
            .header-term h1 {
                font-size: 3rem !important;
            }
        }
        #term .nav {
            background-color: none;
            color: var(--primary-color);
        }
        #term .nav-pills .nav-link.active {
        color: #fff;
        background-color: var(--primary-color);
    </style>
@endpush

@push( 'scripts' )
@endpush