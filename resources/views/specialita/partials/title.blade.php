<section>
    <div class="container-fluid d-flex header align-items-center" style="height: 300px">
        <h1 class="display-1 font-weight-bold text-white text-center">
            I NOSTRI SERVIZI
        </h1>
    </div>
</section>

@push('styles')
    <style>
        .header {
            background-image: url( images/especial/header.png );
            background-size: cover;
        }

        .header h1 {
            font-size: 4rem !important;
        }

        @media screen and (max-width: 728px) {
            .header h1 {
                font-size: 3rem !important;
            }
        }
    </style>
@endpush