<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGallerySliderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'gallery_slider_items', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->text( 'slug' )->nullable()->default( null );
            $table->string( 'image', 250 )->nullable()->default( null );

            $table->unsignedInteger( 'status_id' );
            $table->foreign( 'status_id' )->references( 'id' )->on( 'statuses' );

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'gallery_slider_items' );
    }
}
