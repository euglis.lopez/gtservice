<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'business_values', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->text( 'field' );
            $table->text( 'name' );
            $table->text( 'value' );
            $table->text( 'icon' )->nullable()->default( null );

            $table->unsignedInteger( 'status_id' );
            $table->foreign( 'status_id' )->references( 'id' )->on( 'statuses' );

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'business_values' );
    }
}
