<?php

use Illuminate\Database\Seeder;

class BusinessValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table( 'business_values' )->insert([
            [
                'field'         => 'phone1',
                'name'          => 'Telefono 1',
                'value'         => '3458377263',
                'status_id'     => 1,
            ],
            // [
            //     'field'         => 'phone2',
            //     'name'          => 'Telefono 2',
            //     'value'         => '349 3297664',
            //     'status_id'     => 1,
            // ],
            [
                'field'         => 'email',
                'name'          => 'E-mail',
                'value'         => 'gtserviceolbia@gmail.com',
                'status_id'     => 1,
            ],
            [
                'field'         => 'address1',
                'name'          => 'Indirizzo 1',
                'value'         => 'Via del Melograno, 2c, 07026',
                'status_id'     => 1,
            ],
            [
                'field'         => 'address2',
                'name'          => 'Indirizzo 2',
                'value'         => 'Olbia OT, Italia',
                'status_id'     => 1,
            ],
            // [
            //     'field'         => 'facebook',
            //     'name'          => 'Facebook',
            //     'value'         => 'https://www.facebook.com/daniele.folino.1441',
            //     'status_id'     => 1,
            // ],
            [
                'field'         => 'link_maps',
                'name'          => 'Url Google Map',
                'value'         => 'https://goo.gl/maps/YnVtnZ52X1G2',
                'status_id'     => 1,
            ],
            [
                'field'         => 'iframe_maps',
                'name'          => 'Iframe Google maps',
                'value'         => '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12053.772341686063!2d9.4981091!3d40.9498713!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfb2c8339ddccb1cb!2sGt%20service!5e0!3m2!1ses!2sve!4v1576591668287!5m2!1ses!2sve" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>',
                'status_id'     => 1,
            ]
        ] );
    }
}
