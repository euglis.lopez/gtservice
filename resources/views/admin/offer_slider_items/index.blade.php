@extends( 'layouts.admin.app' )

@section( 'header' )
    <h1>
        Diapositiva offerte
        <small>Listato</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route( 'admin' ) }}"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li class="active">Diapositiva offerte</li>
    </ol>
@stop

@section( 'content' )
   @include( 'adminlte-templates::common.errors' )

    @include( 'flash::message' )

    <div class="clearfix"></div>
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">Listato</h3>
            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#createOfferSliderItem">Creare</button>
        </div>

        <div class="box-body">
            @include( 'admin.offer_slider_items.table' )
        </div>

    </div>
@endsection

