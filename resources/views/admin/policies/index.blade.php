@extends( 'layouts.admin.app' )

@section( 'header' )
    <h1>
        Termini e Condizioni
        <small>Listato</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route( 'admin' ) }}"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li class="active">Termini e Condizioni</li>
    </ol>
@stop

@section( 'content' )
   @include( 'adminlte-templates::common.errors' )

    @include( 'flash::message' )

    <div class="clearfix"></div>
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">Listato</h3>
        </div>

        <div class="box-body">
            @include( 'admin.policies.table' )
        </div>

    </div>
@endsection

