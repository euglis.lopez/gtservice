<?php

use Illuminate\Database\Seeder;

class MainSliderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table( 'main_slider_items' )->insert( [
            [
                'slug'          => 'gt-service',
                'image'         => 'images/header/header1.jpg',
                'status_id'     => 1,
            ]
        ] );
        DB::table( 'main_slider_item_translations' )->insert( [
            [
                'main_slider_item_id'   => 1,
                'language_id'           => 1,
                'title'                 => 'GT SERVICE',
                'description'           => '<h3>Olbia</h3> <h3>Idraulici ed Elettricisti</h3>',
                'text_button'           => 'CONTATTACI',
                'link_button'           => '#contacto',
            ]
        ] );
    }
}
