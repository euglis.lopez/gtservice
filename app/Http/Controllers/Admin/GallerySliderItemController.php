<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateGallerySliderItemRequest;
use App\Http\Requests\Admin\UpdateGallerySliderItemRequest;
use App\Repositories\Admin\GallerySliderItemRepository;
use Flash;
use Illuminate\Http\Request;

use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class GallerySliderItemController extends AppBaseController
{
    /** @var  GallerySliderItemRepository */
    private $gallerySliderItemRepository;

    public function __construct( GallerySliderItemRepository $gallerySliderItemRepo )
    {
        $this->gallerySliderItemRepository = $gallerySliderItemRepo;
    }

    /**
     * Display a listing of the GallerySliderItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request)
    {
        $this->gallerySliderItemRepository->pushCriteria( new RequestCriteria( $request ) );
        $gallerySliderItems = $this->gallerySliderItemRepository->all();

        return view( 'admin.gallery_slider_items.index' )
            ->with( 'gallerySliderItems', $gallerySliderItems );
    }

    /**
     * Show the form for creating a new GallerySliderItem.
     *
     * @return Response
     */
    public function create()
    {
        return view( 'admin.gallery_slider_items.create' );
    }

    /**
     * Store a newly created GallerySliderItem in storage.
     *
     * @param CreateGallerySliderItemRequest $request
     *
     * @return Response
     */
    public function store( CreateGallerySliderItemRequest $request )
    {
        $request->validate( [
            'title'         => 'required|unique:main_slider_item_translations,title',
        ] );

        // inputs
        $input              = $this->input( $request, false );
        $inputTranslation   = $this->inputTranslation( $request, false );

        // create model
        $model = $this->gallerySliderItemRepository->create( $input );

        // create translation
        $model->translations()->create( $inputTranslation );

        Flash::success( 'Gallery Slider Item saved successfully.' );

        return redirect()->route( 'admin.gallerySliderItems.edit', $model );
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request, $validate = true )
    {
        //Translation Validation
        if ( $validate === true ) {
            // $request->validate( [
            //     'image'     => 'required',
            // ] );
        }

        $model =  $request->only( [
            // 'image'
        ] );
        $model[ 'slug' ]        = str_slug( $request->input( 'title' ), '-' );
        $model[ 'status_id' ]   = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request, $validate = true )
    {
        //Translation Validation
        if ( $validate === true ) {
            $request->validate( [
                'title'         => 'required',
                // 'description'   => 'required',
            ] );
        }

        $translation = $request->only( [
            'title',
            // 'description',
        ] );
        $translation[ 'language_id' ] = 1;

        return $translation;
    }

    /**
     * Show the form for editing the specified GallerySliderItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $id )
    {
        $gallerySliderItem = $this->gallerySliderItemRepository->findWithoutFail( $id );

        if ( empty( $gallerySliderItem ) ) {
            Flash::error( 'Gallery Slider Item not found' );

            return redirect( route( 'admin.gallerySliderItems.index' ) );
        }

        return view( 'admin.gallery_slider_items.edit' )
            ->with( 'gallerySliderItem', $gallerySliderItem )
            ->with( 'translation', $gallerySliderItem->translations->first() );
    }

    /**
     * Update the specified GallerySliderItem in storage.
     *
     * @param  int              $id
     * @param UpdateGallerySliderItemRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateGallerySliderItemRequest $request )
    {
        $model = $this->gallerySliderItemRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Gallery Slider Item not found' );

            return redirect( route( 'admin.gallerySliderItems.index' ) );
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->gallerySliderItemRepository->update( $input, $id );

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        Flash::success( 'Gallery Slider Item updated successfully.' );

        return redirect( route( 'admin.gallerySliderItems.index' ) );
    }

    /**
     * Remove the specified GallerySliderItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $gallerySliderItem = $this->gallerySliderItemRepository->findWithoutFail( $id );

        if ( empty( $gallerySliderItem ) ) {
            Flash::error( 'Gallery Slider Item not found' );

            return redirect( route( 'admin.gallerySliderItems.index' ) );
        }

        // delete translations
        $translations = $gallerySliderItem->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        // delete model
        $this->gallerySliderItemRepository->delete( $id );

        Flash::success( 'Gallery Slider Item deleted successfully.' );

        return redirect( route( 'admin.gallerySliderItems.index' ) );
    }

    /**
     * Store a image in $model at given $field.
     *
     * @param Request $request
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function storePhoto( $id )
    {
        $model = $this->gallerySliderItemRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Gallery Slider Item not found' );

            return redirect( route( 'admin.gallerySliderItems.index' ) );
        }

        return $this->storePhotoGeneral( request(), $model, 'image' );
    }

    /**
     * Delete a image in $model at given $field.
     *
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function destroyPhoto( $id )
    {
        $model = $this->gallerySliderItemRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Gallery Slider Item not found' );

            return redirect( route( 'admin.gallerySliderItems.index' ) );
        }

        return $this->destroyPhotoGeneral( $model, 'image' );
    }
}
