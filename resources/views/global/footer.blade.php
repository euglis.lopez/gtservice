<footer class="footer pt-3 bg-primary">
    <div class="container jumperr">
        <hr>
        <div class="row align-items-lg-end">
            <div class="col-12 col-md-10 mb-md-4">
                <span class="text-center d-block d-md-inline text-white">
                    © 2019 GT Service Olbia |
                </span>
                <span class="text-center d-block d-md-inline">
                    <a class="text-capitalize text-white" href=" {{ route( 'terms', [ 't' => 'policy' ] ) }} ">
                        Politica sulla privacy |
                    </a>
                </span>
                <span class="text-center d-block d-md-inline">
                    <a class="text-capitalize text-white" href=" {{ route( 'terms', [ 't' => 'terms' ] ) }} ">
                        Termini e condizioni
                    </a>
                </span>
            </div>
            <div class="col-8 mx-auto col-md-2 align-self-end">
                <p class="text-center">
                    <a href="http://www.jumperr.com/" target="_blank">
                        <img class="img-fluid" src="images/logo/powered-by-jumperr.svg" width="128" alt="jumperr">
                    </a>
                </p>
            </div>
        </div>
    </div>
    <!-- div para darle altura al footer y no se deje de mostrar -->
    <div class="d-md-none" style="height: 74px;"></div>
</footer>

@push('styles')
    <style>
        .bg-gris{
            background-color: #636363 !important;
        }
    </style>
@endpush