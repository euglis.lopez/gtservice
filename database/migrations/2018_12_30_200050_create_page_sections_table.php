<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'page_sections', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->text( 'code' )->nullable()->default( null );

            $table->text( 'slug' )->nullable()->default( null );
            $table->string( 'image' )->nullable()->default( null );

            $table->unsignedInteger( 'status_id' );
            $table->foreign( 'status_id' )->references( 'id' )->on( 'statuses' );

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'page_sections' );
    }
}
