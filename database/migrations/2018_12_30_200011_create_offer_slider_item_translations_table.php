<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferSliderItemTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'offer_slider_item_translations', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->unsignedInteger( 'offer_slider_item_id' );
            $table->foreign( 'offer_slider_item_id' )->references( 'id' )->on( 'offer_slider_items' );

            $table->unsignedInteger( 'language_id' );
            $table->foreign( 'language_id' )->references( 'id' )->on( 'languages' );

            $table->text( 'title' );
            $table->text( 'description' )->nullable()->default( null );

            // buttons
            $table->text( 'text_button1' )->nullable()->default( null );
            $table->text( 'value_button1' )->nullable()->default( null );
            $table->text( 'text_button2' )->nullable()->default( null );
            $table->text( 'value_button2' )->nullable()->default( null );

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'offer_slider_item_translations' );
    }
}
