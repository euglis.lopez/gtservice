<div class="sticky d-none d-md-block">
    <ul class="nav flex-column bg-primary pl-3" style="border-top-right-radius: 24px;border-bottom-right-radius: 12px;">
        <li class="nav-item">
            <a class="text-white" href="https://api.whatsapp.com/send?phone=+{{ str_replace( ' ', '', $businessValues[ 'phone1' ] ) }}&text=Ciao%2C%20voglio%20acquisire%20un%20contatto%20con%20te">
                <figure class="my-4">
                    <img width="32" src="{{ asset( 'images/icons/whatsapp-logo.png' ) }}" alt="whatsaap">
                </figure>
            </a>
        </li>
        <li class="nav-item">
            <a class="text-white" href="tel:+{{ str_replace( ' ', '', $businessValues[ 'phone1' ] ) }}">
                <figure class="my-4">
                    <img width="32" src="{{ asset( 'images/icons/telephone.png' ) }}" alt="phone">
                </figure>
            </a>
        </li>
        <li class="nav-item">
            <a class="text-white border-white" href="mailto:{{ $businessValues[ 'email' ] }}">
                <figure class="my-4">
                    <img width="32" src="{{ asset('images/icons/mail-envelope-symbol.png' ) }}" alt="email">
                </figure>
            </a>
        </li>
    </ul>
</div>

@push('style')
    <style>
    </style>
@endpush