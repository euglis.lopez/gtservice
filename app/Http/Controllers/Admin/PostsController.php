<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;
use App\Tag;
use Carbon\Carbon;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('admin.posts.index', compact('posts'));
    }

    // public function create()
    // {
    //     $categorias = Category::all();
    //     $tags = Tag::all();
    //     return view('admin.posts.create', compact('categorias', 'tags'));
    // }

    public function store( Request $request )
    {
        $this->validate( $request, [ 'title' => 'required' ] );

        $post = Post::create( [
            'title' => $request->title,
            'url'   => str_slug( $request->title ),
        ] );

        return redirect()->route( 'admin.posts.edit', $post );
    }

    public function edit(Post $post)
    {
        $categorias = Category::all();
        $tags = Tag::all();
        return view('admin.posts.edit', compact('categorias', 'tags', 'post'));
    }

    public function update(Post $post, Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'excerpt' => 'required',
        ]);

        $post->title = $request->title;
        $post->url = str_slug($request->title);
        $post->body = $request->body;
        $post->excerpt = $request->excerpt;
        $post->published_at = $request->published_at ? Carbon::parse($request->published_at) : null;
        $post->category_id = $request->category;
        $post->save();

        //etiquetas
        $post->tags()->attach($request->tags);

        return redirect()->route('admin.posts.edit', $post)->with('flash', 'Tu publicacion ha sido guardada');
    }
}
