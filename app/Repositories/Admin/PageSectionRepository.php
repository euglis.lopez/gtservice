<?php

namespace App\Repositories\Admin;

use App\Models\Admin\PageSection;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PageSectionRepository
 * @package App\Repositories\Admin
 * @version March 7, 2019, 5:45 pm UTC
 *
 * @method PageSection findWithoutFail($id, $columns = ['*'])
 * @method PageSection find($id, $columns = ['*'])
 * @method PageSection first($columns = ['*'])
*/
class PageSectionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'slug',
        'image',
        'status_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PageSection::class;
    }
}
