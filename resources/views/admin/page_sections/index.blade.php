@extends( 'layouts.admin.app' )

@section( 'header' )
    <h1>
        Sezioni della Pagina
        <small>Listato</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route( 'admin' ) }}"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li class="active">Sezioni della Pagina</li>
    </ol>
@stop

@section( 'content' )
   @include( 'adminlte-templates::common.errors' )

    @include( 'flash::message' )

    <div class="clearfix"></div>
    <div class="box box-primary">

        <div class="box-header">
            <h3 class="box-title">Listato</h3>
        </div>

        <div class="box-body">
            @include( 'admin.page_sections.table' )
        </div>

    </div>
@endsection
