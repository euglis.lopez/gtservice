<?php

use Illuminate\Database\Seeder;

class GallerySliderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Imagen 1
         */
        DB::table( 'gallery_slider_items' )->insert( [
            [
                'slug'          => 'imagen-1',
                'image'         => 'images/gallery/imagen1.jpeg',
                'status_id'     => 1,
            ]
        ] );
        DB::table( 'gallery_slider_item_translations' )->insert( [
            [
                'gallery_slider_item_id'    => 1,
                'language_id'               => 1,
                'title'                     => 'Imagen 1',
            ]
        ] );


        /*
         * Imagen 2
         */
        DB::table( 'gallery_slider_items' )->insert( [
            [
                'slug'          => 'imagen-2',
                'image'         => 'images/gallery/imagen2.jpeg',
                'status_id'     => 1,
            ]
        ] );
        DB::table( 'gallery_slider_item_translations' )->insert( [
            [
                'gallery_slider_item_id'    => 2,
                'language_id'               => 1,
                'title'                     => 'Imagen 2',
            ]
        ] );


        /*
         * Imagen 3
         */
        DB::table( 'gallery_slider_items' )->insert( [
            [
                'slug'          => 'imagen-3',
                'image'         => 'images/gallery/imagen3.jpeg',
                'status_id'     => 1,
            ]
        ] );
        DB::table( 'gallery_slider_item_translations' )->insert( [
            [
                'gallery_slider_item_id'    => 3,
                'language_id'               => 1,
                'title'                     => 'Imagen 3',
            ]
        ] );


        /*
         * Imagen 4
         */
        DB::table( 'gallery_slider_items' )->insert( [
            [
                'slug'          => 'imagen-4',
                'image'         => 'images/gallery/imagen4.jpeg',
                'status_id'     => 1,
            ]
        ] );
        DB::table( 'gallery_slider_item_translations' )->insert( [
            [
                'gallery_slider_item_id'    => 4,
                'language_id'               => 1,
                'title'                     => 'Imagen 4',
            ]
        ] );
    }
}
