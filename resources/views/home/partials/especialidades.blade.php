<section id="specialita">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="text-uppercase underline font-weight-bold text-primary mt-4">
                    I nostri servizi
                </h2>
            </div>
        </div>
    </div>

    <div class="swiper-especialidades mt-5">
    <div class="swiper-wrapper">

        @foreach ( $posts as $post )
            @if ( $post->photos->count() > 0 )
                <a href="{{ route( 'specialita' ) . '#' . $post->url }}"
                    class="swiper-slide item-special d-sm-flex align-items-sm-end d-md-flex align-items-md-end"
                    style="background-image: url( {{ asset( $post->photos->first()->url ) }} )">

                    <div class="col-12 bg-primary text-white d-flex align-items-center btn-card">
                        <span class="font-weight-bold p-4">
                            {{ $post->title }}
                        </span>
                    </div>
                </a>
            @endif
        @endforeach

    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination swiper-especial"></div>
  </div>
</section>

@push( 'styles' )
    <style>
        #specialita{
            overflow: hidden;
        }

        .btn-card {
            height: 50px;
            border-top: 3px solid;
            max-height: 50px;
        }
        .item-special{
            width: 100%;
            height: 160px;
        }
        @media screen and (min-width: 992px)
        {
            .item-special{
                /* width: 25%;
                height: 320px; */
            }
        }

        @media screen and (max-width: 992px) {
            .swiper-slide {
                justify-content: flex-end;
            }
        }

        .especialidades h2 {
            font-family: 'Montserrat', sans-serif;
            letter-rilegaturacing: 1.5px;
            font-size: 25px;
        }

    </style>

    <style>
        .swiper-especialidades {
          width: 100%;
          padding-top: 0px;
          padding-bottom: 50px;
        }
        .swiper-especialidades .swiper-slide {
          background-position: center;
          background-size: cover;
          width: 300px;
          height: 320px;
        }
    </style>
@endpush

@push( 'scripts' )
    <script>
        var swiper = new Swiper('.swiper-especialidades', {
          effect: 'coverflow',
          grabCursor: true,
          centeredSlides: true,
          slidesPerView: 'auto',
          coverflowEffect: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows : true,
          },
          autoplay: {
            delay: 2000,
          },
          pagination: {
            el: '.swiper-pagination.swiper-especial',
          },
        });
    </script>
@endpush