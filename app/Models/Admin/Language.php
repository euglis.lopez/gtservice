<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Language
 * @package App\Models\Admin
 * @version December 12, 2018, 9:34 pm CET
 *
 * @property \App\Models\Admin\Status status
 * @property \Illuminate\Database\Eloquent\Collection activities
 * @property \Illuminate\Database\Eloquent\Collection ActivityCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection ActivityTranslation
 * @property \Illuminate\Database\Eloquent\Collection additionalCategories
 * @property \Illuminate\Database\Eloquent\Collection AdditionalCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection AdditionalTranslation
 * @property \Illuminate\Database\Eloquent\Collection additionals
 * @property \Illuminate\Database\Eloquent\Collection blogComments
 * @property \Illuminate\Database\Eloquent\Collection BlogTranslation
 * @property \Illuminate\Database\Eloquent\Collection BrandTranslation
 * @property \Illuminate\Database\Eloquent\Collection EventCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection EventTranslation
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection orderDetailsAdditionals
 * @property \Illuminate\Database\Eloquent\Collection orders
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection ProductCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection ProductFeatureCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection ProductFeatureTranslation
 * @property \Illuminate\Database\Eloquent\Collection productFeatures
 * @property \Illuminate\Database\Eloquent\Collection ProductPresentationTranslation
 * @property \Illuminate\Database\Eloquent\Collection productPresentationsProducts
 * @property \Illuminate\Database\Eloquent\Collection productSubcategories
 * @property \Illuminate\Database\Eloquent\Collection ProductSubcategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection ProductTranslation
 * @property \Illuminate\Database\Eloquent\Collection RequestCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection roomCategories
 * @property \Illuminate\Database\Eloquent\Collection RoomCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection RoomLocationTranslation
 * @property \Illuminate\Database\Eloquent\Collection RoomSeasonTranslation
 * @property \Illuminate\Database\Eloquent\Collection roomSeasons
 * @property \Illuminate\Database\Eloquent\Collection RoomTranslation
 * @property \Illuminate\Database\Eloquent\Collection rooms
 * @property \Illuminate\Database\Eloquent\Collection roomsCategoriesServices
 * @property \Illuminate\Database\Eloquent\Collection roomsServices
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection SeoTranslation
 * @property \Illuminate\Database\Eloquent\Collection ServiceCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection ServiceTranslation
 * @property \Illuminate\Database\Eloquent\Collection services
 * @property \Illuminate\Database\Eloquent\Collection StatusTranslation
 * @property \Illuminate\Database\Eloquent\Collection TagTranslation
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property string code
 * @property string name
 * @property integer status_id
 */
class Language extends Model
{
    use SoftDeletes;

    public $table = 'languages';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'code',
        'name',
        'status_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'name' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Admin\Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function statusTranslations()
    {
        return $this->hasMany(\App\Models\Admin\StatusTranslation::class);
    }
}
