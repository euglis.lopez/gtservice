<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateMainSliderItemRequest;
use App\Http\Requests\Admin\UpdateMainSliderItemRequest;
use App\Repositories\Admin\MainSliderItemRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MainSliderItemController extends AppBaseController
{
    /** @var  MainSliderItemRepository */
    private $mainSliderItemRepository;

    public function __construct( MainSliderItemRepository $mainSliderItemRepo )
    {
        $this->mainSliderItemRepository = $mainSliderItemRepo;
    }

    /**
     * Display a listing of the MainSliderItem.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->mainSliderItemRepository->pushCriteria( new RequestCriteria( $request ) );
        $mainSliderItems = $this->mainSliderItemRepository->all();

        return view( 'admin.main_slider_items.index' )
            ->with( 'mainSliderItems', $mainSliderItems );
    }

    /**
     * Show the form for creating a new MainSliderItem.
     *
     * @return Response
     */
    public function create()
    {
        return view( 'admin.main_slider_items.create' );
    }

    /**
     * Store a newly created MainSliderItem in storage.
     *
     * @param CreateMainSliderItemRequest $request
     *
     * @return Response
     */
    public function store( CreateMainSliderItemRequest $request )
    {
        $request->validate( [
            'title'         => 'required|unique:main_slider_item_translations,title',
        ] );

        // inputs
        $input              = $this->input( $request, false );
        $inputTranslation   = $this->inputTranslation( $request, false );

        // create model
        $model = $this->mainSliderItemRepository->create( $input );

        // create translation
        $model->translations()->create( $inputTranslation );

        Flash::success( 'Main Slider Item saved successfully.' );

        return redirect()->route( 'admin.mainSliderItems.edit', $model );
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request, $validate = true )
    {
        //Translation Validation
        if ( $validate === true ) {
            // $request->validate( [
            //     'image'     => 'required',
            // ] );
        }

        $model =  $request->only( [
            // 'image'
        ] );
        $model[ 'slug' ]        = str_slug( $request->input( 'title' ), '-' );
        $model[ 'status_id' ]   = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request, $validate = true )
    {
        //Translation Validation
        if ( $validate === true ) {
            $request->validate( [
                'title'         => 'required',
                'description'   => 'required',
                'text_button'   => 'required',
                'link_button'   => 'required',
            ] );
        }

        $translation = $request->only( [
            'title',
            'description',
            'text_button',
            'link_button',
        ] );
        $translation[ 'language_id' ] = 1;

        return $translation;
    }

    /**
     * Show the form for editing the specified MainSliderItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $id )
    {
        $mainSliderItem = $this->mainSliderItemRepository->findWithoutFail( $id );

        if ( empty( $mainSliderItem ) ) {
            Flash::error( 'Main Slider Item not found' );

            return redirect( route( 'admin.mainSliderItems.index' ) );
        }

        return view( 'admin.main_slider_items.edit' )
            ->with( 'mainSliderItem', $mainSliderItem )
            ->with( 'translation', $mainSliderItem->translations->first() );
    }

    /**
     * Update the specified MainSliderItem in storage.
     *
     * @param  int              $id
     * @param UpdateMainSliderItemRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateMainSliderItemRequest $request )
    {
        $model = $this->mainSliderItemRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Main Slider Item not found' );

            return redirect( route( 'admin.mainSliderItems.index' ) );
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->mainSliderItemRepository->update( $input, $id );

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        Flash::success( 'Main Slider Item updated successfully.' );

        return redirect( route( 'admin.mainSliderItems.index' ) );
    }

    /**
     * Remove the specified MainSliderItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $mainSliderItem = $this->mainSliderItemRepository->findWithoutFail( $id );

        if ( empty( $mainSliderItem ) ) {
            Flash::error( 'Main Slider Item not found' );

            return redirect( route( 'admin.mainSliderItems.index' ) );
        }

        // delete translations
        $translations = $mainSliderItem->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        // delete model
        $this->mainSliderItemRepository->delete( $id );

        Flash::success( 'Main Slider Item deleted successfully.' );

        return redirect( route( 'admin.mainSliderItems.index' ) );
    }

    /**
     * Store a image in $model at given $field.
     *
     * @param Request $request
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function storePhoto( $id )
    {
        $model = $this->mainSliderItemRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Main Slider Item not found' );

            return redirect( route( 'admin.mainSliderItems.index' ) );
        }

        return $this->storePhotoGeneral( request(), $model, 'image' );
    }

    /**
     * Delete a image in $model at given $field.
     *
     * @param $model
     * @param string $field
     *
     * @return void
     */
    public function destroyPhoto( $id )
    {
        $model = $this->mainSliderItemRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Main Slider Item not found' );

            return redirect( route( 'admin.mainSliderItems.index' ) );
        }

        return $this->destroyPhotoGeneral( $model, 'image' );
    }
}
