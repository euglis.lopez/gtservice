<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MainSliderItemTranslation
 * @package App\Models\Admin
 * @version February 19, 2019, 5:32 pm UTC
 *
 * @property \App\Models\Admin\Language language
 * @property \App\Models\Admin\MainSliderItem mainSliderItem
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property integer main_slider_item_id
 * @property integer language_id
 * @property string title
 * @property string description
 * @property string text_button
 * @property string link_button
 */
class MainSliderItemTranslation extends Model
{
    use SoftDeletes;

    public $table = 'main_slider_item_translations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'main_slider_item_id',
        'language_id',
        'title',
        'description',
        'text_button',
        'link_button'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'main_slider_item_id' => 'integer',
        'language_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'text_button' => 'string',
        'link_button' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function language()
    {
        return $this->belongsTo(\App\Models\Admin\Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mainSliderItem()
    {
        return $this->belongsTo(\App\Models\Admin\MainSliderItem::class);
    }
}
