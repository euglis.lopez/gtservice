<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes( [ 'verify' => true ] );

Route::post( '/sendmail', 'MailController@sendMail' )->name( 'sendmail' );
Route::post( '/conttato', 'MailController@sendMail' );

/*
|
| Page routes
|
*/
Route::group( [], function () {
    Route::get( '/', 'PagesController@home' )->name( 'home' );
    Route::get( '/servizi', 'PagesController@services' )->name( 'specialita' );
    Route::get( '/terms', 'PagesController@terms' )->name( 'terms' );
    // Route::get( '/policy', 'PagesController@policy' )->name( 'policy' );
    // Route::get( '/cookies', 'PagesController@cookies' )->name( 'cookies' );
} );

Route::post('/password/reset-user', function(){
    Auth::logout();
    return redirect()->route('password.request');
})->name('logoutForChangePassword');

/*
|
| Admin routes
|
*/
Route::group( [ 'prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth' ], function() {

    Route::get( '/', function () {
        return view( 'admin.dashboard');
    } )->name( 'admin' );


    Route::get( 'posts', 'PostsController@index' )->name( 'admin.posts.index' );
    Route::get( 'posts/create', 'PostsController@create' )->name( 'admin.posts.create' );
    Route::post( 'posts/', 'PostsController@store' )->name( 'admin.posts.store' );
    Route::get( 'posts/{post}', 'PostsController@edit' )->name( 'admin.posts.edit' );
    Route::put( 'posts/{post}', 'PostsController@update' )->name( 'admin.posts.update' );

    Route::post( 'posts/{post}/photos', 'PhotoController@store' )->name( 'admin.posts.photos.store' );
    route::delete( 'posts/{photo}', 'PhotoController@destroy' )->name( 'admin.photo.destroy' );

    // Slider Principal
    Route::get( 'mainSliderItems', [ 'as'=> 'admin.mainSliderItems.index', 'uses' => 'MainSliderItemController@index' ] );
    Route::post( 'mainSliderItems', [ 'as'=> 'admin.mainSliderItems.store', 'uses' => 'MainSliderItemController@store' ] );
    Route::put( 'mainSliderItems/{mainSliderItems}', [ 'as'=> 'admin.mainSliderItems.update', 'uses' => 'MainSliderItemController@update' ] );
    Route::patch( 'mainSliderItems/{mainSliderItems}', [ 'as'=> 'admin.mainSliderItems.update', 'uses' => 'MainSliderItemController@update' ] );
    Route::delete( 'mainSliderItems/{mainSliderItems}', [ 'as'=> 'admin.mainSliderItems.destroy', 'uses' => 'MainSliderItemController@destroy' ] );
    Route::get( 'mainSliderItems/{mainSliderItems}/edit', [ 'as'=> 'admin.mainSliderItems.edit', 'uses' => 'MainSliderItemController@edit' ] );
        Route::post( 'mainSliderItems/{mainSliderItem}/photos/{field}', 'MainSliderItemController@storePhoto' )->name( 'admin.mainSliderItems.photos.store' );
        route::delete( 'mainSliderItems/{mainSliderItem}/photos/{field}', 'MainSliderItemController@destroyPhoto' )->name( 'admin.mainSliderItems.photos.destroy' );

    // Slider de Ofertas
    Route::get( 'offerSliderItems', [ 'as'=> 'admin.offerSliderItems.index', 'uses' => 'OfferSliderItemController@index' ] );
    Route::post( 'offerSliderItems', [ 'as'=> 'admin.offerSliderItems.store', 'uses' => 'OfferSliderItemController@store' ] );
    Route::put( 'offerSliderItems/{offerSliderItems}', [ 'as'=> 'admin.offerSliderItems.update', 'uses' => 'OfferSliderItemController@update' ] );
    Route::patch( 'offerSliderItems/{offerSliderItems}', [ 'as'=> 'admin.offerSliderItems.update', 'uses' => 'OfferSliderItemController@update' ] );
    Route::delete( 'offerSliderItems/{offerSliderItems}', [ 'as'=> 'admin.offerSliderItems.destroy', 'uses' => 'OfferSliderItemController@destroy' ] );
    Route::get( 'offerSliderItems/{offerSliderItems}/edit', [ 'as'=> 'admin.offerSliderItems.edit', 'uses' => 'OfferSliderItemController@edit' ] );
        Route::post( 'offerSliderItems/{offerSliderItem}/photos/{field}', 'OfferSliderItemController@storePhoto' )->name( 'admin.offerSliderItems.photos.store' );
        route::delete( 'offerSliderItems/{offerSliderItem}/photos/{field}', 'OfferSliderItemController@destroyPhoto' )->name( 'admin.offerSliderItems.photos.destroy' );

    // Slider de Galeria
    Route::get( 'gallerySliderItems', [ 'as'=> 'admin.gallerySliderItems.index', 'uses' => 'GallerySliderItemController@index' ] );
    Route::post( 'gallerySliderItems', [ 'as'=> 'admin.gallerySliderItems.store', 'uses' => 'GallerySliderItemController@store' ] );
    Route::put( 'gallerySliderItems/{gallerySliderItems}', [ 'as'=> 'admin.gallerySliderItems.update', 'uses' => 'GallerySliderItemController@update' ] );
    Route::patch( 'gallerySliderItems/{gallerySliderItems}', [ 'as'=> 'admin.gallerySliderItems.update', 'uses' => 'GallerySliderItemController@update' ] );
    Route::delete( 'gallerySliderItems/{gallerySliderItems}', [ 'as'=> 'admin.gallerySliderItems.destroy', 'uses' => 'GallerySliderItemController@destroy' ] );
    Route::get( 'gallerySliderItems/{gallerySliderItems}/edit', [ 'as'=> 'admin.gallerySliderItems.edit', 'uses' => 'GallerySliderItemController@edit' ] );
        Route::post( 'gallerySliderItems/{gallerySliderItem}/photos/{field}', 'GallerySliderItemController@storePhoto' )->name( 'admin.gallerySliderItems.photos.store' );
        route::delete( 'gallerySliderItems/{gallerySliderItem}/photos/{field}', 'GallerySliderItemController@destroyPhoto' )->name( 'admin.gallerySliderItems.photos.destroy' );

    // Información del negocio
    Route::get( 'businessValues', [ 'as'=> 'admin.businessValues.index', 'uses' => 'BusinessValueController@index' ] );
    Route::put( 'businessValues/{businessValues}', [ 'as'=> 'admin.businessValues.update', 'uses' => 'BusinessValueController@update' ] );
    Route::patch( 'businessValues/{businessValues}', [ 'as'=> 'admin.businessValues.update', 'uses' => 'BusinessValueController@update' ] );
    Route::get( 'businessValues/{businessValues}/edit', [ 'as'=> 'admin.businessValues.edit', 'uses' => 'BusinessValueController@edit' ] );

    // Politicas, Terminos y Cookies
    Route::get( 'policies', [ 'as'=> 'admin.policies.index', 'uses' => 'PolicyController@index' ] );
    Route::put( 'policies/{policies}', [ 'as'=> 'admin.policies.update', 'uses' => 'PolicyController@update' ] );
    Route::patch( 'policies/{policies}', [ 'as'=> 'admin.policies.update', 'uses' => 'PolicyController@update' ] );
    Route::get( 'policies/{policies}/edit', [ 'as'=> 'admin.policies.edit', 'uses' => 'PolicyController@edit' ] );

    // Chi Siamo
    Route::get( 'pageSections', [ 'as'=> 'admin.pageSections.index', 'uses' => 'PageSectionController@index' ] );
    // Route::post( 'pageSections', [ 'as'=> 'admin.pageSections.store', 'uses' => 'PageSectionController@store' ] );
    Route::put( 'pageSections/{pageSections}', [ 'as'=> 'admin.pageSections.update', 'uses' => 'PageSectionController@update' ] );
    Route::patch( 'pageSections/{pageSections}', [ 'as'=> 'admin.pageSections.update', 'uses' => 'PageSectionController@update' ] );
    // Route::delete( 'pageSections/{pageSections}', [ 'as'=> 'admin.pageSections.destroy', 'uses' => 'PageSectionController@destroy' ] );
    Route::get( 'pageSections/{code}/edit', [ 'as'=> 'admin.pageSections.edit', 'uses' => 'PageSectionController@edit' ] );
        Route::post( 'pageSections/{pageSections}/photos/{field}', 'PageSectionController@storePhoto' )->name( 'admin.pageSections.photos.store' );
        route::delete( 'pageSections/{pageSections}/photos/{field}', 'PageSectionController@destroyPhoto' )->name( 'admin.pageSections.photos.destroy' );

} );

