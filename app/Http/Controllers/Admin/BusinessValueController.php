<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateBusinessValueRequest;
use App\Http\Requests\Admin\UpdateBusinessValueRequest;
use App\Repositories\Admin\BusinessValueRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BusinessValueController extends AppBaseController
{
    /** @var  BusinessValueRepository */
    private $businessValueRepository;

    public function __construct(BusinessValueRepository $businessValueRepo)
    {
        $this->businessValueRepository = $businessValueRepo;
    }

    /**
     * Display a listing of the BusinessValue.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->businessValueRepository->pushCriteria(new RequestCriteria($request));
        $businessValues = $this->businessValueRepository->all();

        return view('admin.business_values.index')
            ->with('businessValues', $businessValues);
    }

    /**
     * Show the form for creating a new BusinessValue.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.business_values.create');
    }

    /**
     * Store a newly created BusinessValue in storage.
     *
     * @param CreateBusinessValueRequest $request
     *
     * @return Response
     */
    public function store(CreateBusinessValueRequest $request)
    {
        $input = $request->all();

        $businessValue = $this->businessValueRepository->create($input);

        Flash::success('Business Value saved successfully.');

        return redirect(route('admin.businessValues.index'));
    }

    /**
     * Display the specified BusinessValue.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $businessValue = $this->businessValueRepository->findWithoutFail($id);

        if (empty($businessValue)) {
            Flash::error('Business Value not found');

            return redirect(route('admin.businessValues.index'));
        }

        return view('admin.business_values.show')->with('businessValue', $businessValue);
    }

    /**
     * Show the form for editing the specified BusinessValue.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $businessValue = $this->businessValueRepository->findWithoutFail($id);

        if (empty($businessValue)) {
            Flash::error('Business Value not found');

            return redirect(route('admin.businessValues.index'));
        }

        return view('admin.business_values.edit')->with('businessValue', $businessValue);
    }

    /**
     * Update the specified BusinessValue in storage.
     *
     * @param  int              $id
     * @param UpdateBusinessValueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBusinessValueRequest $request)
    {
        $businessValue = $this->businessValueRepository->findWithoutFail($id);

        if (empty($businessValue)) {
            Flash::error('Business Value not found');

            return redirect(route('admin.businessValues.index'));
        }

        $businessValue = $this->businessValueRepository->update($request->all(), $id);

        Flash::success('Business Value updated successfully.');

        return redirect(route('admin.businessValues.index'));
    }

    /**
     * Remove the specified BusinessValue from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $businessValue = $this->businessValueRepository->findWithoutFail($id);

        if (empty($businessValue)) {
            Flash::error('Business Value not found');

            return redirect(route('admin.businessValues.index'));
        }

        $this->businessValueRepository->delete($id);

        Flash::success('Business Value deleted successfully.');

        return redirect(route('admin.businessValues.index'));
    }
}
