<?php

namespace App\Repositories\Admin;

use App\Models\Admin\MainSliderItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MainSliderItemRepository
 * @package App\Repositories\Admin
 * @version February 18, 2019, 8:44 pm UTC
 *
 * @method MainSliderItem findWithoutFail($id, $columns = ['*'])
 * @method MainSliderItem find($id, $columns = ['*'])
 * @method MainSliderItem first($columns = ['*'])
*/
class MainSliderItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MainSliderItem::class;
    }
}
