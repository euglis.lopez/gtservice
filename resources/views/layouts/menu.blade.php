{{-- Slider Principal --}}
<li class="treeview {{ request()->is('admin/mainSliderItems*') ? 'active' : '' }}">
    <a href="#">
        <i class="fa fa-link"></i> <span>Diapositiva principale</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ route('admin.mainSliderItems.index') }}">Vedi tutti</a></li>
        <li><a href="#" data-toggle="modal" data-target="#createMainSliderItem">Crea diapositiva</a></li>
    </ul>
</li>

{{-- posts --}}
<li class="treeview {{ request()->is('admin/posts*') ? 'active' : '' }}">
    <a href="#">
        <i class="fa fa-link"></i> <span>Servizi</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ route('admin.posts.index') }}">Vedi tutti</a></li>
        <li><a href="#" data-toggle="modal" data-target="#myModal">Crea servizio</a></li>
    </ul>
</li>

{{-- Chi SIAMO--}}
<li class="{{ Request::is('pageSections*') ? 'active' : '' }}">
    <a href="{!! route('admin.pageSections.edit', [ 'code' => 'chi-siamo' ] ) !!}"><i class="fa fa-edit"></i><span>Chi Siamo</span></a>
</li>

{{-- Slider de Ofertas --}}
<li class="treeview {{ request()->is('admin/offerSliderItems*') ? 'active' : '' }}">
    <a href="#">
        <i class="fa fa-link"></i> <span>Diapositiva offerte</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ route('admin.offerSliderItems.index') }}">Vedi tutti</a></li>
        <li><a href="#" data-toggle="modal" data-target="#createOfferSliderItem">Crea offerte</a></li>
    </ul>
</li>

{{-- Configuracion general--}}
<li class="{{ Request::is('businessValues*') ? 'active' : '' }}">
    <a href="{!! route('admin.businessValues.index') !!}"><i class="fa fa-edit"></i><span>Configurazioni generali</span></a>
</li>

{{-- Slider de Galeria --}}
<li class="treeview {{ request()->is('admin/gallerySliderItems*') ? 'active' : '' }}">
    <a href="#">
        <i class="fa fa-link"></i> <span>Diapositiva della galleria</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ route('admin.gallerySliderItems.index') }}">Vedi tutti</a></li>
        <li><a href="#" data-toggle="modal" data-target="#createGallerySliderItem">Crea diapositiva</a></li>
    </ul>
</li>

{{-- Terminos y condiciones --}}
<li class="{{ Request::is('policies*') ? 'active' : '' }}">
    <a href="{!! route('admin.policies.index') !!}"><i class="fa fa-edit"></i><span>Termini e Condizioni</span></a>
</li>


