@extends('layouts.admin.app')

@section( 'header' )
    <h1>
        Diapositiva offerte
        <small>Modifica offerta</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route( 'admin' ) }}"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li><a href="{{ route( 'admin.offerSliderItems.index' ) }}"><i class="fa fa-list"></i> Diapositiva offerte</a></li>
        <li class="active">Modifica</li>
      </ol>
@stop

@section( 'content' )
   @include( 'adminlte-templates::common.errors' )

   <div class="row">

        {{-- image --}}
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-6" style="height: 200px; overflow: hidden;">

                            <label style="display: block;">Immagine del desktop</label>
                            @if ( $offerSliderItem->image )

                                <form method="POST" action="{{ route( 'admin.offerSliderItems.photos.destroy', [ $offerSliderItem->id, 'image' ] ) }}">
                                    @csrf
                                    @method( 'DELETE' )
                                    <button class="btn btn-danger btn-xs" style="position: absolute;"><i class="fa fa-remove"></i></button>
                                    <img src="{{ url( $offerSliderItem->image ) }}" alt="" class="img-responsive">
                                </form>

                            @else

                                <p>Nessuna foto</p>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- image_responsive --}}
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-6" style="height: 200px; overflow: hidden;">

                            <label style="display: block;">Immagine del responsive</label>
                            @if ( $offerSliderItem->image_responsive )

                                <form method="POST" action="{{ route( 'admin.offerSliderItems.photos.destroy', [ $offerSliderItem->id, 'image_responsive' ] ) }}">
                                    @csrf
                                    @method( 'DELETE' )
                                    <button class="btn btn-danger btn-xs" style="position: absolute;"><i class="fa fa-remove"></i></button>
                                    <img src="{{ url( $offerSliderItem->image_responsive ) }}" alt="" class="img-responsive">
                                </form>

                            @else

                                <p>Nessuna foto</p>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>



       {!! Form::model( $offerSliderItem, [ 'route' => [ 'admin.offerSliderItems.update', $offerSliderItem->id ], 'method' => 'patch' ] ) !!}

            @include( 'admin.offer_slider_items.fields' )

       {!! Form::close() !!}
   </div>
@endsection