@extends('layouts.admin.app')

@section( 'header' )
    <h1>
        {{ $translation->title }}
        <small>Modifica</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route( 'admin' ) }}"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li><a href="{{ route( 'admin.pageSections.index' ) }}"><i class="fa fa-dashboard"></i> Sezioni della Pagina</a></li>
        <li class="active">{{ $translation->title }}</li>
      </ol>
@stop

@section( 'content' )
   @include( 'adminlte-templates::common.errors' )

   <div class="row">

        {{-- image --}}
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-6" style="height: 200px; overflow: hidden;">

                            <label style="display: block;">Immagine</label>
                            @if ( $pageSection->image )

                                <form method="POST" action="{{ route( 'admin.pageSections.photos.destroy', [ $pageSection->id, 'image' ] ) }}">
                                    @csrf
                                    @method( 'DELETE' )
                                    <button class="btn btn-danger btn-xs" style="position: absolute;"><i class="fa fa-remove"></i></button>
                                    <img src="{{ url( $pageSection->image ) }}" alt="" class="img-responsive">
                                </form>

                            @else

                                <p>Nessuna foto</p>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>

       {!! Form::model( $pageSection, [ 'route' => [ 'admin.pageSections.update', $pageSection->id ], 'method' => 'patch' ] ) !!}

            @include( 'admin.page_sections.fields' )

       {!! Form::close() !!}
   </div>
@endsection
