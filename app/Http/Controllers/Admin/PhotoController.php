<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class PhotoController extends Controller
{
    public function store(Post $post)
    {
        $this->validate(request(), [
            'photo' => 'image|max:2048|required'
        ]);
        
        $photo = request()->file('photo')->store('public');
        
        $photoUrl = Storage::url($photo);

        Photo::create([
            'url' => $photoUrl,
            'post_id' => $post->id
        ]);
        
    }

    public function destroy(Photo $photo)
    {
        $photo->delete();
        $photoPatch = str_replace('storage', 'public', $photo->url);
        Storage::delete($photoPatch);
        return back()->with('flash', 'foto eliminada');
    }
}
