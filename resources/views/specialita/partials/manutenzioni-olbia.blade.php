<section id="manutenzioni-olbia" class="orverflow-hidden pb-5 pt-3">
    <div class="container-fluid mt-md-5">
        <div class="row px-5">
            <div class="col-xs-12 col-md-6 text-center">
                <img class="img-fluid" width="400" height="400" src="images/especial/slider_2.jpg">
            </div>
            <div class="col-xs-12 col-md-6 pl-0">
                <!-- MOBILE -->
                <h4 class="d-md-none text-uppercase underline-left font-weight-bold text-primary mt-4 mb-5">
                    MANUTENZIONI OLBIA
                </h4>
                
                <!-- DESKTOP -->
                <h2 class="d-none d-md-block underline-left font-weight-bold text-primary mb-5">
                    MANUTENZIONI OLBIA
                </h2>

                <p style="font-size: 14pt">
                    Siamo specializzati e svolgiamo servizi di manutenzione ordinaria e straordinaria per privati, condomini e pubblica amministrazione.
                </p>
            </div>
        </div>
    </div>
</section>      
</section>