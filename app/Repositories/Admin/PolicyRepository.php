<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Policy;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PolicyRepository
 * @package App\Repositories\Admin
 * @version February 22, 2019, 7:29 pm UTC
 *
 * @method Policy findWithoutFail($id, $columns = ['*'])
 * @method Policy find($id, $columns = ['*'])
 * @method Policy first($columns = ['*'])
*/
class PolicyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'image',
        'status_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Policy::class;
    }

    public function getCustom()
    {
        $return = [];

        foreach ( $this->all() as $item ) {
            $return[ $item->slug ] = $item->translation()->description;
        }

        return $return;
    }
}
