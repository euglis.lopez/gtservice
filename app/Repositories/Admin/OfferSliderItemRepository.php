<?php

namespace App\Repositories\Admin;

use App\Models\Admin\OfferSliderItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OfferSliderItemRepository
 * @package App\Repositories\Admin
 * @version February 20, 2019, 3:29 pm UTC
 *
 * @method OfferSliderItem findWithoutFail($id, $columns = ['*'])
 * @method OfferSliderItem find($id, $columns = ['*'])
 * @method OfferSliderItem first($columns = ['*'])
*/
class OfferSliderItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'image',
        'image_responsive',
        'status_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OfferSliderItem::class;
    }
}
