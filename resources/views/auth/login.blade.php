@extends( 'layouts.auth.app' )

@section( 'content' )
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url( '/' ) }}">{{ config( 'app.name' ) }}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">{{ __('Accedi per iniziare la sessione') }}</p>

            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Accedi per iniziare la sessione') }}">
                @csrf
                <div class="form-group {{ $errors->has('email') ? ' is-invalid' : '' }} has-feedback">
                    <input type="email"
                    class="form-control"
                    placeholder="E-mail"
                    name="email"
                    value="{{ old('email') }}"
                    required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' is-invalid' : '' }} has-feedback">
                    <input type="password" class="form-control" placeholder="Password" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        {{-- <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </label>
                        </div> --}}
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Accesso') }}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <!-- <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                Google+</a>
            </div> -->

            <!-- /.social-auth-links -->
            <a href="{{ route('password.request') }}">{{ __('Hai dimenticato la password?') }}</a><br>
            <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

        </div>
        <!-- /.login-box-body -->
    </div>
@endsection
