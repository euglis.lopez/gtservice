<div class="col-12">
    <h1>Termini</h1>
    <h4 class="sub-header">Panoramica</h4>
    <p>
        Questo sito è gestito da GT SERVICE. In tutto il sito, i termini "noi", "noi" e "nostro" si riferiscono a GT SERVICE. GT SERVICE offers this website, incluse tutte le informazioni, gli strumenti e i servizi disponibili da questo sito all'utente, condizionati all'accettazione da parte dell'utente di tutti i termini, condizioni, politiche e comunicazioni qui indicati.

        Visitando il nostro sito e / o acquistando qualcosa da noi, ti impegni nel nostro "Servizio" e accetti di essere vincolato dai seguenti termini e condizioni ("Termini di servizio", "Termini"), compresi i termini e le condizioni supplementari e le politiche di cui al presente documento e / o disponibile tramite collegamento ipertestuale. I presenti Termini di servizio si applicano a tutti gli utenti del sito, compresi, a titolo esemplificativo, gli utenti che sono browser, fornitori, clienti, commercianti e / o contributori di contenuti.
                                                        
        Leggere attentamente questi Termini di servizio prima di accedere o utilizzare il nostro sito Web. Accedendo o utilizzando qualsiasi parte del sito, l'utente accetta di essere vincolato da questi Termini di servizio. Se non si accettano tutti i termini e le condizioni del presente accordo, non è possibile accedere al sito Web o utilizzare alcun servizio. Se questi Termini di servizio sono considerati un'offerta, l'accettazione è espressamente limitata ai presenti Termini di servizio.
                                                        
        Eventuali nuove funzionalità o strumenti aggiunti al negozio corrente saranno anch'essi soggetti ai Termini di servizio. Puoi rivedere la versione più aggiornata dei Termini di servizio in qualsiasi momento su questa pagina. Ci riserviamo il diritto di aggiornare, modificare o sostituire qualsiasi parte di questi Termini di servizio pubblicando aggiornamenti e / o modifiche al nostro sito web. È tua responsabilità controllare periodicamente questa pagina per le modifiche. L'uso continuato o l'accesso al sito Web dopo la pubblicazione di qualsiasi modifica costituisce accettazione di tali modifiche.
                                                        
        Il nostro negozio è ospitato su Shopify Inc. Ci forniscono la piattaforma di e-commerce online che ci consente di vendere i nostri prodotti e servizi.
    </p>
    <h4 class="sub-header">Sezione 1 - Termini del negozio online</h4>
    <p>
        Accettando questi Termini di servizio, dichiari di avere almeno la maggiore età nel tuo stato o provincia di residenza, o di avere la maggiore età nel tuo stato o provincia di residenza e ci hai dato il tuo consenso a consentire a tutti i minori dipendenti di utilizzare questo sito.
                                                        
        L'utente non può utilizzare i nostri prodotti per scopi illegali o non autorizzati né può, nell'uso del Servizio, violare alcuna legge nella propria giurisdizione (incluse, a titolo esemplificativo ma non esaustivo, le leggi sul copyright).
                    
        Non è necessario trasmettere worm o virus o codici di natura distruttiva.
                    
        Una violazione o una violazione di uno dei Termini comporterà la cessazione immediata dei Servizi.
    </p>
    <h4 class="sub-header">Sezione 2 - Condizioni generali</h4>
    <p>
        Ci riserviamo il diritto di rifiutare il servizio a chiunque, per qualsiasi motivo, in qualsiasi momento.

        Comprendi che i tuoi contenuti (non compresi i dati della carta di credito), possono essere trasferiti in chiaro e implicano (a) trasmissioni su varie reti; e (b) modifiche per conformarsi e adattarsi ai requisiti tecnici delle reti o dei dispositivi di collegamento. Le informazioni sulla carta di credito vengono sempre crittografate durante il trasferimento su reti.
                    
        Accetti di non riprodurre, duplicare, copiare, vendere, rivendere o sfruttare alcuna parte del Servizio, l'uso del Servizio, o l'accesso al Servizio o qualsiasi contatto sul sito web attraverso il quale il servizio è fornito, senza espressa autorizzazione scritta da parte nostra .
                    
        I titoli utilizzati nel presente accordo sono inclusi solo per comodità e non limiteranno o influenzeranno in alcun modo le presenti Condizioni.
    </p>
    <h4 class="sub-header">Sezione 3 - Accuratezza, completezza e tempestività delle informazioni</h4>
    <p>
        Non siamo responsabili se le informazioni rese disponibili su questo sito non sono accurate, complete o attuali. Il materiale su questo sito è fornito solo per informazione generale e non dovrebbe essere invocato o utilizzato come unica base per prendere decisioni senza consultare fonti di informazione primarie, più accurate, più complete o più tempestive. Qualsiasi affidamento sul materiale in questo sito è a tuo rischio.

        Questo sito può contenere alcune informazioni storiche. Le informazioni storiche, necessariamente, non sono aggiornate e vengono fornite solo come riferimento. Ci riserviamo il diritto di modificare i contenuti di questo sito in qualsiasi momento, ma non abbiamo l'obbligo di aggiornare alcuna informazione sul nostro sito. Accetti che è tua responsabilità monitorare le modifiche al nostro sito.
    </p>
    <h4 class="sub-header">Sezione 4 - Modifiche al servizio e ai prezzi</h4>
    <p>
        I prezzi dei nostri prodotti sono soggetti a modifiche senza preavviso.
                                                        
        Ci riserviamo il diritto in qualsiasi momento di modificare o interrompere il Servizio (o qualsiasi parte o contenuto dello stesso) senza preavviso in qualsiasi momento.
                    
        Non saremo responsabili nei confronti dell'utente o di terzi per eventuali modifiche, variazioni di prezzo, sospensione o interruzione del servizio.
    </p>
    <h4 class="sub-header">Sezione 5 - Prodotti o servizi (se applicabile)</h4>
        Alcuni prodotti o servizi potrebbero essere disponibili esclusivamente online attraverso il sito web. Questi prodotti o servizi possono avere quantità limitate e sono soggetti a restituzione o sostituzione solo in base alla nostra Politica di restituzione.

        Abbiamo fatto ogni sforzo per visualizzare nel modo più accurato possibile i colori e le immagini dei nostri prodotti che appaiono nel negozio. Non possiamo garantire che il display del monitor del computer di qualsiasi colore sarà accurato.

        Ci riserviamo il diritto, ma non siamo obbligati, di limitare le vendite dei nostri prodotti o servizi a qualsiasi persona, area geografica o giurisdizione. Potremmo esercitare questo diritto caso per caso. Ci riserviamo il diritto di limitare le quantità di prodotti o servizi che offriamo. Tutte le descrizioni dei prodotti o dei prezzi dei prodotti sono soggette a modifiche in qualsiasi momento e senza preavviso, a nostra esclusiva discrezione. Ci riserviamo il diritto di interrompere qualsiasi prodotto in qualsiasi momento. Qualsiasi offerta per qualsiasi prodotto o servizio fatto su questo sito è nulla ove proibito.

        Non garantiamo che la qualità di qualsiasi prodotto, servizio, informazione o altro materiale acquistato o ottenuto da voi soddisferà le vostre aspettative o che eventuali errori nel Servizio verranno corretti.
    </p>
    <h4 class="sub-header">Sezione 6 - Precisione della fatturazione e informazioni sull'account</h4>
    <p>
        Ci riserviamo il diritto di rifiutare qualsiasi ordine effettuato da noi. Potremmo, a nostra esclusiva discrezione, limitare o annullare le quantità acquistate a persona, per nucleo familiare o per ordine. Queste restrizioni possono includere gli ordini effettuati da o sotto lo stesso account cliente, la stessa carta di credito e / o ordini che utilizzano lo stesso indirizzo di fatturazione e / o spedizione. Nel caso in cui apportiamo una modifica o annulliamo un ordine, possiamo tentare di notificarlo contattando l'indirizzo e-mail e / o l'indirizzo di fatturazione / numero di telefono forniti al momento dell'ordine. Ci riserviamo il diritto di limitare o vietare ordini che, a nostro insindacabile giudizio, sembrano essere posti da rivenditori, rivenditori o distributori.

        Accetti di fornire informazioni di acquisto e di conto correnti, complete e accurate per tutti gli acquisti effettuati nel nostro negozio. Accetti di aggiornare tempestivamente il tuo account e altre informazioni, incluso il tuo indirizzo e-mail, i numeri di carta di credito e le date di scadenza, in modo che possiamo completare le tue transazioni e contattarti secondo necessità.

        Per maggiori dettagli, si prega di rivedere la nostra politica di restituzione.

        For more detail, please review our Returns Policy.
    </p>
    <h4 class="sub-header">Sezione 7 - Strumenti opzionali</h4>
    <p>
        Potremmo fornirti l'accesso a strumenti di terze parti sui quali non controlliamo né abbiamo alcun controllo o input.

        Riconosci e accetti che forniamo l'accesso a tali strumenti "così come sono" e "come disponibili" senza garanzie, dichiarazioni o condizioni di alcun tipo e senza alcuna approvazione. Non avremo alcuna responsabilità derivante da o relativa al tuo utilizzo di strumenti di terze parti opzionali.
                    
        Qualsiasi utilizzo da parte dell'utente degli strumenti opzionali offerti attraverso il sito è interamente a proprio rischio e discrezione e si dovrebbe garantire di conoscere e approvare i termini su quali strumenti sono forniti dai relativi fornitori di terze parti.

        Potremmo anche, in futuro, offrire nuovi servizi e / o funzionalità attraverso il sito web (incluso il rilascio di nuovi strumenti e risorse). Tali nuove funzionalità e / o servizi saranno inoltre soggetti alle presenti Condizioni di servizio.
    </p>
    <h4 class="sub-header">Sezione 8 - Link di terze parti</h4>
    <p>
        Alcuni contenuti, prodotti e servizi disponibili tramite il nostro Servizio possono includere materiali di terze parti.

        I link di terze parti su questo sito possono indirizzarti a siti Web di terze parti che non sono affiliati con noi. Non siamo responsabili per l'esame o la valutazione del contenuto o dell'accuratezza e non garantiamo e non avremo alcuna responsabilità o responsabilità per materiali o siti Web di terzi o per altri materiali, prodotti o servizi di terze parti.
        
        Non siamo responsabili di alcun danno o danno relativo all'acquisto o all'utilizzo di beni, servizi, risorse, contenuti o qualsiasi altra transazione effettuata in relazione a siti Web di terzi. Rivedi attentamente le politiche e le pratiche di terze parti e assicurati di averle comprese prima di intraprendere qualsiasi transazione. Reclami, reclami, dubbi o domande relative a prodotti di terzi devono essere indirizzati a terze parti.
    </p>
    <h4 class="sub-header">Sezione 9 - Commenti degli utenti, feedback e altri contributi</h4>
    <p>
        Se, su nostra richiesta, invii determinati invii specifici (ad esempio voci di concorso) o senza una nostra richiesta, invii idee creative, suggerimenti, proposte, piani o altro materiale, sia online, per email, per posta o in altro modo (collettivamente, "commenti"), l'utente accetta che possiamo, in qualsiasi momento, senza limitazioni, modificare, copiare, pubblicare, distribuire, tradurre e altrimenti utilizzare in qualsiasi supporto i commenti che ci inoltrate. Siamo e non abbiamo alcun obbligo (1) di mantenere alcun commento in confidenza; (2) pagare un risarcimento per eventuali commenti; o (3) per rispondere a qualsiasi commento.

        Potremmo, ma non abbiamo alcun obbligo, monitorare, modificare o rimuovere contenuti che determiniamo a nostra esclusiva discrezione illeciti, offensivi, minacciosi, calunniosi, diffamatori, pornografici, osceni o altrimenti discutibili o che violino la proprietà intellettuale di qualsiasi parte o questi Termini di servizio .

        Accetti che i tuoi commenti non violino alcun diritto di terze parti, inclusi copyright, marchio, privacy, personalità o altri diritti personali o proprietari. Accetti inoltre che i tuoi commenti non contengano materiale diffamatorio o altrimenti illegale, offensivo o osceno, o contengano virus informatici o altri malware che potrebbero in qualche modo compromettere il funzionamento del Servizio o di qualsiasi sito web correlato. Non è possibile utilizzare un falso indirizzo e-mail, fingere di essere qualcuno diverso da te o altrimenti indurre in errore noi o terze parti circa l'origine di eventuali commenti. Sei l'unico responsabile per i commenti che fai e la loro accuratezza. Non ci assumiamo alcuna responsabilità e non ci assumiamo alcuna responsabilità per eventuali commenti pubblicati dall'utente o da terze parti.
    </p>
    <h4 class="sub-header">Sezione 10 - Informazioni personali</h4>
    <p>
        L'invio di informazioni personali tramite questo sito Web è regolato dal presente documento che rappresenta la nostra attuale Informativa sulla privacy.
    </p>
    <h4 class="sub-header">Sezione 11 - Errori, inesattezze e omissioni</h4>
    <p>
        Occasionalmente possono esserci informazioni sul nostro sito o nel Servizio che contengono errori tipografici, inesattezze o omissioni che possono riguardare descrizioni di prodotti, prezzi, promozioni, offerte, spese di spedizione del prodotto, tempi di transito e disponibilità. Ci riserviamo il diritto di correggere eventuali errori, inesattezze o omissioni e di modificare o aggiornare le informazioni o annullare gli ordini se qualsiasi informazione nel Servizio o su qualsiasi sito Web correlato è inaccurata in qualsiasi momento senza preavviso (anche dopo aver inviato l'ordine) .

        Non ci assumiamo alcun obbligo di aggiornare, modificare o chiarire le informazioni nel Servizio o su qualsiasi sito Web correlato, incluse, senza limitazioni, informazioni sui prezzi, ad eccezione di quanto richiesto dalla legge. Nessun aggiornamento specifico o data di aggiornamento applicata nel Servizio o su qualsiasi sito Web correlato deve essere presa per indicare che tutte le informazioni nel Servizio o su qualsiasi sito Web correlato sono state modificate o aggiornate.
    </p>
    <h4 class="sub-header">Sezione 12 - Usi proibiti</h4>
    <p>
        Oltre agli altri divieti stabiliti nei Termini di servizio, è vietato l'uso del sito o del suo contenuto: (a) per scopi illeciti; (b) sollecitare altri a compiere o partecipare a atti illeciti; (c) violare regolamenti, regole, leggi o ordinanze locali internazionali, federali, provinciali o statali; (d) violare o violare i nostri diritti di proprietà intellettuale o i diritti di proprietà intellettuale di terzi; (e) molestare, abusare, insultare, danneggiare, diffamare, diffamare, denigrare, intimidire o discriminare in base al genere, all'orientamento sessuale, alla religione, all'etnia, alla razza, all'età, all'origine nazionale o alla disabilità; (f) fornire informazioni false o fuorvianti; (g) caricare o trasmettere virus o qualsiasi altro tipo di codice dannoso che possa o possa essere utilizzato in qualsiasi modo che possa influenzare la funzionalità o il funzionamento del Servizio o di qualsiasi sito Web correlato, altri siti Web o Internet; (h) per raccogliere o tracciare le informazioni personali di terzi; (i) spam, phish, pharm, pretesto, spider, crawl o scrap; (j) per qualsiasi scopo osceno o immorale; o (k) per interferire con o eludere le funzionalità di sicurezza del Servizio o qualsiasi sito Web correlato, altri siti Web o Internet. Ci riserviamo il diritto di interrompere l'utilizzo del Servizio o di qualsiasi sito Web correlato per violare uno degli usi vietati.
    </p>
    <h4 class="sub-header">Sezione 13 - Limitazione di responsabilità</h4> 
    <p>
        Non garantiamo, dichiariamo o garantiamo che l'utilizzo del nostro servizio sarà 
        ininterrotto, tempestivo, sicuro o privo di errori. Non garantiamo che i risultati che 
        possono essere ottenuti dall'uso del servizio saranno accurati o affidabili. Accetti che, di volta in volta, possiamo 
        rimuovere il servizio per periodi di tempo indefiniti o annullare il servizio in qualsiasi momento, senza preavviso. L'utente accetta 
        espressamente che l'utilizzo o l'impossibilità di utilizzare il servizio è a proprio rischio. Il servizio e tutti i prodotti e servizi forniti 
        all'utente tramite il servizio sono (salvo quanto espressamente dichiarato da noi) forniti "come sono" e "come disponibili" per l'uso dell'utente, 
        senza alcuna dichiarazione, garanzia o condizione di alcun tipo, espressa o implicita, comprese tutte le garanzie implicite o condizioni di commerciabilità, 
        qualità commerciabile, idoneità per uno scopo particolare, durata, titolo e non violazione. In nessun caso GT SERVICE, i nostri amministratori, 
        funzionari, dipendenti, affiliati, agenti, appaltatori, stagisti, fornitori, fornitori di servizi o licenziatari saranno ritenuti responsabili per eventuali 
        lesioni, perdite, reclami o qualsiasi azione diretta, indiretta, incidentale, punitiva, speciale , o danni conseguenti di qualsiasi tipo, inclusi, a titolo
        esemplificativo, mancati profitti, mancati guadagni, risparmi persi, perdita di dati, costi di sostituzione o altri danni simili, siano essi derivanti 
        da contratto, illecito civile (inclusa negligenza), responsabilità oggettiva o altro dall'utilizzo di qualsiasi servizio o di qualsiasi prodotto acquistato 
        utilizzando il servizio, o per qualsiasi altra rivendicazione relativa in qualsiasi modo all'utilizzo del servizio o di qualsiasi prodotto, compresi, a titolo 
        esemplificativo, errori o omissioni in qualsiasi contenuto, o qualsiasi perdita o danno di qualsiasi tipo derivante dall'uso del servizio o di qualsiasi 
        contenuto (o prodotto) pubblicato, trasmesso o reso altrimenti disponibile tramite il servizio, anche se consigliato. Poiché alcuni stati o giurisdizioni 
        non consentono l'esclusione o la limitazione di responsabilità per danni conseguenti o incidentali, in tali stati o giurisdizioni, la nostra responsabilità è 
        limitata alla massima estensione consentita dalla legge
    </p>
    <h4 class="sub-header">Sezione 14 – Indennizzo</h4>
    <p>
        Accetti di indennizzare, difendere e tenere indenne GT SERVICE e i nostri genitori, sussidiarie, affiliate, partner, funzionari, direttori, agenti, appaltatori, licenzianti, fornitori di servizi, subappaltatori, fornitori, stagisti e dipendenti, inoffensivi da qualsiasi richiesta o richiesta, incluse le spese legali ragionevoli, fatte da terze parti a causa o derivanti dalla violazione delle presenti Condizioni di servizio o dei documenti che incorporano per riferimento, o la violazione di qualsiasi legge o dei diritti di terzi.
    </p>
    <h4 class="sub-header">Sezione 15 – Separabilità</h4>
    <p>
        Nel caso in cui qualsiasi disposizione dei presenti Termini di servizio sia ritenuta illegale, nulla o inapplicabile, tale disposizione sarà comunque applicabile nella misura massima consentita dalla legge applicabile, e la parte inapplicabile sarà considerata come recisa dai presenti Termini di Servizio, tale determinazione non pregiudica la validità e l'applicabilità di altre disposizioni rimanenti.
    </p>
    <h4 class="sub-header">Sezione 16 – Risoluzione</h4>
    <p>
        Gli obblighi e le responsabilità delle parti sostenute prima della data di scadenza sopravvivono alla risoluzione del presente accordo a tutti gli effetti.

        I presenti Termini di servizio sono efficaci a meno che e fino alla risoluzione da parte nostra o di noi. È possibile risolvere i presenti Termini di servizio in qualsiasi momento informandoci che non si desidera più utilizzare i nostri Servizi o quando si interrompe l'utilizzo del nostro sito.

        Se a nostro insindacabile giudizio fallisci, o sospettiamo che tu abbia fallito, di rispettare qualsiasi termine o disposizione dei presenti Termini di servizio, potremmo anche rescindere il presente accordo in qualsiasi momento senza preavviso e rimarrai responsabile per tutte le somme dovute fino alla data di cessazione e / o di conseguenza può negare l'accesso ai nostri Servizi (o parte di essi).
    </p>
    <h4 class="sub-header">Sezione 17 - Intero accordo</h4>
    <p>
        L'incapacità di esercitare o far valere qualsiasi diritto o disposizione di questi Termini di servizio non costituisce una rinuncia a tale diritto o disposizione.

        I presenti Termini di servizio e le eventuali politiche o regole operative pubblicate da noi su questo sito o in relazione al Servizio costituiscono l'intero accordo e intesa tra l'utente e noi e disciplinano l'utilizzo del Servizio, sostituendo qualsiasi accordo, comunicazione e proposta precedente o contemporanea. , sia orale che scritto, tra te e noi (incluse, ma non limitate a, eventuali versioni precedenti dei Termini di servizio).

        Eventuali ambiguità nell'interpretazione di questi Termini di servizio non devono essere interpretate contro la parte redazionale.
    </p>
    <h4 class="sub-header">Section 18 - Legge applicabile</h4>
    <p>
        
        I presenti Termini di servizio e gli eventuali accordi separati in base ai quali vi forniamo i Servizi saranno disciplinati e interpretati in conformità con le leggi di  Loc.
    </p>
    <h4 class="sub-header">Sezione 19 - Modifiche ai Termini di servizio</h4>
    <p>
        È possibile rivedere la versione più recente dei Termini di servizio in qualsiasi momento su questa pagina.

        Ci riserviamo il diritto, a nostra esclusiva discrezione, di aggiornare, modificare o sostituire qualsiasi parte di questi Termini di servizio pubblicando aggiornamenti e modifiche al nostro sito web. È responsabilità dell'utente controllare periodicamente il nostro sito Web per eventuali modifiche. L'uso continuato o l'accesso al nostro sito Web o al Servizio in seguito alla pubblicazione di eventuali modifiche ai presenti Termini di servizio costituisce accettazione di tali modifiche.
    </p>
    <h4 class="sub-header">Sezione 20 - Informazioni di contatto</h4>
    <p>
        Le domande sui Termini di servizio devono essere inviate a 
        <a href="mailto:gtserviceolbia@gmail.com" target="_blank">gtserviceolbia@gmail.com
        </a> 
    </p>
</div>


