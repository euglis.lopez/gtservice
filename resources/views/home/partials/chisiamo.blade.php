@push('styles')
<style>
</style>
@endpush

<section id="chisiamo" class="bg-gray">
    <div class="container-fluid pt-5 mx-md-5">
        <div class="row px-md-5 mr-md-4">
            <div class="col-xs-12 col-md-6 mb-4 text-center">
                <img class="img-fluid d-md-none" width="300" height="300" src="{{ $chiSiamo->image }}">
                <img class="img-fluid d-none d-md-block" width="540" src="{{ $chiSiamo->image }}">
            </div>
            <div class="col-xs-12 col-md-6">
                <h2 class="text-uppercase underline-left font-weight-bold text-primary mb-5">
                    {{ $chiSiamo->translation()->title }}
                </h2>
                <span class="text-justify" style="font-size: 14pt">

                    {!! $chiSiamo->translation()->description !!}

                </span>
                <!--
                <button class="btn btn-primary btn-lg">
                    VEDI DI PIU
                </button>
                -->
            </div>
        </div>
    </div>
</section>