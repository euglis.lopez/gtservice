@extends('app')

@push('styles')
@endpush

@push('scripts')
@endpush

@section('content')

@include('global.navbar')





<div class="container">


<div class="row">


    <div class="col-12">
    <h1 class="py-5">Politica sui cookie</h1>
    <h4 class="sub-header">fdgrafik Politica sui cookie
                </h4>
                <p>
                    Questa Informativa sulla privacy descrive come le vostre informazioni personali vengono raccolte, utilizzate e condivise quando visitate o effettuate un acquisto da  http://fdgrafik.com/ (il "Sito").
                </p>
                 <h4 class="sub-header">Informazioni personali We Collec</h4>
                <p>
                    Quando visiti il Sito, raccogliamo automaticamente alcune informazioni sul tuo dispositivo, incluse informazioni sul tuo browser web, indirizzo IP, fuso orario e alcuni dei cookie installati sul tuo dispositivo. Inoltre, mentre navighi sul Sito, raccogliamo informazioni sulle singole pagine web o sui prodotti che visualizzi, su quali siti web o termini di ricerca ti hanno indirizzato al Sito e su come interagisci con il Sito. Ci riferiamo a queste informazioni raccolte automaticamente come "Informazioni sul dispositivo".
                </p>
                <h4 class="sub-header">Raccogliamo informazioni sui dispositivi utilizzando le seguenti tecnologie:</h4>
                <p>
                    - I "cookie" sono file di dati che vengono inseriti sul dispositivo o sul computer e spesso includono un identificatore univoco anonimo. Per ulteriori informazioni sui cookie e su come disabilitare i cookie, visitare http://www.allaboutcookies.org.
                    - "File di registro" tiene traccia delle azioni che si verificano sul sito e raccolgono dati tra cui indirizzo IP, tipo di browser, provider di servizi Internet, pagine di riferimento / uscita e timbri di data / ora.
                    - "Web beacon", "tag" e "pixel" sono file elettronici utilizzati per registrare informazioni su come si naviga nel sito. Inoltre, quando effettui un acquisto o tenti di effettuare un acquisto tramite il Sito, raccogliamo alcune informazioni da te, tra cui il tuo nome, indirizzo di fatturazione, indirizzo di spedizione, informazioni di pagamento (inclusi numeri di carta di credito), indirizzo email e numero di telefono. Ci riferiamo a queste informazioni come "Informazioni sull'Ordine". Quando parliamo di "Informazioni personali" in questa Informativa sulla privacy, stiamo parlando sia di Informazioni dispositivo che di informazioni sull'ordine. 
                </p>
                <h4 class="sub-header">Come usiamo le tue informazioni personali?</h4>
                <p>Usiamo le informazioni sull'ordine che raccogliamo in genere per soddisfare qualsiasi ordine effettuato attraverso il Sito (incluso l'elaborazione delle informazioni di pagamento, la preparazione della spedizione e la fornitura di fatture e / o conferme d'ordine). Inoltre, utilizziamo le informazioni dell'ordine per: 
                Comunicare con voi;
                Esamina i nostri ordini di potenziali rischi o frodi; e se in linea con le preferenze che hai condiviso con noi, fornisci informazioni o pubblicità relative ai nostri prodotti o servizi.
                Utilizziamo le Informazioni sul dispositivo che raccogliamo per aiutarci a individuare potenziali rischi e frodi (in particolare il vostro indirizzo IP) e, più in generale, a migliorare e ottimizzare il nostro Sito (ad esempio, generando analisi su come i nostri clienti navigano e interagiscono con il sito e per valutare il successo delle nostre campagne di marketing e pubblicitarie). 
                </p>
                <h4 class="sub-header">Pubblicità comportamentale</h4>
                <p>
                    Come descritto sopra, utilizziamo le vostre informazioni personali per fornirvi pubblicità mirate o comunicazioni di marketing che riteniamo possano essere di vostro interesse. Per ulteriori informazioni su come funziona la pubblicità mirata, è possibile visitare la pagina educativa Network Advertising Initiative ("NAI") all'indirizzo http://www.networkadvertising.org/understanding-online-advertising/how-does-it-work.
                    Inoltre, puoi disattivare alcuni di questi servizi visitando il portale di opt-out di Digital Advertising Alliance all'indirizzo: http://optout.aboutads.info/.
                </p>
                <h4 class="sub-header">Non tracciare</h4>
                <p>
                    Si prega di notare che non alteriamo la raccolta dei dati del nostro sito e le pratiche di utilizzo quando vediamo un segnale Do Not Track dal browser.
                </p>
                <h4 class="sub-header">I tuoi diritti</h4>
                <p>
                    Se sei residente in Europa, hai il diritto di accedere alle informazioni personali che deteniamo su di te e di chiedere che le tue informazioni personali siano corrette, aggiornate o cancellate. Se desideri esercitare questo diritto, ti preghiamo di contattarci attraverso le informazioni di contatto qui sotto.

                    Inoltre, se sei residente in Europa, notiamo che stiamo elaborando le tue informazioni al fine di adempiere ai contratti che potremmo avere con te (ad esempio se fai un ordine attraverso il Sito), o comunque per perseguire i nostri legittimi interessi commerciali sopra elencati. Inoltre, ti preghiamo di notare che le tue informazioni saranno trasferite al di fuori dell'Europa, incluso in Canada e negli Stati Uniti.
                </p>
                <h4 class="sub-header">Conservazione dei dati</h4>
                <p>
                    Quando effettui un ordine attraverso il Sito, manterremo le informazioni sull'Ordine per i nostri archivi a meno che e fino a quando non ci chiedi di eliminare queste informazioni.
                </p>
                <h4 class="sub-header">I minorenni</h4>
                <p>
                    Il sito non è destinato a persone di età inferiore ai 15 anni.
                </p>
                <h4 class="sub-header">I cambiamenti</h4>
                <p>
                    Potremmo aggiornare la presente informativa sulla privacy di volta in volta al fine di riflettere, ad esempio, modifiche alle nostre pratiche o per altri motivi operativi, legali o normativi.
                </p>
                <h4 class="sub-header">Contattaci</h4>
                <p>
                    Per ulteriori informazioni sulle nostre pratiche sulla privacy, se avete domande, o se volete fare un reclamo, vi preghiamo di contattarci via e-mail a fdgrafik.com  oo via mail utilizzando i dettagli forniti di seguito:
                    Loc. Spiaggia Bianca Golfo Aranci, Sardegna, Italy
                </p>
    </div>


</div>



</div>





@include('global.contact')
@include('global.footer')
@include('global.navfixed')


@endsection