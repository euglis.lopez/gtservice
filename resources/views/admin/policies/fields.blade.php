<div class="col-md-8">
    <div class="box box-primary">
        <!-- <div class="box-header">
            <h3 class="box-title">Crear item</h3>
        </div> -->

        <div class="box-body">
            <!-- title -->
            <div class="form-group {{ $errors->has( 'title' ) ? 'has-error' : ''}}">
                <label for="">Titolo</label>
                <input name="title" type="text" class="form-control" value="{{ old( 'title', @$translation->title ) }}">
                {!! $errors->first( 'title', '<small class="help-block">:message</small>' ) !!}
            </div>

            <!-- description -->
            <div class="form-group {{ $errors->has( 'description' ) ? 'has-error' : ''}}">
                <label for="">Dettaglio</label>
                <textarea id="description" name="description" rows="10" class="form-control"> {{ old( 'description', @$translation->description ) }} </textarea>
                {!! $errors->first( 'description', '<small class="help-block">:message</small>' ) !!}
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="box box-primary">
        <!-- <div class="box-header">
            <h3 class="box-title"></h3>
        </div> -->

        <div class="box-body">
            <div class="form-group">
                <button class="btn btn-primary btn-block">Salvare</button>
                <p class="text-center"><small>Salva oggetto</small></p>
            </div>

            <div class="form-group">
                <a href="{{ route('admin.policies.index') }}" class="btn btn-default btn-block">Indietro</a>
            </div>
        </div>
    </div>
</div>


@push( 'style' )
@endpush

@push( 'script' )

    <!-- CK Editor -->
    <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

    <!-- CKEDITOR -->
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace( 'description' )
            CKEDITOR.config.height = 315;
            //Initialize Select2 Elements
        })
    </script>
@endpush