<aside class="fixed-bottom d-md-none">
    <nav class="d-flex w-100 justify-content-around bg-primary text-center"
        style="box-shadow: -2px 0px 5px;">
        <a class="text-white" href="https://api.whatsapp.com/send?phone=+{{ str_replace( ' ', '', $businessValues[ 'phone1' ] ) }}&text=Ciao%2C%20voglio%20acquisire%20un%20contatto%20con%20te">
            <figure class="my-4">
               <img width="30" src="{{asset('images/icons/whatsapp-logo.png')}}" alt="whatsaap">
           </figure>
        </a>
        <a class="text-white" href="tel:+{{ str_replace( ' ', '', $businessValues[ 'phone1' ] ) }}">
            <figure class="my-4">
               <img width="32" src="{{asset('images/icons/telephone.png')}}" alt="phone">
            </figure>
        </a>
        <a class="text-white border-white" href="mailto:{{ $businessValues[ 'email' ] }}">
            <figure class="my-4">
                <img width="32" src="{{asset('images/icons/mail-envelope-symbol.png')}}" alt="email">
                <!-- <figcaption class="mt-1">Email</figcaption> -->
            </figure>
        </a>
    </nav>
</aside>