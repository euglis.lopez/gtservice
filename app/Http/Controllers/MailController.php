<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MailController extends Controller
{
	public static function sendMail(Request $request)
	{
		$request['subject']		= isset( $request['subject'] ) ? $request['subject'] : 'GT SERVICE';
		$request['msg']			= isset( $request['message'] ) ? $request['message'] : '';

		$subject 			   	= $request['subject'];
		$destinatario 	 		= $request['email'];

		try{
				$response = Mail::send('mails.contactoCliente', $request->all(), function ($message) use ($destinatario, $subject) {
				$message->to($destinatario)->subject($subject);
				});
		}catch(\Swift_TransportException $e){
			return $e->getMessage();
		}

		try{
				Mail::send('mails.contacto', $request->all(), function ($message) use ($subject) {
				$message->to("gtserviceolbia@gmail.com")->subject($subject);
			});
		}catch(\Swift_TransportException $e){
			return $e->getMessage();
		}
	}
}
