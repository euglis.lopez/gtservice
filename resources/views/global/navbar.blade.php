@push('styles')
<style>
    @media screen and (min-width: 992px) {
        .navbar-light .navbar-nav .nav-link {
            color: rgba(0, 0, 0, 0.9);
        }
        .navbar-font{
            font-size: 1rem;
            font-family: 'Montserrat', sans-serif;
            font-weight: bold;
        }
        .navbar-expand-lg .navbar-nav .nav-link {
            padding-right: 1.5rem;
            padding-left: 1.5rem;
        }
        .nav-link, .nav-link:hover {
            color: var(--primary-color);
            font-weight: 800;
        }
        .nav-item.active, .nav-item:hover {
            border-bottom: 8px solid var(--primary-color);
        }
        .navbar-brand {
            margin-top: -20px;   
        }
        .btn-nav {
            margin-top: -5px;
        }
    }
</style>
@endpush
<div class="posicionado">
<div class="container pt-md-4">
    <nav class="navbar navbar-font d-sm-flex navbar-expand-lg pb-md-0">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars fa-lg text-primary" aria-hidden="true"></i>
        </button>
        <a class="navbar-brand mx-auto p-0" href="{{route('home')}}">
            <img width="256" src="{{asset('images/logo/logo.svg')}}" alt="GT SERVICE">
        </a>
        
        <div class="collapse navbar-collapse py-0 w-33 pl-lg-3" id="navbarNav">
            <div class="d-md-flex justify-content-md-between col-12">
                <div class="nav-item"
                    v-bind:class="{ active: pathname === '/' || pathname === '/terms' }"
                >
                    <a class="nav-link text-uppercase" href="{{route('home')}}">HOME <span class="sr-only">(current)</span>
                    </a>
                </div>
                <div class="nav-item">
                    <a class="nav-link text-uppercase" href="/#chisiamo">CHI SIAMO</a>
                </div>
                <div class="nav-item"
                    v-bind:class="{ active: pathname === '/servizi' }"
                >
                    <a class="nav-link text-uppercase" href="{{route('specialita')}}">SERVIZI</a>
                </div>
                <div class="nav-item">
                    <a class="nav-link text-uppercase" href="/#gallery">GALLERIA D'IMMAGINI</a>
                </div>
                <div class="btn-nav pb-2 pb-md-0">
                    <a class="btn btn-primary mt-1 font-weight-bold text-uppercase" href="#contacto">CONTATTACI</a>
                </div>

            </div>              
        </div>
    </nav>
</div>
</div>

@push('scripts')
<script>
    $(document).on('click', '.nav-item a', function (e) {
        $(this).parent().addClass('active').siblings().removeClass('active');
    });
</script>
@endpush