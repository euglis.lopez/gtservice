@extends('layouts.admin.app')

@section( 'header' )
    <h1>
        Diapositiva principale
        <small>Modifica elemento</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route( 'admin' ) }}"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li><a href="{{ route( 'admin.mainSliderItems.index' ) }}"><i class="fa fa-list"></i> Diapositiva principale</a></li>
        <li class="active">Modifica</li>
      </ol>
@stop

@section( 'content' )
   @include( 'adminlte-templates::common.errors' )

   <div class="row">

        {{-- image --}}
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3 col-xs-6" style="height: 200px; overflow: hidden;">

                            <label style="display: block;">Immagine</label>
                            @if ( $mainSliderItem->image )

                                <form method="POST" action="{{ route( 'admin.mainSliderItems.photos.destroy', [ $mainSliderItem->id, 'image' ] ) }}">
                                    @csrf
                                    @method( 'DELETE' )
                                    <button class="btn btn-danger btn-xs" style="position: absolute;"><i class="fa fa-remove"></i></button>
                                    <img src="{{ url( $mainSliderItem->image ) }}" alt="" class="img-responsive">
                                </form>

                            @else

                                <p>Nessuna foto</p>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>

       {!! Form::model( $mainSliderItem, [ 'route' => [ 'admin.mainSliderItems.update', $mainSliderItem->id ], 'method' => 'patch' ] ) !!}

            @include( 'admin.main_slider_items.fields' )

       {!! Form::close() !!}
   </div>
@endsection