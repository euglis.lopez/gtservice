<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PageSectionTranslation
 * @package App\Models\Admin
 * @version March 7, 2019, 5:45 pm UTC
 *
 * @property \App\Models\Admin\Language language
 * @property \App\Models\Admin\PageSection pageSection
 * @property \Illuminate\Database\Eloquent\Collection gallerySliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection mainSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection offerSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection policyTranslations
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property integer page_section_id
 * @property integer language_id
 * @property string title
 * @property string description
 */
class PageSectionTranslation extends Model
{
    use SoftDeletes;

    public $table = 'page_section_translations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'page_section_id',
        'language_id',
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'page_section_id' => 'integer',
        'language_id' => 'integer',
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function language()
    {
        return $this->belongsTo(\App\Models\Admin\Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pageSection()
    {
        return $this->belongsTo(\App\Models\Admin\PageSection::class);
    }
}
