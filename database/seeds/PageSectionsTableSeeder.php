<?php

use Illuminate\Database\Seeder;

class PageSectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Chi Siamo
         */
        DB::table( 'page_sections' )->insert( [
            [
                'code'              => 'chi-siamo',
                'slug'              => 'chi-siamo',
                'image'             => 'images/chisiamo/chisiamo.png',
                'status_id'         => 1,
            ]
        ] );
        DB::table( 'page_section_translations' )->insert( [
            [
                'page_section_id'       => 1,
                'language_id'           => 1,
                'title'                 => 'Chi Siamo',
                'description'           => '<p>
                    La nostra azienda ha a disposizione un team di tecnici specializzati, pronti ad intervenire a qualsiasi ora per fornire servizi idraulici di pronto intervento.
                    I nostri operatori  valuteranno la situazione in maniera rapida ed efficace in modo da proporre rapidamente la soluzione più consona ad ovviare al problema.
                    <br>
                    <br>
                    La ditta Gt service di Olbia esegue piccole ristrutturazioni edili in ambito domestico, manutenzione e installazione impianti elettrici, manutenzione e installazione impianti idraulici, sostituzione scaldabagni, rubinetterie, sifoni e sanitari.
                </p>'
            ]
        ] );
    }
}
