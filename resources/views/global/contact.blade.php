<section id="contact" class="bg-primary">
    <div class="container jumperr">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-3 align-self-center my-4 text-xs-center">
                <a class="navbar-brand text-white mr-lg-0 p-0" href="{{ route( 'home' ) }}">
                    <img width="256"
                        src="{{ asset( 'images/logo/logo-footer.png' ) }}"
                        alt="GT Service">
                </a>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3 mt-md-5">
                <ul style="line-height: 2.5" class="text-white menu font-weight-bold">
                    <li><a href="/#" class="text-white">HOME</a></li>
                    <li><a href="{{ route( 'specialita' ) }}" class="text-white">SERVIZI</a></li>
                    <li><a href="/#chisiamo" class="text-white">CHI SIAMO</a></li>
                    <li><a href="/#gallery" class="text-white">GALLERIA D'IMMAGINI</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3 mt-md-5 d-none d-md-block">
                <h5 class="text-uppercase font-weight-bold text-white">
                    CATEGORIE
                </h5>
                <hr class="mt-2" align="left" width="35px" style="border-width: 4px">
                <p style="line-height: 2.3">
                    @foreach ( $posts as $post )
                        <a class="text-white" href="{{ route( 'specialita' ) . '#' . $post->url }}">
                            {{ $post->title }}
                        </a>
                        <br>
                    @endforeach
                </p>
            </div>
            <div class="col-xs-6 col-md-4 col-lg-3 mt-md-5 d-none d-md-block">
                <h5 class="text-uppercase font-weight-bold text-white">
                    CONTATACCI
                </h5>
                <hr class="mt-2" align="left" width="35px" style="border-width: 4px">
                <p style="line-height: 1.7">
                    <a class="text-white" target="_blank" href="{{ $businessValues[ 'link_maps' ] }}">
                        GT Service Olbia
                    </a><br>
                    <a href="tel:{{ str_replace( ' ', '', $businessValues[ 'phone1' ] ) }}" class="text-white">
                        Cell: {{ $businessValues[ 'phone1' ] }}
                    </a><br>
                    <a href="mailto:{{ $businessValues[ 'email' ] }}" class="text-white">
                        {{ $businessValues[ 'email' ] }}
                    </a><br>
                    <a class="text-white" target="_blank" href="{{ $businessValues[ 'link_maps' ] }}">
                        {{ $businessValues[ 'address1' ] }}
                        <br>
                        {{ $businessValues[ 'address2' ] }}
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>

@push('styles')
    <style>
        #contact a.nav-link {
            color: #f9cb4c;
        }

        #contact a.nav-link:hover {
            color: #fff;
        }

        hr {
            box-sizing: content-box;
            height: 0;
            overflow: visible;
            background-color: #ffffff;
        }

        .border {
            border-bottom: 1px solid #000;
            padding-bottom: 1px;
            display: inline-block;
        }

        @media screen and (max-width: 767.98px) {
            #contact .menu {
                font-size: 16pt;
                margin-left: 10%;
            }
        }
    </style>
@endpush