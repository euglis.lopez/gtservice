@extends('layouts.admin.app')

@section('header')
    <h1>
        Servizi
        <small>Listato</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li class="active">Servizi</li>
      </ol>
@stop

@section('content')
    <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Listato</h3>
              <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">Creare</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="posts-table" class="table table-bordered table-striped tableGeneral">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Titolo</th>
                  <th>Contenuto</th>
                  <th>Azione</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <td>{{ $post->id }}</td>
                            <td> {{ $post->title }} </td>
                            <td> {{ $post->excerpt }} </td>
                            <td>
                                {{-- <a href="{{ route('posts.show', $post) }}" class="btn btn-default btn-xs" target="_blank">
                                  <i class="fa fa-eye"></i>
                                </a> --}}
                                <a href="{{ route('admin.posts.edit', $post) }}" class="btn btn-info btn-xs">
                                  <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
@stop

@push('style')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{asset('/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endpush

@push('script')
    <!-- bootstrap datepicker -->
    <!-- DataTables -->
    <script src="{{asset('/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>


    {{-- Para inicializar el datatable --}}
    <script src="{{ asset( '/js/datatables.js' ) }}"></script>
    <script>
        $(function() {
            TablesDatatables.init();
        });
    </script>

@endpush