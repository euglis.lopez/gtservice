@extends('layouts.admin.app')

@section( 'header' )
    <h1>
      Configurazioni generali
        <small>Modifica</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route( 'admin' ) }}"><i class="fa fa-dashboard"></i> Cruscotto</a></li>
        <li><a href="{{ route( 'admin.businessValues.index' ) }}"><i class="fa fa-list"></i> Configurazioni generali</a></li>
        <li class="active">Modifica</li>
      </ol>
@stop

@section( 'content' )
   @include( 'adminlte-templates::common.errors' )

   <div class="row">

       {!! Form::model( $businessValue, [ 'route' => [ 'admin.businessValues.update', $businessValue->id ], 'method' => 'patch' ] ) !!}

            @include( 'admin.business_values.fields' )

       {!! Form::close() !!}

   </div>
@endsection