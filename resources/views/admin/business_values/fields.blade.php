<div class="col-md-8" style="padding: 0;">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">{{ $businessValue->name }}</h3>
        </div>

        <div class="box-body">
            <!-- value -->
            <div class="form-group {{ $errors->has( 'value' ) ? 'has-error' : ''}}">
                <label for="">Valore</label>
                <input name="value" type="text" class="form-control" value="{{ old( 'value', $businessValue->value ) }}">
                {!! $errors->first( 'value', '<small class="help-block">:message</small>' ) !!}
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="box box-primary">
        <!-- <div class="box-header">
            <h3 class="box-title">Azione</h3>
        </div> -->

            <div class="box-body">
                <div class="form-group">
                    <button class="btn btn-primary btn-block">Salvare</button>
                    <p class="text-center"><small>Salva oggetto</small></p>
                </div>

                <div class="form-group">
                    <a href="{{ route('admin.businessValues.index') }}" class="btn btn-default btn-block">Indietro</a>
                </div>
            </div>
    </div>
</div>


@push( 'style' )
@endpush

@push( 'script' )
@endpush