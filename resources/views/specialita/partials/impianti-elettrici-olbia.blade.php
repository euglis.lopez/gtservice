<section id="impianti-elettrici-olbia" class="orverflow-hidden pb-5 pt-3">
    <div class="container-fluid mt-md-5">
        <div class="row px-5">
            <div class="col-xs-12 col-md-6 text-center">
                <img class="img-fluid" width="400" height="400" src="images/especial/slider_1.jpg">
            </div>
            <div class="col-xs-12 col-md-6 pl-0">
                <!-- MOBILE -->
                <h4 class="d-md-none text-uppercase underline-left font-weight-bold text-primary mt-4 mb-5">
                    IMPIANTI ELETTRICI OLBIA
                </h4>
                
                <!-- DESKTOP -->
                <h2 class="d-none d-md-block underline-left font-weight-bold text-primary mb-5">
                    IMPIANTI ELETTRICI OLBIA
                </h2>

                <p style="font-size: 14pt">
                    Realizziamo impianti elettrici civili (edilizia residenziale e commerciale) per nuove abitazioni o ristrutturando impianti fuori norma. Nonché fotovoltaico.
                </p>
            </div>
        </div>
    </div>
</section>