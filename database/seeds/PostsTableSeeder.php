<?php

use App\Category;
use App\Photo;
use App\Post;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::truncate();
        Category::truncate();
        Tag::truncate();

        $category = new Category;
        $category->name =  "SERVIZI";
        $category->save();

        // 1
        $post = new Post;
        $post->title = "IMPIANTI ELETTRICI OLBIA";
        $post->url = str_slug("IMPIANTI ELETTRICI OLBIA");
        $post->excerpt = "IMPIANTI ELETTRICI OLBIA";
        $post->body = '<p>
                        Realizziamo impianti elettrici civili (edilizia residenziale e commerciale) per nuove abitazioni o ristrutturando impianti fuori norma. Nonché fotovoltaico.
                        </p>';
        $post->published_at = Carbon::now()->subDays( 3 );
        $post->category_id = 1;
        $post->save();

        $post->tags()->sync( Tag::create( [ 'name' => 'etiqueta 1' ] ) );

        $photo = new Photo();
        $photo->url = 'images/especial/slider_1.jpg';
        $post->photos()->save( $photo );
        $photo = new Photo();
        $photo->url = 'images/especial/stempi-servizi-digitali.jpg';
        $post->photos()->save( $photo );


        // 2
        $post = new Post;
        $post->title = "IMPIANTI IDRAULICI OLBIA";
        $post->url = str_slug("IMPIANTI IDRAULICI OLBIA");
        $post->excerpt = "IMPIANTI IDRAULICI OLBIA";
        $post->body = '<p>
                        Siamo in grado di realizzare impianti idraulici a Olbia di qualunque dimensione, raggiungiamo facilmente clienti in tutta la Sardegna ma sopratutto nelle aree limitrofi.
                        </p>';
        $post->published_at = Carbon::now()->subDays( 2 );
        $post->category_id = 1;
        $post->save();

        $post->tags()->sync( Tag::create( [ 'name' => 'etiqueta 3' ] ) );

        $photo = new Photo();
        $photo->url = 'images/especial/slider_3.jpg';
        $post->photos()->save( $photo );
        $photo = new Photo();
        $photo->url = 'images/especial/StampeCad-servizio.jpg';
        $post->photos()->save( $photo );


        // 3
        $post = new Post;
        $post->title = "MANUTENZIONI OLBIA";
        $post->url = str_slug("MANUTENZIONI OLBIA");
        $post->excerpt = "MANUTENZIONI OLBIA";
        $post->body = '<p>
                        Siamo specializzati e svolgiamo servizi di manutenzione ordinaria e straordinaria per privati, condomini e pubblica amministrazione.
                        </p>';
        $post->published_at = Carbon::now()->subDays(1);
        $post->category_id = 1;
        $post->save();

        $post->tags()->sync( Tag::create( [ 'name' => 'etiqueta 4' ] ) );

        $photo = new Photo();
        $photo->url = 'images/especial/slider_2.jpg';
        $post->photos()->save( $photo );
        $photo = new Photo();
        $photo->url = 'images/especial/grafica-e-siti-web-serviziol.jpg';
        $post->photos()->save( $photo );

    }
}
