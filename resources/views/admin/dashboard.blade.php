@extends('layouts.admin.app')

@section('content')
    <h1>Cruscotto</h1>
    {{-- <p>Usuario : {{ auth()->user()->email }}</p> --}}

    <div class="row">
            <a href="{{ route('admin.mainSliderItems.index') }}" class="col-md-3 col-sm-6 col-xs-12" style="display: inline-block;">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-picture"></i></span>
    
                <div class="info-box-content">
                  <span class="">Diapositiva principale</span>
                  <span class="info-box-number">Vedi tutto</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </a>
            <!-- /.col -->

            <a href="{{ route('admin.offerSliderItems.index') }}" class="col-md-3 col-sm-6 col-xs-12" style="display: inline-block;">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-heart"></i></span>
    
                <div class="info-box-content">
                  <span class="">Diapositiva offerte</span>
                  <span class="info-box-number">Vedi tutto</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </a>
            <!-- /.col -->
    
            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>
    
            <a href="{{ route('admin.posts.index') }}" class="col-md-3 col-sm-6 col-xs-12" style="display: inline-block;">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="glyphicon glyphicon-star"></i></span>
    
                <div class="info-box-content">
                  <span class="">Servizi</span>
                  <span class="info-box-number">Vedi tutto</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </a>
            <!-- /.col -->
            
            <a href="{!! route('admin.businessValues.index') !!}" class="col-md-3 col-sm-6 col-xs-12" style="display: inline-block;">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="glyphicon glyphicon-cog"></i></span>
    
                <div class="info-box-content">
                  <span class="">Configurazioni globali</span>
                  <span class="info-box-number">Vedi tutto</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </a>
            <!-- /.col -->
            
          </div>
@stop