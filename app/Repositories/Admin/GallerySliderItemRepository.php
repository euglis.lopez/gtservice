<?php

namespace App\Repositories\Admin;

use App\Models\Admin\GallerySliderItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GallerySliderItemRepository
 * @package App\Repositories\Admin
 * @version February 22, 2019, 3:41 pm UTC
 *
 * @method GallerySliderItem findWithoutFail($id, $columns = ['*'])
 * @method GallerySliderItem find($id, $columns = ['*'])
 * @method GallerySliderItem first($columns = ['*'])
*/
class GallerySliderItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'image',
        'status_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GallerySliderItem::class;
    }
}
