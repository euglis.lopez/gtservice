<div class="col-md-8">
    <div class="box box-primary">
        <!-- <div class="box-header">
            <h3 class="box-title">Crear item</h3>
        </div> -->

        <div class="box-body">
            <!-- title -->
            <div class="form-group {{ $errors->has( 'title' ) ? 'has-error' : ''}}">
                <label for="">Titolo</label>
                <input name="title" type="text" class="form-control" value="{{ old( 'title', @$translation->title ) }}">
                {!! $errors->first( 'title', '<small class="help-block">:message</small>' ) !!}
            </div>

            <!-- description -->
            <div class="form-group {{ $errors->has( 'description' ) ? 'has-error' : ''}}">
                <label for="">Dettaglio</label>
                <textarea id="description" name="description" rows="10" class="form-control"> {{ old( 'description', @$translation->description ) }} </textarea>
                {!! $errors->first( 'description', '<small class="help-block">:message</small>' ) !!}
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="box box-primary">
        <!-- <div class="box-header">
            <h3 class="box-title"></h3>
        </div> -->

        <div class="box-body">
            <!-- text_button -->
            <div class="form-group {{ $errors->has( 'text_button' ) ? 'has-error' : '' }}">
                <label for="">Testo del pulsante</label>
                <input name="text_button" type="text" class="form-control" value="{{ old( 'text_button', @$translation->text_button ) }}">
                {!! $errors->first('text_button', '<small class="help-block">:message</small>') !!}
            </div>

            <!-- link_button -->
            <div class="form-group {{ $errors->has( 'link_button' ) ? 'has-error' : '' }}">
                <label for="">Collegamento del pulsante</label>
                <input name="link_button" type="text" class="form-control" value="{{ old( 'link_button', @$translation->link_button ) }}" placeholder="http://">
                {!! $errors->first('link_button', '<small class="help-block">:message</small>') !!}
            </div>

            <!-- dropzone -->
            <div class="form-group">
                <label for="">Immagine</label>
                <div class="dropzone"></div>
            </div>

            <div class="form-group">
                <button class="btn btn-primary btn-block">Salvare</button>
                <p class="text-center"><small>Salva oggetto</small></p>
            </div>

            <div class="form-group">
                <a href="{{ route('admin.mainSliderItems.index') }}" class="btn btn-default btn-block">Indietro</a>
            </div>
        </div>
    </div>
</div>


@push( 'style' )
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
@endpush

@push( 'script' )
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

    <!-- CK Editor -->
    <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

    <!-- Dropzone -->
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace( 'description' )
            CKEDITOR.config.height = 315;
            //Initialize Select2 Elements
        })

        var myDropzone = new Dropzone( '.dropzone', {
            url: '/admin/mainSliderItems/{{ @$mainSliderItem->id }}/photos/image',
            acceptedFiles: 'image/*',
            maxFilesize: 2,
            paramName: 'photo',
            maxFiles: 1,
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            dictDefaultMessage: 'Metti qui le tue immagini' + '<br/>' +
                                'Dimensione consigliata: 1920 x 800' + '<br/>' +
                                'Peso consigliato: 500 kb'
        } )
        // Cambiamos el texto del error desde el servidor.
        myDropzone.on( 'error', ( file ) => {
            const msgJSON = file.xhr.response;
            var msg = JSON.parse( msgJSON );
            var elemento = document.querySelectorAll( '.dz-error-message span' );
            elemento[ elemento.length - 1 ].textContent = msg.errors.photo;
        } );
        Dropzone.autoDiscover = false;
    </script>
@endpush