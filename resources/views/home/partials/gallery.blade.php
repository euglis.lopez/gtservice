<section id="gallery" class="orverflow-hidden bg-gray">
<div class="container my-4">
    <div class="row">
        <div class="col-12 text-center">
            <h2 class="text-uppercase underline font-weight-bold text-primary mt-4">
                GALLERIA
            </h2>
        </div>

        <div class="swiper-gallery-desktop mt-5">
            <div class="swiper-wrapper">

                @foreach ( $gallerySliderItems as $item )
                    @if ( $item->image !== null )
                        <div class="swiper-slide scale1-2">
                            <a href="{{ $item->image }}"
                                data-lightbox="example-set">
                                <img class="img-fluid" src="{{ $item->image }}" alt="{{ $item->translation()->image }}">
                            </a>
                        </div>
                    @endif
                @endforeach

            </div>
            <div class="swiper-pagination swiper-galle mt-4"></div>
        </div>

    </div>

</div>
</section>

@push('styles')
    <style>
        #gallery{
            overflow-x: hidden;
            overflow-y: -webkit-paged-y;
        }
        .img-height {
            height: 200px;
            max-height: 200px;
        }
        .swiper-gallery-desktop{
            width: 100%;
            height: 100%;
        }
        .swiper-gallery-desktop .swiper-slide {
          text-align: center;
          font-size: 18px;
          background: #fff;
          /* Center slide text vertically */
          display: -webkit-box;
          display: -ms-flexbox;
          display: -webkit-flex;
          display: flex;
          -webkit-box-pack: center;
          -ms-flex-pack: center;
          -webkit-justify-content: center;
          justify-content: center;
          -webkit-box-align: center;
          -ms-flex-align: center;
          -webkit-align-items: center;
          align-items: center;
        }
        .swiper-gallery-desktop > .swiper-pagination-bullets,
        .swiper-pagination-custom,
        .swiper-pagination-fraction {
            bottom: 0;
            left: 0;
            width: 100%;
        }
        .swiper-gallery-desktop > .swiper-pagination {
            position: initial;
        }
    </style>
@endpush

@push('scripts')
    <script>
        var swiperGallery = new Swiper( '.swiper-gallery-desktop', {
            slidesPerView: 3,
            spaceBetween: 0,
            centeredSlides: false,
            pagination: {
                el: '.swiper-pagination.swiper-galle',
                clickable: true,
            },
            autoplay: {
                delay: 3000,
            },
            // Responsive breakpoints
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                1080: {
                    slidesPerView:3,
                    spaceBetween: 30
                }
            }
        } );

        lightbox.option({
          'resizeDuration': 200,
          'wrapAround': true,
          'albumLabel': "Immagine %1 of %2"
        });
    </script>
@endpush