<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OfferSliderItemTranslation
 * @package App\Models\Admin
 * @version February 20, 2019, 3:50 pm UTC
 *
 * @property \App\Models\Admin\Language language
 * @property \App\Models\Admin\OfferSliderItem offerSliderItem
 * @property \Illuminate\Database\Eloquent\Collection mainSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property integer offer_slider_item_id
 * @property integer language_id
 * @property string title
 * @property string description
 * @property string text_button1
 * @property string value_button1
 * @property string text_button2
 * @property string value_button2
 */
class OfferSliderItemTranslation extends Model
{
    use SoftDeletes;

    public $table = 'offer_slider_item_translations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'offer_slider_item_id',
        'language_id',
        'title',
        'description',
        'text_button1',
        'value_button1',
        'text_button2',
        'value_button2'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'offer_slider_item_id' => 'integer',
        'language_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'text_button1' => 'string',
        'value_button1' => 'string',
        'text_button2' => 'string',
        'value_button2' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function language()
    {
        return $this->belongsTo(\App\Models\Admin\Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function offerSliderItem()
    {
        return $this->belongsTo(\App\Models\Admin\OfferSliderItem::class);
    }
}
