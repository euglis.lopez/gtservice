<section id="ofertas">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="text-uppercase underline font-weight-bold text-primary mt-4">
                    OFFERTE
                </h2>
            </div>
        </div>
        <div class="row my-5">

            <!-- PROMOCION -->
            @foreach ( $offerSliderItems as $offerSliderItem )
                <div class="col-12 col-md-6 mb-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="img-container">
                                <img class="img-fluid" src="{{ $offerSliderItem->image }}" alt="{{ $offerSliderItem->translation()->title }}">
                                <div class="overlay">
                                    <a class="btn btn-action mr-md-5" href="tel:{{ $offerSliderItem->translation()->value_button1 }}">
                                        <img class="img-fluid d-md-none" width="48" height="48" src="images/icons/call-answer.png">
                                        <img class="img-fluid d-none d-md-block" src="images/icons/call-answer.png">
                                    </a>
                                    <a class="btn btn-action ml-md-5" href="mailto:{{ $offerSliderItem->translation()->value_button2 }}">
                                        <img class="img-fluid d-md-none" width="48" height="48" src="images/icons/email.png">
                                        <img class="img-fluid d-none d-md-block" src="images/icons/email.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pt-2">
                            <h3 class="text-primary font-weight-bold">OFFERTA
                                <small class="text-muted">{{ $offerSliderItem->translation()->title }}</small>
                            </h3>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>

@push('styles')
    <style>
        .swiper-oferta {
            margin: 0 auto;
            position: relative;
            overflow: hidden;
            list-style: none;
            padding: 0;
            z-index: 1;
        }

        .swiper-oferta {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        #ofertas .swiper-slide p {
            font-family: 'Roboto', sans-serif;
            font-size: 1.2rem;
            color: grey;
        }
        @media screen and (max-width: 767.98px) {
            /* #ofertas h2, #specialita h2, #gallery h2 {
                font-size: 1.4rem;
            } */
            .icon-title{
                width: 30px;
            }
            .btn-action {
                margin-right: 10px;
                margin-left: 10px;
            }
        }

        .img-container{
            position:relative;
            display:inline-block;
        }
        .img-container .overlay{
            position:absolute;
            top:0;
            left:0;
            width:100%;
            height:100%;
            background:rgb(35,35,35, 0.50);
            opacity:0;
            transition:opacity 500ms ease-in-out;
        }
        .img-container:hover .overlay{
            opacity:1;
        }
        .overlay span{
            position:absolute;
            top:50%;
            left:50%;
            transform:translate(-50%,-50%);
            color:#fff;
        }

        .overlay {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .btn-action {
            background-color: #81b2ff !important;
            border-radius: 12px !important;
            border: none !important;
        }
        .btn-action:hover {
            background-color: #6581ac !important;
        }
    </style>
@endpush

@push('scripts')
    <script>
        var swiperOferta = new Swiper('.swiper-oferta', {
            slidesPerView: 3,
            spaceBetween: 30,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            // Responsive breakpoints
            breakpoints: {
                // when window width is <= 320px
                320: {
                slidesPerView: 1,
                spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                slidesPerView: 1,
                spaceBetween: 20
                },
                // when window width is <= 640px
                940: {
                slidesPerView: 2,
                spaceBetween: 30
                }
            }
        });
    </script>
@endpush