<ul class="sidebar-menu" data-widget="tree">
    <li class="header">Navigazione</li>
    <!-- Optionally, you can add icons to the links -->
    <li {{ request()->is('admin') ? 'class=active' : '' }}>
        <a href="{{ route('admin') }}"><i class="fa fa-dashboard"></i> <span>Cruscotto</span></a>
    </li>

    @include( 'layouts.menu' )
</ul>